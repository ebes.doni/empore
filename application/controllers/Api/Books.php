<?php  
require APPPATH . 'libraries/REST_Controller.php';

class Books extends REST_Controller{
	function __construct(){
		parent::__construct();
		#Configure limit request methods
		$this->methods['index_get']['limit']	= 10; #10 requests per hour per book/key
		$this->methods['index_post']['limit']	= 10; #10 requests per hour per book/key
		$this->methods['index_put']['limit']	= 10; #10 requests per hour per book/key
        $this->methods['index_delete']['limit']	= 10; #10 requests per hour per book/key
		
		#Configure load model api table books
		$this->load->model('book/Book_model');
	}


	function index_get($code=null){	
		#Set response API if Success
		$response['SUCCESS'] = array('status' => TRUE, 'message' => 'Success get data' , 'data' => null );
		
		#Set response API if Not Found
		$response['NOT_FOUND']=array('status' => FALSE, 'message' => 'No data were found' , 'data' => null );
        
		#get id shipment
		if (!empty($this->get('code'))){
			$code=$this->get('code');
		}
			
		if ($code==null) {
			#Call methode get_all from m_Shipments model
			$limit 		= $this->get('limit') 	? : 10; 
			$offset 	= $this->get('offset')	? : '';
			$orderby 	= $this->get('orderby') ? : 'code';
			
			$Books=$this->Book_model->get($limit, $offset, $orderby);
		}

		if ($code!=null) {
			#Check if exist data
			if (!$this->is_exist($code)) {
				$this->response($response['NOT_FOUND'], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
			}
			#Call methode get_by_id from Books model
			$Books=$this->Book_model->get(array('code'=>$code));
		}

        # Check if the Books data store contains Books
		if ($Books) {

			$Books_data = array();

			if($code!=null){				

				foreach($Books as $key => $value){
					$Books_data[$key] = $value;
				}	
			
			}else{
				
				foreach($Books as $key => $value){
					$Books_data[$key] = $value;
                }

			}
			$response['SUCCESS']['data'] = $Books_data;
			
			#if found ICStockShipments
			$this->response($response['SUCCESS'] , REST_Controller::HTTP_OK);

		}else{ 
	        $this->response($response['NOT_FOUND'], REST_Controller::HTTP_NOT_FOUND); # NOT_FOUND (404) being the HTTP response code
		}
	}


	function index_post(){
        #Set response API if exist data
        $response['EXIST'] = array('status' => FALSE, 'message' => 'exist data' , 'data' => null );
        $code = $this->input->post('code');
        if ($this->is_exist($code)){
            $this->response($response['EXIST'],REST_Controller::HTTP_FORBIDDEN);
        }

        $Books_data['code']             = $this->input->post('code');
        $Books_data['title']            = $this->input->post('title');
        $Books_data['publication_year'] = $this->input->post('publication_year');
        $Books_data['author']           = $this->input->post('author');
        $Books_data['stock']            = $this->input->post('stock');

        $insert_book = $this->Book_model->add($Books_data);

        #Set response API if Success
        $response['SUCCESS'] = array('status' => TRUE, 
                                    'message' => 'Success insert data', 
                                    'data' => $Books_data );

        #Set response API if Fail
        $response['FAIL'] = array('status' => FALSE, 
                                    'message' => 'Fail insert data', 
                                    'data' => null );

        if ($insert_book != FALSE) {	
            #If success
            $this->response($response['SUCCESS'],REST_Controller::HTTP_CREATED);

        }else{
           
            $error_msg = '';
            if($this->db->error()){

                $error_msg = $this->db->error();
                $response['ERROR'] = array('status' => FALSE, 'error customers' => $error_msg);

                #If error
                $this->response($response['ERROR'],REST_Controller::HTTP_FORBIDDEN);	
                
            }else{

                #If fail
                $this->response($response['FAIL'],REST_Controller::HTTP_FORBIDDEN);
            
            }
			
        }
		
	 }


	 function index_put(){
        $Books_data['code']             = $this->put('code');
        $Books_data['title']            = $this->put('title');
        $Books_data['publication_year'] = $this->put('publication_year');
        $Books_data['author']           = $this->put('author');
        $Books_data['stock']            = $this->put('stock');

		#get book_id
		$book_id = $this->Book_model->get(array('code'=>$this->put('code')))[0]['book_id'];

        #Check available
		if (empty($book_id)){
			$this->response($response['NOT_FOUND'],REST_Controller::HTTP_NOT_FOUND);
		}
		
		//cek utk memastikan kode buku blm dipakai
		if (!$this->is_exist($this->put('code'))){
			$this->response($response['EXIST'],REST_Controller::HTTP_FORBIDDEN);
		}

        if($book_id){
            $Books_data['book_id'] = $book_id;
            $update_book = $this->Book_model->add($Books_data);
        }
    
        #Set response API if Success
        $response['SUCCESS'] = array('status' => TRUE, 
                                    'message' => 'Success update data', 
                                    'data' => $Books_data );

        #Set response API if Fail
        $response['FAIL'] = array('status' => FALSE, 
                                'message' => 'Fail update data', 
                                'data' => null );

        if ($update_book != FALSE) {	
            #If success
            $this->response($response['SUCCESS'],REST_Controller::HTTP_CREATED);
        }else{
            $error_msg = '';
            if($this->db->error()){

				$error_msg = $this->db->error();
				$response['ERROR'] = array('status' => FALSE, 'error : ' => $error_msg);

				#If error
				$this->response($response['ERROR'],REST_Controller::HTTP_FORBIDDEN);	

            }else{
                #If fail
                $this->response($response['FAIL'],REST_Controller::HTTP_FORBIDDEN);

            }

        }		

	}

    function index_delete($book_code=null){
		#Set response API if Success
		$response['SUCCESS'] = array('status' => TRUE, 'message' => 'success delete data');
		
		#Set response API if data not found
		$response['NOT_FOUND']=array('status' => FALSE, 'message' => 'no data were found');

		#get book_id
		$book_id = $this->Book_model->get(array('code'=>$book_code))[0]['book_id'];

		#Check available data
		if (!$this->validate($book_id)){
			$this->response($response['NOT_FOUND'],REST_Controller::HTTP_NOT_FOUND);
		}

		$delete = $this->Book_model->delete($book_id);
		
		if ($delete == TRUE) {
			#If success
			$this->response($response['SUCCESS'],REST_Controller::HTTP_CREATED);
		
		}else{
			#Set response API if Fail
			$response['FAIL'] = array('status' => FALSE, 'message' => 'Fail insert data');
			#If Fail
			$this->response($response['FAIL'],REST_Controller::HTTP_CREATED);
		}
	}

	function validate($book_id){
		$Books=$this->Book_model->get(array('book_id'=>$book_id));
		if (count($Books)>0){
			return TRUE;  
		}else{
			return FALSE;
		}
	}

	function is_exist($code){
		$Books=$this->Book_model->get(array('code'=>$code));
		if (count($Books)>0){
			return TRUE;  
		}else{
			return FALSE;
		}
	}

}

?>