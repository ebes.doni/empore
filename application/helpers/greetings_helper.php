<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Terbilang Helper
 *
 * @package	CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author	Achyar Anshorie
 */

if ( ! function_exists('greeting')){

    function greeting(){
        $date = date ("G : i A");
        if ($date>=0 and $date<10) {
            echo "Selamat Pagi";
        } else if ($date>=10 and $date<15) {
            echo "Selamat Siang";
        } else if ($date>=15 and $date<19) {
            echo "Selamat Sore";
        } else if ($date>=19 and $date<00) {
            echo "Selamat Malam";
        }else echo "Waktu Salah";
    }
	
}
