<?php
/**
 * author: doniebes
 * Date: 24/03/23
 */
class Menu{
    public function __construct(){
        $this->CI=& get_instance(); 
        $this->CI->load->database('default');
        $this->CI->load->model('modul/Modul_model','',TRUE);
        $this->CI->load->model('users/Users_model','',TRUE);
    }

    public function dynamicMenu(){

        $modul = $this->CI->Modul_model->get();    
        foreach($modul as $m){
            $role = json_decode($m['role_id'], true);
            $uroleid = $this->CI->session->userdata('uroleid'); 
            $user_roles = $this->CI->Users_model->get_role(array('id'=>$uroleid));
            $role_name = $user_roles['role_name']; 

            if($role[0]["$role_name"]=='1'){

                if($m['menu']=='main_menu'){                

                    if(!empty($this->CI->uri->segment(3)) OR $this->CI->uri->segment(3)!=''){
                        $modul_name = $this->CI->uri->segment(3);
                    }else{
                        $modul_name = $this->CI->uri->segment(2);
                    }

                    $parent_id_active = $this->CI->db->query("SELECT parent_id 
                                                            FROM modul WHERE `name` = '$modul_name'")
                                                            ->row_array()['parent_id'];
                    $menu = $this->CI->db->query("SELECT menu, `name` 
                                                    FROM modul WHERE id = '$parent_id_active'")
                                                    ->row_array();
                    
                    if($menu['menu'] != 'main_menu'){
                        $mod_name = $menu['name'];
                        $parent_id_active = $this->CI->db->query("SELECT parent_id 
                                                                FROM modul WHERE `name` = '$mod_name'")
                                                                ->row_array()['parent_id'];
                    }

                    $active = '';
                    if($parent_id_active == $m['id']){
                        $active = 'active';
                    }

                    if ($m['slug'] == '#') {                            
                        $href = '#'.$m['name'];                        
                        $toogle = 'dropdown-toggle';
                        $collapsed = 'collapsed';
                        $collapse = 'collapse';
                        $nav_collapse = 'nav-collapse collapse';
                    } else {                        
                        $href = site_url($m['slug']);                        
                        $toogle = '';
                        $collapsed = '';
                        $collapse = '';
                        $nav_collapse = '';
                    }               

                    //  <!-- Collapse -->
                    $menu = '<div class="nav-item">';

                    // main menu
                    $menu .= '<a class="nav-link '.$toogle.' '.$collapsed.' '.$active.'" 
                                href="'.$href.'" role="button" 
                                data-bs-toggle="'. $collapse.'" 
                                data-bs-target="#navbarVerticalMenu'.$m['name'].'" 
                                aria-expanded="false" 
                                aria-controls="navbarVerticalMenu'.$m['name'].'">';
                    $menu .= '<i class="'.$m['icon'].' nav-icon"></i>';
                    $menu .= '<span class="nav-link-title">'.$m['note'].'</span>';
                    $menu .= '</a>';


                    // sub menu 1
                    if($parent_id_active == $m['id']){
                        $menu .= '<div id="navbarVerticalMenu'.$m['name'].'" 
                                class="'.$nav_collapse.' '.$active.' collapse show" 
                                data-bs-parent="#navbarVerticalMenu">';
                    }else{
                        $menu .= '<div id="navbarVerticalMenu'.$m['name'].'" 
                                class="'.$nav_collapse.' '.$active.'" 
                                data-bs-parent="#navbarVerticalMenu">';
                    }
                    
                    $menu .= '<div id="navbarVerticalMenu'.$m['name'].'Menu">';

                    foreach($modul as $sub){
                        /**set privilages by role name */ 
                        $role = json_decode($sub['role_id'], true);
                        $uroleid = $this->CI->session->userdata('uroleid'); 
                        $user_roles = $this->CI->Users_model->get_role(array('id'=>$uroleid));
                        $role_name = $user_roles['role_name']; 

                        /**set if role name sama dengan session */
                        if($role[0]["$role_name"]=='1'){
                          
                                if($m['id']==$sub['parent_id']){
                                    /**set slug url and treeview class */
                                    if ($sub['slug'] == '#') {    
                                        $slug = '#';                                       
                                        $toogle = 'data-bs-toggle="collapse"';
                                        $href = '#navbarVerticalMenu'.$sub['name'];     
                                        $dropdown_toggle = 'dropdown-toggle';   
                                        $nav_colapse = 'nav-collapse collapse';                               
                                    } else {
                                        $slug = site_url($sub['slug']);                                       
                                        $toogle = '';
                                        $href = site_url($sub['slug']);      
                                        $dropdown_toggle = '';
                                        $nav_colapse = '';                                 
                                    }

                                    /**set modul name from URI segment */
                                    if(!empty($this->CI->uri->segment(3)) OR $this->CI->uri->segment(3)!=''){
                                        $modname = $this->CI->uri->segment(3);
                                    }else{
                                        $modname = $this->CI->uri->segment(2);
                                    }

                                    $parent_id_active = $this->CI->db->query("SELECT parent_id 
                                                        FROM modul WHERE `name` = '$modname'")
                                                        ->row_array()['parent_id'];

                                    /**set class active if nama modul sama dgn URI segment */
                                    if ($modname == $sub['name']) {
                                        $active = 'active';
                                        $open = 'menu-open';    
                                    } else {
                                        $active = '';
                                        $open = '';
                                    }                                    

                                    // sub menu 2
                                    // <!-- Collapse -->
                                    $menu .= '<div class="nav-item">';
                                    $menu .= '<a class="nav-link '.$active.''.$dropdown_toggle.' " 
                                        href="'.$href.'" 
                                        role="button" '.$toogle.' 
                                        data-bs-target="#navbarVerticalMenu'.$sub['name'].'" 
                                        aria-expanded="false" 
                                        aria-controls="navbarVerticalMenu'.$sub['name'].'">
                                        '.$sub['note'].'
                                    </a>';

                                    /**colapse show check parent id active by slug url */
                                    if($parent_id_active == $sub['id']){
                                        $menu .= '<div id="navbarVerticalMenu'.$sub['name'].'" 
                                        class="'.$nav_colapse.' '.$active.' collapse show" 
                                        data-bs-parent="#navbarVerticalMenu'.$m['name'].'Menu">';    
                                    } else {
                                        $menu .= '<div id="navbarVerticalMenu'.$sub['name'].'" 
                                        class="'.$nav_colapse.' 
                                        data-bs-parent="#navbarVerticalMenu'.$m['name'].'Menu">';
                                    }
            
                                        // set sub menu 2
                                        if($sub['menu']=='sub_menu_1'){      
                                            foreach($modul as $sub2){                   
                                                /**set privilages by role name */ 
                                                $role = json_decode($sub2['role_id'], true);
                                                $uroleid = $this->CI->session->userdata('uroleid'); 
                                                $user_roles = $this->CI->Users_model->get_role(array('id'=>$uroleid));
                                                $role_name = $user_roles['role_name'];         

                                                /**set class active if nama modul sama dgn URI segment */
                                                if ($modname == $sub2['name']) {
                                                    $active = 'active';
                                                } else {
                                                    $active = '';
                                                }      

                                                /**set if role name sama dengan session */
                                                if($role[0]["$role_name"]=='1'){                                                                                      
                                                    if($sub['id']==$sub2['parent_id']){                                                        
                                                        $menu .= '<a class="nav-link '.$active.'" href="'.site_url($sub2['slug']).'">'.$sub2['note'].'</a>';                                            
                                                    }
                                                }                                               
                                            }                                                   
                                            // <!-- End Collapse -->
                                            // end sub menu 2         
                                        }           
                                        $menu .= '</div>';
                                        $menu .= '</div>';
                                       
                                }

                        }
                        /**end check rolename sama dengan session user */
                    }
                    $menu .= '</div>';
                    $menu .= '</div>';
                   
                // end main menu 
                $menu .= '</div>';
                //   <!-- End Collapse -->
                echo $menu;
                }
            }
        }
    }
               

}