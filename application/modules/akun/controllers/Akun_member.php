<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun_member extends CI_Controller {
	public function __construct() {
		parent::__construct(TRUE);
		if ($this->session->userdata('logged_member') == NULL) {
			header("Location:" . site_url('member/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('member/Member_model', 
                            'borrow/Borrow_model', 
                            'book/Book_model', 
                            'logs/Logs_model'
							            ));
	}

  // profil member
	public function member_profil() {
      $member_id = $this->session->userdata('uid_member');
      $data['member'] = $this->Member_model->get(array('member_id'=>$member_id));
      $data['title'] = 'Profil Anggota';
      $data['main'] = 'akun/member_profil';
      $this->load->view('member/layout', $data);
	}

  // list peminjaman buku
	public function member_borrow() {
      $member_id = $this->session->userdata('uid_member');
      $data['member'] = $this->Member_model->get(array('member_id'=>$member_id));
      $data['borrow'] = $this->Borrow_model->get(array('member_id'=>$member_id));
      $data['title'] = 'Riwayat Peminjaman';
      $data['main'] = 'akun/member_borrow';
      $this->load->view('member/layout', $data);
  }

  // request buku
	public function member_book_request() {
    $member_id = $this->session->userdata('uid_member');
    $data['book_request'] = $this->Borrow_model->get_book_request(array('member_id'=>$member_id));
    $data['books'] = $this->Book_model->get();
    $data['title'] = 'Book Request';
    $data['main'] = 'akun/member_book_request';
    $this->load->view('member/layout', $data);
  }

  public function add_book_request(){

      $this->load->library('form_validation');
      $this->form_validation->set_rules('request_date', 'Tanggal Request', 'trim|required|xss_clean');
      $this->form_validation->set_rules('book_id', 'Nama Buku', 'trim|required|xss_clean');
  
      $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>', '</div>');
      $data['operation'] = 'Tambah';
  
      if ($_POST AND $this->form_validation->run() == TRUE) {
  
        $params['member_id']        = $this->input->post('member_id');
        $params['book_id']          = $this->input->post('book_id');
        $params['request_date']     = $this->input->post('request_date');
        $params['return_date']      = $this->input->post('return_date');
        $params['approval_status']  = 'pending';
        
        $status = $this->Borrow_model->add_book_request($params);
        
        if($status != FALSE){
          /**add transaksi borrowings */
          $borrow_data = array(
            'book_request_id' => $status,
            'member_id' => $this->input->post('member_id'),
            'book_id' => $this->input->post('book_id'),
            'status' => 'pending'
          );

          $add_borrowings = $this->Borrow_model->add_borrowings($borrow_data);
        }
            
        // activity log
        $this->Logs_model->add(
          array(
            'log_date' => date('Y-m-d H:i:s'),
            'user_id' => NULL,
            'log_module' => 'book_request',
            'log_action' => $data['operation'],
            'log_info' => 'ID:null;Title:'
          )
        );    
        $this->session->set_flashdata('success', $data['operation'] . ' Tambah data berhasil');
        redirect('member/akun/member_book_request');
      } 
  }

}
