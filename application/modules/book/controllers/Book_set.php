<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book_set extends CI_Controller {
  public function __construct(){
    parent::__construct();
    if ($this->session->userdata('logged') == NULL) {
      header("Location:" . site_url('manage/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
    }

    $this->load->model(array('setting/Setting_model',
                             'book/Book_model'
                          ));
    $this->load->helper(array('form', 'url'));
  }

  // book view in list
  public function index() {   
    $data['books'] = $this->Book_model->get();
    $data['title'] = 'Buku';
    $data['main'] = 'book/book_list';
    $this->load->view('manage/layout', $data);
  }

  // book post via api
  public function book_post() {   
    $data['title'] = 'Buku';
    $data['main'] = 'book/book_post';
    $this->load->view('manage/layout', $data);
  }

  // Add book and Update
  public function add($id = NULL) {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('title', 'Judul Buku', 'trim|required|xss_clean');
    $this->form_validation->set_error_delimiters('<div book="alert alert-danger"><button ket="button" book="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
    
    $data['operation'] = is_null($_POST['book_id']) ? 'Tambah' : 'Sunting';

    if ($_POST AND $this->form_validation->run() == TRUE) {
      if ($this->input->post('book_id')) {
        $params['book_id'] = $this->input->post('book_id');
      }
      $params['code']             = $this->input->post('code');
      $params['title']            = $this->input->post('title');
      $params['publication_year'] = $this->input->post('publication_year');
      $params['author']           = $this->input->post('author');
      $params['stock']            = $this->input->post('stock');

      $status = $this->Book_model->add($params);
      
      $this->session->set_flashdata('success', $data['operation'] . ' Buku');
      redirect('manage/book');

      if ($this->input->post('from_angular')) {
        echo $status;
      }

    } else {

      if ($this->input->post('book_id')) {
        redirect('manage/book/edit/' . $this->input->post('book_id'));
      }

      // Edit mode
      if (!is_null($id)) {
        $object = $this->Book_model->get(array('id' => $id));
        if ($object == NULL) {
          redirect('manage/book');
        } else {
          $data['book'] = $object;
        }
      }
      $data['title'] = $data['operation'] . ' Buku';
      $data['main'] = 'book/book_add';
      $this->load->view('manage/layout', $data);
    }
  }


  // Delete to database
  public function delete() {
    if ($this->session->userdata('uroleid')!= SUPERUSER){
        $this->session->set_flashdata('failed', 'Anda tidak mempunyai hak akses untuk menghapus data');
        redirect('manage/book');
    }
    // $siswa = $this->Student_model->get(array('book_id'=>$id));

    if ($_POST) {

      // if (count($siswa) > 0) {
      //   $this->session->set_flashdata('failed', 'Data Kelas tidak dapat dihapus');
      //   redirect('manage/book');
      // }
      
      $id = $_POST['book_id'];
      $this->Book_model->delete($id);
      // activity log
      $this->load->model('logs/Logs_model');
      $this->Logs_model->add(
        array(
          'log_date' => date('Y-m-d H:i:s'),
          'user_id' => $this->session->userdata('uid'),
          'log_module' => 'book',
          'log_action' => 'Hapus',
          'log_info' => 'ID:' . $id . ';Title:' . $this->input->post('delName')
        )
      );
      $this->session->set_flashdata('success', 'Hapus data berhasil');
      redirect('manage/book');

    } elseif (!$_POST) {

      $this->session->set_flashdata('delete', 'Delete');
      redirect('manage/book/edit/' . $id);

    }  
  }
}
