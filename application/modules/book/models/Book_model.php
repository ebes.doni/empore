<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Book_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    
    // Get data from database
    function get($params = array()){
        if(isset($params['book_id'])){
            $this->db->where('book_id', $params['book_id']);
        }
        if(isset($params['code'])){
            $this->db->where('code', $params['code']);
        }
        if(isset($params['title'])){
            $this->db->where('title', $params['title']);
        }
        if(isset($params['publication_year'])){
            $this->db->where('publication_year', $params['publication_year']);
        }
        if(isset($params['author'])){
            $this->db->where('author', $params['author']);
        }
        if(isset($params['stock'])){
            $this->db->where('stock', $params['stock']);
        }
        if(isset($params['limit'])){
            if(!isset($params['offset'])){
                $params['offset'] = NULL;
            }
            $this->db->limit($params['limit'], $params['offset']);
        }

        if(isset($params['order_by'])){
            $this->db->order_by($params['order_by'], 'desc');
        }else{
            $this->db->order_by('book_id', 'desc');
        }

        $this->db->select('books.*');
        $res = $this->db->get('books');

        if(isset($params['book_id'])){
            return $res->row_array();
        }else{
            return $res->result_array();
        }
    }
    
    // Add and update to database
    function add($data = array()) {
        if(isset($data['book_id'])) {
            $this->db->set('book_id', $data['book_id']);
        }
        if(isset($data['title'])) {
            $this->db->set('title', $data['title']);
        }
        if(isset($data['code'])) {
            $this->db->set('code', $data['code']);
        }
        if(isset($data['publication_year'])) {
            $this->db->set('publication_year', $data['publication_year']);
        }
        if(isset($data['author'])) {
            $this->db->set('author', $data['author']);
        }
        if(isset($data['stock'])) {
            $this->db->set('stock', $data['stock']);
        }
        if (isset($data['book_id'])) {
            $this->db->where('book_id', $data['book_id']);
            $this->db->update('books');
            $id = $data['book_id'];
        } else {
            $this->db->insert('books');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }

    // Add and update to database
    function update_by_code($data = array()) {
        if(isset($data['book_id'])) {
            $this->db->set('book_id', $data['book_id']);
        }
        if(isset($data['title'])) {
            $this->db->set('title', $data['title']);
        }
        if(isset($data['code'])) {
            $this->db->set('code', $data['code']);
        }
        if(isset($data['publication_year'])) {
            $this->db->set('publication_year', $data['publication_year']);
        }
        if(isset($data['author'])) {
            $this->db->set('author', $data['author']);
        }
        if(isset($data['stock'])) {
            $this->db->set('stock', $data['stock']);
        }
        if (isset($data['code'])) {
            $this->db->where('code', $data['code']);
            $this->db->update('books');
            $id = $data['code'];
        } else {
            $this->db->insert('books');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }

    // Delete book to database
    function delete($id) {
        $this->db->where('book_id', $id);
        $this->db->delete('books');

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : TRUE;
    }

}
