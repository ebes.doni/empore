<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Book_request_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    // Get book_request from database
    function get($params = array()){
        if (isset($params['book_request_id'])) {
            $this->db->where('book_requests.book_request_id', $params['book_request_id']);
        }
        if (isset($params['member_id'])) {
            $this->db->where('book_requests.member_id', $params['member_id']);
        }
        if (isset($params['book_id'])) {
            $this->db->where('book_requests.book_id', $params['book_id']);
        }
        if (isset($params['request_date'])) {
            $this->db->where('book_requests.request_date', $params['request_date']);
        }
        if (isset($params['approval_status'])) {
            $this->db->where('book_requests.approval_status', $params['approval_status']);
        }
       
        if(isset($params['limit'])){
            if(!isset($params['offset'])){
                $params['offset'] = NULL;
            }
            $this->db->limit($params['limit'], $params['offset']);
        }

        if (isset($params['order_by'])) {
            $this->db->order_by($params['order_by'], 'desc');
        } 

        $this->db->select('book_requests.*');
        $this->db->select('member.*');
        $this->db->select('books.*');

        $this->db->join('member', 'book_requests.member_id = member.member_id', 'left');
        $this->db->join('books', 'book_requests.book_id = books.book_id', 'left');
        
        $res = $this->db->get('book_requests');

        if(isset($params['book_request_id'])){
            return $res->row_array();
        }else{
            return $res->result_array();
        }
    }
    

    // Get Book Request From Databases
    function get_book_request($params = array()) {
        if (isset($params['book_request_id'])) {
            $this->db->where('book_requests.book_request_id', $params['book_request_id']);
        }
        if (isset($params['member_id'])) {
            $this->db->where('book_requests.member_id', $params['member_id']);
        }
        if (isset($params['book_id'])) {
            $this->db->where('book_requests.book_id', $params['book_id']);
        }
        if (isset($params['request_date'])) {
            $this->db->where('book_requests.request_date', $params['request_date']);
        }
        if (isset($params['approval_status'])) {
            $this->db->where('book_requests.approval_status', $params['approval_status']);
        }
       
        if (isset($params['limit'])) {
            if (!isset($params['offset'])) {
                $params['offset'] = NULL;
            }
            $this->db->limit($params['limit'], $params['offset']);
        }
        if (isset($params['order_by'])) {
            $this->db->order_by($params['order_by'], 'desc');
        } 

        $this->db->select('book_requests.*');
        $this->db->select('member.*');
        $this->db->select('books.*');

        $this->db->join('member', 'book_requests.member_id = member.member_id', 'left');
        $this->db->join('books', 'book_requests.book_id = books.book_id', 'left');
        
        $res = $this->db->get('book_requests');

        if (isset($params['book_request_id'])) {
            return $res->row_array();
        }  else {
            return $res->result_array();
        }

    }

    
    // Add and update book request to database
    function add($data = array()) {
        if(isset($data['book_request_id'])) {
            $this->db->set('book_request_id', $data['book_request_id']);
        }
        if(isset($data['member_id'])) {
            $this->db->set('member_id', $data['member_id']);
        }
        if(isset($data['book_id'])) {
            $this->db->set('book_id', $data['book_id']);
        }
        if(isset($data['request_date'])) {
            $this->db->set('request_date', $data['request_date']);
        }
        if(isset($data['approval_status'])) {
            $this->db->set('approval_status', $data['approval_status']);
        }
        
        if (isset($data['book_request_id'])) {
            /**set origin data update */
            $this->db->set('updated_at', date('Y-m-d H:i:s'));
            $this->db->set('updated_by', $this->session->userdata('ufullname'));

            $this->db->where('book_request_id', $data['book_request_id']);
            $this->db->update('book_requests');
            $id = $data['book_request_id'];
        } else {
            /**set origin data insert */
            $this->db->set('created_at', date('Y-m-d H:i:s'));
            $this->db->set('created_by', $this->session->userdata('ufullname'));
            
            $this->db->insert('book_requests');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }

    // Delete class to database
    function delete($id) {
        $this->db->where('borrowing_id', $id);
        $this->db->delete('book_request');
    }

}
