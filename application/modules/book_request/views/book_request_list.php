<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="card">		
					<div class="card-body table-responsive">
						<table id="dtable" class="table table-hover">
							<thead class="thead-light">
                            <tr>
                                <th>No</th>	
                                <th>Nama Anggota</th>
                                <th>Buku</th>
                                <th>Tanggal Request</th>
                                <th>#ID Request</th>
                                <th>Approval Status</th>
                                <th>Actions</th>
                            </tr>
							</thead>
							<tbody>    
							<?php
							if (!empty($book_request)) {
								$i = 1;
								foreach ($book_request as $row):
							?>        
							<tr>
								<td><?= $i ?></td>                                
                                <td><?= $row['member_name'] ?></td>	
								<td class="table-column-ps-0">
								<a class="d-flex align-items-center" href="#">
									<div class="flex-shrink-0">
									<img class="avatar avatar-lg" src="<?= site_url() ?>media/front/assets/img/400x400/img22.jpg" alt="Image Description">
									</div>
									<div class="flex-grow-1 ms-3">
									<h5 class="text-inherit mb-0"><?= $row['title'] ?></h5>
									</div>
								</a>
								</td>
								<td><?= $row['request_date'] ?></td>		
                                <td><?= $row['book_request_id'] ?></td>					
								<td>
									<?php if($row['approval_status']=='approved'){ ?>
										<span class="badge bg-soft-success text-success">
											<span class="legend-indicator bg-success"></span>approved
										</span>
									<?php }elseif($row['approval_status']=='rejected'){ ?>
										<span class="badge bg-soft-danger text-danger">
											<span class="legend-indicator bg-danger"></span>rejected
										</span>
									<?php }elseif($row['approval_status']=='pending'){ ?>
										<span class="badge bg-soft-warning text-warning">
											<span class="legend-indicator bg-warning"></span>pending
										</span>
									<?php } ?>
								</td>
								<td>
                                    <?php if($row['approval_status']=='pending') { ?>
                                    <a href="<?= site_url('manage/book_request/request_approved/' . $row['book_request_id']) ?>" 
                                        class="btn btn-xs btn-success" 
                                        data-toggle="tooltip" title="Approved"><i class="fa fa-check"></i>
                                    </a>
                                    <a href="<?= site_url('manage/book_request/request_rejected/' . $row['book_request_id']) ?>" 
                                        class="btn btn-xs btn-danger" 
                                        data-toggle="tooltip" title="Rejected"><i class="fa fa-close"></i>
                                    </a>
                                    <?php } ?>
								</td>
							</tr>
							<?php
								$i++;
								endforeach;
								} else {
							?>
							<tr id="row">
								<td colspan="5" align="center">Data Kosong</td>
							</tr>
							<?php } ?>							
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>

	</section>
</div>

<!-- Modal Add Data -->
<div class="modal fade" id="modalAddData" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Book Request</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>						
            </div>
            <?= form_open('member/akun/add_book_request', array('method'=>'post')); ?>
            <div class="modal-body">
                <input type="hidden" readonly name="member_id" class="form-control" value="<?= $this->session->userdata('uid_member') ?>">            
                <div id="p_scents_class">
                    <p>
                        <div class="form-group mb-3">
                            <label>Request Date</label>
                            <div id="payoutFlatpickr" class="js-flatpickr flatpickr-custom input-group"
                                data-hs-flatpickr-options='{
                                    "appendTo": "#payoutFlatpickr",
                                    "dateFormat": "Y-m-d",
                                    "wrap": true
                                }'>
                                <div class="input-group-prepend input-group-text" data-bs-toggle>
                                    <i class="bi-calendar-week"></i>
                                </div>
                                <input type="text" name="request_date" class="flatpickr-custom-form-control form-control" id="payoutFlatpickrLabel" placeholder="Select dates" data-input value="<?= date('Y-m-d') ?>" required>
                            </div>
                        </div> 
                        <div class="form-group mb-3">
                            <label>Nama Buku</label>
                            <div class="tom-select-custom tom-select-custom-with-tags">
                                <select class="js-select form-select" 
                                        name="book_id" 
                                        id="book_id"
                                        data-hs-tom-select-options='{
                                            "placeholder": "Select a book..."
                                        }'>
                                    <?php if (count($books)): ?>                                     
                                    <?php foreach ($books as $row) : ?>                                           
                                        <option value="<?= $row['book_id'] ?>" 
                                                data-icon="<?= $row['book_id'] ?>"
                                                data-option-template='<span class="d-flex align-items-center">
                                                <img class="avatar avatar-xss avatar-circle me-2" src="<?= site_url() ?>media/front/assets/img/400x400/img22.jpg" alt="<?= $row['title'] ?>" />
                                                                        <span class="text-truncate"> <?= $row['title'] ?></span>
                                                                    </span>'>
                                                <?= $row['title'] ?>
                                        </option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>  
                                </select>
                            </div>
                        </div>            
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
</div>
<!-- End modal add data -->

<script>
$(document).ready(function() {
    $('#dtable').DataTable({
        order: [[6, 'desc']] // Set the default order based on column index 2 (ascending)
    });
});    
</script>