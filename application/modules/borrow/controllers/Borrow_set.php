<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Borrow_set extends CI_Controller {
  public function __construct() {
    parent::__construct(TRUE);
    if ($this->session->userdata('logged') == NULL) {
      header("Location:" . site_url('manage/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
    }
    $this->load->model(array('period/Period_model',
                            'setting/Setting_model', 
                            'logs/Logs_model',
                            'borrow/Borrow_model',
                            'member/Member_model',
                            'book/Book_model'
                          ));
  }

  // borrow view in list
  public function index($offset = NULL, $id =NULL) {
    // Apply Filter
    $f = $this->input->get(NULL, TRUE);
    $data['f'] = $f;
    $params = array();
    $logs   = array();
    $params['period_id'] = NULL;
    $params['member_id'] = NULL;

    // Period
    if (isset($f['n']) && !empty($f['n']) && $f['n'] != '') {
      $params['period_id']  = $f['n'];
      $logs['period_id']    = $f['n'];
    }
    // Member
    if (isset($f['r']) && !empty($f['r']) && $f['r'] != '') {
      $params['member_id']  = $f['r'];
      $logs['member_id']    = $f['r'];
    }

    $data['period']   = $this->Period_model->get(array('status'=>1));
    $data['members']  = $this->Member_model->get(array('is_active'=>1));    
    $data['member']   = $this->Member_model->get(array('member_id'=>$params['member_id']));
    $data['borrow']   = $this->Borrow_model->get(array('member_id'=>$params['member_id'], 'limit' => 5));
    $data['borrowings'] = $this->Borrow_model->get(array('status'=>'borrowed', 'member_id'=>$params['member_id']));

    $data['title'] = 'Pengembalian';
    $data['main']  = 'borrow/borrow_list';
    $this->load->view('manage/layout', $data);

  } 

  function proses_return() {    
    
    $period_id = $this->input->post('period_id');
    $member_id = $this->input->post('member_id');

    $borrowing = array(
      'borrowing_id' => $this->input->post('borrowing_id'),
      'return_date' => $this->input->post('return_date'),
      'borrow_note' => $this->input->post('borrow_note'),
      'status' => 'returned'
    );

    $status = $this->Borrow_model->add_borrowings($borrowing);

    /** update stock buku */
    $current_stock = $this->Book_model->get(array('book_id'=>$this->input->post('book_id')))['stock'];

    $books = array(
                    'book_id' => $this->input->post('book_id'),
                    'stock' => $current_stock+1
                  );

    $update_book = $this->Book_model->add($books);
    
    if ($this->input->is_ajax_request()) {
      echo $status;
    } else {
      $this->session->set_flashdata('success', 'Update Data Berhasil');

      redirect('manage/borrow?n='.$period_id.'&r='.$member_id);
    }
  }

  function borrow_summary() {
    $this->load->helper(array('dompdf'));
    $f = $this->input->get(NULL, TRUE);
    $data['f'] = $f;
    $param = array();

    // Tahun Ajaran
    if (isset($f['n']) && !empty($f['n']) && $f['n'] != '') {
      $param['period_id'] = $f['n'];
    }
    // Member
    if (isset($f['r']) && !empty($f['r']) && $f['r'] != '') {
      $param['member_id'] = $f['r'];
    }
    // tanggal
    if (isset($f['d']) && !empty($f['d']) && $f['d'] != '') {
      $param['return_date'] = $f['d'];
    }
   
    $data['period'] = $this->Period_model->get();
    $data['member'] = $this->Member_model->get(array('member_id'=>$f['r']));
    $data['borrow']   = $this->Borrow_model->get(array('member_id'=>$f['r'], 'return_date' => $f['d']));
    
    $data['setting_district'] = $this->Setting_model->get(array('id' => SCHOOL_DISTRICT)); 
    $data['setting_school']   = $this->Setting_model->get(array('id' => SCHOOL_NAME)); 
    $data['setting_address']  = $this->Setting_model->get(array('id' => SCHOOL_ADRESS)); 
    $data['setting_phone']    = $this->Setting_model->get(array('id' => SCHOOL_PHONE)); 

    $html = $this->load->view('borrow/borrow_summary_pdf', $data, true);
    $data = pdf_create($html, 'RIWAYAT_TRANSAKSI_'.$data['member']['member_name'].'_'.date('Y-m-d'), TRUE, 'A4', TRUE);
  }

}