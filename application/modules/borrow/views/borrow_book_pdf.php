<html>
<head>
    <title>BUKU TABUNGAN - <?= $siswa['student_full_name'] ?></title>
  <style type="text/css">
  .upper { text-transform: uppercase; }
  .lower { text-transform: lowercase; }
  .cap   { text-transform: capitalize; }
  .small { font-variant:   small-caps; }
</style>
<style type="text/css">
@page {
  margin-top: 0.5cm;
  margin-bottom: 0.1em;
  margin-left: 5.0em;
  margin-right: 5.0em;
  } .style12 {font-size: 10px}
  .style13 {
   font-size: 14pt;
   font-weight: bold;
 }
 .name-school{
	font-size: 15pt;
	font-weight: bold;
	padding-bottom: -15px;
}
.alamat{
	font-size: 9pt;
	margin-bottom: -10px;
}
hr {
	border: none;
	height: 1px;
	/* Set the hr color */
	color: #333; /* old IE */
	background-color: #333; /* Modern Browsers */
}
 .title{
  font-size: 14pt;
  text-align: center;
  font-weight: bold;
  padding-bottom: -10px;
}
.tp{
  font-size: 12pt;
  text-align: center;
  font-weight: bold;
}
body {
  font-family: sans-serif;
}
table {
  border-collapse: collapse;
  font-size: 9pt;
  width: 100%;
}
</style>
</head>
<body>

  <p class="name-school"><?= $setting_school['setting_value'] ?></p>
	<p class="alamat"><?= $setting_address['setting_value'] ?><br>
		<?= $setting_phone['setting_value'] ?></p>
		<hr>

  <p class="title">RINCIAN TABUNGAN SISWA</p>
  <p class="tp">Unit Sekolah : <?= $siswa['unit_name'] ?></p>

  <table style="font-size: 10pt;" width="100%" border="0">
    <tr>
      <td width="100">NIS</td>
      <td width="5">:</td>

        <td width=""><?= $siswa['student_nis'] ?></td>
    </tr>
    <tr>
      <td>Nama</td>
      <td>:</td>
        <td><?= $siswa['student_full_name'] ?></td>
    </tr>
    <tr>
      <td>Kelas</td>
      <td>:</td>
        <td><?= $siswa['class_name'] ?></td>
    </tr>
    <tr>
      <td>Tahun Ajaran</td>
      <td>:</td>
        <td><?= $period['period_start'].'/'.$period['period_end'] ?></td>
    </tr>

    </table><br>
    <?php if ($f['n'] AND $f['r'] != NULL) { ?> 

      <table width="100%" border="1" style="white-space: nowrap;">
        <tr>
          <th style="height: 30px;">NO</th>
          <th>TANGGAL</th>
          <th>KODE</th>
          <th>CATATAN</th>
          <th>DEBIT</th>
          <th>KREDIT</th>
          <th>SALDO</th>
          <th>KASIR</th>
        </tr>
        <?php 
          $i = 1;
          foreach ($banking as $row) :
        ?>
          <tr>
            <td style="text-align: center;"><?= $i ?></td>
            <td><?= $row['trans_at'] ?></td>
            <td><?= $row['code'] ?></td>
            <td><?= $row['note'] ?></td>
            <td><?= 'Rp. ' .number_format($row['debit_val']) ?></td>
            <td><?= 'Rp. ' .number_format($row['credit_val']) ?></td>
            <td><?= 'Rp. ' .number_format($row['balance']) ?></td>
            <td><?= $row['created_by'] ?></td>
          </tr>
          <?php 
          $i++;
        endforeach
        ?>
        
      </table>
      <?php } else redirect('manage/banking?n='.$f['n'].'&r='.$f['r'])  ?>

      <table style="width:100%; margin-top: 50px; font-size: 10pt; ">
        <tr>
          <td><span class="cap"><?= $setting_district['setting_value'] ?></span>, <?= pretty_date(date('Y-m-d'),'d F Y',false) ?></td>
        </tr>
        <tr>
          <td>Kepala Tata Usaha</td>
        </tr>

      </table>
      <br><br><br><br>
      <table width="100%" style="font-size: 10pt;">
        <tr>
          <td><strong><u><span class="upper">( <?= $this->session->userdata('ufullname'); ?> )</span></u></strong></td>
        </tr>
      </table>


    </body>
    </html>