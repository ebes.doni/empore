<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_member extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if ($this->session->userdata('logged_member') == NULL) {
            header("Location:" . site_url('member/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model(array('member/Member_model',  
                                'setting/Setting_model',
                                'information/Information_model'
                            ));
    }

    public function index() {
        // $this->output->enable_profiler(TRUE);
        $id = $this->session->userdata('uid'); 
        $data['information'] = $this->Information_model->get(array('information_publish'=>1));

        $data['title'] = 'Dashboard';
        $data['main'] = 'dashboard/dashboard_member';
        $this->load->view('member/layout', $data);
    }


}
