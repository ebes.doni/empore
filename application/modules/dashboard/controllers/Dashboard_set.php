<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_set extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('manage/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model(array('users/Users_model', 
                                'setting/Setting_model',
                                'information/Information_model', 
                                'book_request/Book_request_model', 
                                'modul/Modul_model',
                                'borrow/Borrow_model',
                                'book/Book_model',
                                'member/Member_model'
                            ));
        
        $this->load->library('user_agent');
        $this->load->helper('greetings');
    }

    public function index() {
        $id = $this->session->userdata('uid'); 
        
        $data['setting_logo']   = $this->Setting_model->get(array('id' => 6));
        $data['book_request']   = count($this->Book_request_model->get());
        $data['borrowings']     = count($this->Borrow_model->get(array('status'=>'returned')));
        $data['books']          = count($this->Book_model->get());
        $data['members']        = count($this->Member_model->get());

        $data['title']              = 'Dashboard';
        $data['main']               = 'dashboard/dashboard';
        $this->load->view('manage/layout', $data);

    }

    public function get() {
        $events = $this->Holiday_model->get();
        foreach ($events as $i => $row) {
            $data[$i] = array(
                'id' => $row['id'],
                'title' => strip_tags($row['info']),
                'start' => $row['date'],
                'end' => $row['date'],
                'year' => $row['year'],
            );
        }
        echo json_encode($data, TRUE);
    }

}
