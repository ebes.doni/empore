<div class="">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
  </section>

  <!-- Main content -->
  <section class="content container-fluid">
    <div class="row">
      <div class="col-md-6">
       <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Informasi</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators --> 
              <ol class="carousel-indicators ind"> 
                <li data-bs-target="#carousel-example-generic" data-slide-to="0" class="active"></li> 
                <li data-bs-target="#carousel-example-generic" data-slide-to="1"></li> 
                <li data-bs-target="#carousel-example-generic" data-slide-to="2"></li> 
              </ol> 
              <!-- Wrapper for slides --> 
              <div class="carousel-inner"> 
                <?php
                $i = 1;
                foreach ($information as $row):
                  ?>
                  <div class="item <?= ($i == 1) ? 'active' : ''; ?>"> 
                    <div class="row"> 
                        <div class="adjust1"> 
                          <a href="#showDetail<?= $row['information_id']?>" 
                              data-bs-toggle="modal">                        
                            <!-- <a href="<?= site_url('manage/information')?>"> -->
                              <div class="caption"> 
                                <p class="text-info lead adjust2">
                                <img src="<?= base_url() ?>uploads/information/<?= $row['information_img']?>" width="100%">
                                  <?= $row['information_title'] ?></p>  
                                <blockquote class="adjust2"> <p><?= strip_tags(character_limiter($row['information_desc'], 250)) ?></p> 
                                </blockquote> 
                              </div> 
                          </a>
                        </div> 
                    </div> 
                  </div> 
                  <?php
                  $i++;
                endforeach;
                ?>
              </div> <!-- Controls --> 
              <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"> 
                <span class="glyphicon glyphicon-chevron-left" style="font-size:20px"></span> </a> 
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"> 
                  <span class="glyphicon glyphicon-chevron-right" style="font-size:20px"></span> 
                </a> 
              </div> 
            </div>

        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="fa fa-dollar"></i></span>

          <div class="info-box-content">
            <span class="info-box-text dash-text">Sisa Tagihan Bulanan</span>
            <span class="info-box-number"></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>

          <div class="info-box-content">
            <span class="info-box-text dash-text">Sisa Tagihan Lainnya</span>
            <span class="info-box-number"></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      
    </div>
    <div style="margin-bottom: 50px;"></div>

  </section>
  <!-- /.content -->
</div>

<!-- show detail banner -->
<?php foreach ($information as $row): ?>
  <div class="modal modal-default fade" id="showDetail<?= $row['information_id']?>">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title"><span class="fa fa-info"></span> Detail Informasi</h3>
          </div>
          <div class="modal-body">
              <p class="text-info lead adjust2">
              <img src="<?= base_url() ?>uploads/information/<?= $row['information_img']?>" width="100%">
                <?= $row['information_title'] ?></p>  
              <blockquote class="adjust2"> <p><?= $row['information_desc'] ?></p> 
              </blockquote> 
          </div>
          <div class="modal-footer">            
            <button type="button" class="btn btn-default pull-left" data-bs-dismiss="modal"><span class="fa fa-close"></span> Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  </div>
<?php endforeach ?>
<!-- end show detail -->