<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Logs controllers Class
 *
 * @package     26 Jan 2023
 * @subpackage  Controllers
 * @category    Controllers 
 * @author      Doni Ebes
 */
class Logs_set extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('manage/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model('logs/Logs_model');
    }

    public function index($offset = NULL) {
        $q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

        // Date start
		if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
			$params['date_start'] = $q['ds'];
		}
        // Date end
		if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
			$params['date_end'] = $q['de'];
		}
		// Modul
		if (isset($q['m']) && !empty($q['m']) && $q['m'] != '') {
			$params['log_module'] = $q['m'];
		}

        $data['logs'] = $this->Logs_model->get($params);
        $data['modul'] = $this->Logs_model->get_modul();
        $data['title'] = 'Log Aktifitas';
        $data['main'] = 'logs/log_list';
        $this->load->view('manage/layout', $data);
    }

}
