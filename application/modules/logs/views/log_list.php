<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title"></h3>	
						<div class="mb-4">
							<?= form_open(current_url(), array('method' => 'get')) ?> <br>
								<div class="row">							

									<div class="col-md-2 mb-2">  
										<div class="form-group">
											<!-- <div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
												<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
												<input class="form-control" type="text" name="ds" id="dari_tanggal" readonly="readonly" placeholder="Dari" <?php echo (isset($q['ds'])) ? 'value="'.$q['ds'].'"' : '' ?>>
											</div> -->
											<div id="dsFlatpickr" class="js-flatpickr flatpickr-custom input-group"
												data-hs-flatpickr-options='{
													"appendTo": "#dsFlatpickr",
													"dateFormat": "Y-m-d",
													"wrap": true
												}'>
												<div class="input-group-prepend input-group-text" data-bs-toggle>
													<i class="bi-calendar-week"></i>
												</div>
												<input class="flatpickr-custom-form-control form-control" 
														type="text" 
														name="ds" 
														id="dari_tanggal" 
														placeholder="Tanggal Awal" 
														data-input <?= (isset($q['ds'])) ? 'value="'.$q['ds'].'"' : '' ?> 
														required>
											</div>
										</div>
									</div>

									<div class="col-md-2 mb-2">  
										<div class="form-group">
											<!-- <div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
												<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
												<input class="form-control" type="text" name="de" id="sampai_tanggal" readonly="readonly" placeholder="Sampai" <?= (isset($q['de'])) ? 'value="'.$q['de'].'"' : '' ?>>	
											</div> -->
											<div id="deFlatpickr" class="js-flatpickr flatpickr-custom input-group"
												data-hs-flatpickr-options='{
													"appendTo": "#deFlatpickr",
													"dateFormat": "Y-m-d",
													"wrap": true
												}'>
												<div class="input-group-prepend input-group-text" data-bs-toggle>
													<i class="bi-calendar-week"></i>
												</div>
												<input class="flatpickr-custom-form-control form-control" 
														type="text" 
														name="de" 
														id="sampai_tanggal" 
														placeholder="Tanggal Akhir" 
														data-input <?= (isset($q['de'])) ? 'value="'.$q['de'].'"' : '' ?> 
														required>
											</div>
										</div>
									</div>

									<div class="col-md-3 mb-2">  
										<div class="form-group">
											<select class="form-control form-select" name="m">
												<option value="">Semua Modul</option>
												<?php if(isset($modul)) foreach($modul as $row): ?>
												<option value="<?= $row['modul'] ?>" <?php if(isset($q['m'])) if($q['m'] == $row['modul']) echo 'selected' ?>><?= $row['modul'] ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>

									<div class="col-md-2 mb-2">  
										<div class="form-group">
											<button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Cari</button>
										</div>
									</div>
								
								</div>
							<?= form_close() ?>	

						</div>
					</div>

					<!-- /.card-header -->
					<div class="card-body table-responsive">
						<table id="dtable" class="table table-hover">
							<thead class="bg-soft-dark">
							<tr>
								<th>No</th>
								<th>Tanggal</th>
								<th>Modul</th>
								<th>Aksi</th>
								<th>Info</th>
								<th>Penulis</th>
							</tr>
							</thead>
							<tbody>
								<?php
								if (!empty($logs)) {
									$i = 1;
									foreach ($logs as $row):
										?>
										<tr>
											<td><?= $i; ?></td>
											<td><?= pretty_date($row['log_date'],'d M Y h:m:s',false) ?></td>
											<td><?= $row['log_module']; ?></td>
											<td><?= $row['log_action']; ?></td>
											<td><?= $row['log_info']; ?></td>
											<td><?= $row['user_full_name']; ?></td>	
										</tr>
										<?php
										$i++;
									endforeach;
								} else {
									?>
									<tr id="row">
										<td colspan="6" align="center">Data Kosong</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
		</section>
		<!-- /.content -->
	</div>