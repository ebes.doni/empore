
<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-6">
				<div class="card card-warning">
					<div class="card-header">
						<div class="col-md-6">
							<a href="<?= site_url('manage/maintenance/backup') ?>">
							<i class="fa fa-database" style="color:#03C9A9; font-size: 90pt"></i><br>
							<span class="h5">Backup Database</span></a>
						</div>
						<div class="col-md-6">
							<div class="alert alert-danger pull-left">
								Warning!... !<br>
								Halaman ini digunakan untuk membackup seluruh database
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
	<!-- /.content -->
</div>