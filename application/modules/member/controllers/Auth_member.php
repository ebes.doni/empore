<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth_member extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('member/Member_model');
        $this->load->model('setting/Setting_model');
        $this->load->library('form_validation');
        $this->load->helper('string');
    }

    function index() {
        redirect('member/auth/login');
    }

    function login() {
        if ($this->session->userdata('logged_member')) {
            redirect('member');
        }
        if ($this->input->post('location')) {
            $location = $this->input->post('location');
        } else {
            $location = NULL;
        }
        $this->form_validation->set_rules('member_id', 'ID', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($_POST AND $this->form_validation->run() == TRUE) {
            $member_id = $this->input->post('member_id', TRUE);
            $password = $this->input->post('password', TRUE);

            $member = $this->Member_model->get(array('member_id' => $member_id, 'password' => sha1($password)));

            if (count($member) > 0) {
                $this->session->set_userdata('logged_member', TRUE);
                $this->session->set_userdata('uid_member', $member['member_id']);
                $this->session->set_userdata('ufullname_member', $member['member_name']);
                $this->session->set_userdata('member_img', $member['member_img']);
                if ($location != '') {
                    header("Location:" . htmlspecialchars($location));
                } else {
                    redirect('member');
                }
            } else {
                if ($location != '') {
                    $this->session->set_flashdata('failed', 'Maaf, ID dan password tidak cocok!');
                    header("Location:" . site_url('member/auth/login') . "?location=" . urlencode($location));
                } else {
                    $this->session->set_flashdata('failed', 'Maaf, ID dan password tidak cocok!');
                    redirect('member/auth/login');
                }
            }
        } else {
            $data['setting_school'] = $this->Setting_model->get(array('id'=>1));
            $data['setting_logo'] = $this->Setting_model->get(array('id'=>SCHOOL_LOGO));
            $this->load->view('member/login', $data);
        }
    }

    // Logout Processing
    function logout() {
        $this->session->unset_userdata('logged_member');
        $this->session->unset_userdata('uid_member');
        $this->session->unset_userdata('ufullname_member');
        $this->session->unset_userdata('member_img');

        $q = $this->input->get(NULL, TRUE);
        if ($q['location'] != NULL) {
            $location = $q['location'];
        } else {
            $location = NULL;
        }
        header("Location:" . $location);
    }

}
