<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_set extends CI_Controller {
  public function __construct(){
    parent::__construct();
    if ($this->session->userdata('logged') == NULL) {
      header("Location:" . site_url('manage/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
    }
    $this->load->model(array('member/Member_model', 
                            'setting/Setting_model'
                          ));
    $this->load->helper(array('form', 'url'));
  }

  // student view in list
  public function index($offset = NULL) {
    // Apply Filter
    $f = $this->input->get(NULL, TRUE);
    $data['f'] = $f;
    $params = array();
        
    $data['member'] = $this->Member_model->get($params);
    $data['title'] = 'Anggota';
    $data['main'] = 'member/member_list';
    $this->load->view('manage/layout', $data);
  }

  // View data detail
  public function view($id = NULL) {
    $data['member'] = $this->Member_model->get(array('member_id' => $id));
    $data['title'] = 'Anggota';
    $data['main'] = 'member/member_view';
    $this->load->view('manage/layout', $data);

  }

  // Delete to database
  public function delete($id = NULL) {
    if ($this->session->userdata('uroleid')!= SUPERUSER){
        $this->session->set_flashdata('failed', 'Anda tidak mempunyai hak akses untuk menghapus data');
        redirect('manage/member');
    }
    if ($_POST) {
      
      if($this->input->post('member_id')){
        $id = $this->input->post('member_id');
      }else{
        $id = $id;
      }
      // $bulan = $this->Bulan_model->get(array('student_id' => $this->input->post('student_id')));

      // if (count($bulan)>0) {
      //   $this->session->set_flashdata('failed', 'Siswa tidak dapat dihapus');
      //   redirect('manage/student');
      // }

      $this->Member_model->delete($id);

      // activity log
      $this->load->model('logs/Logs_model');
      $this->Logs_model->add(
        array(
          'log_date' => date('Y-m-d H:i:s'),
          'user_id' => $this->session->userdata('uid'),
          'log_module' => 'member',
          'log_action' => 'Hapus',
          'log_info' => 'ID:' . $id . ';Title:' . $this->input->post('delName')
        )
      );

      $this->session->set_flashdata('success', 'Hapus data berhasil');
      redirect('manage/member');

    } elseif (!$_POST) {

      $this->session->set_flashdata('delete', 'Delete');
      redirect('manage/member/edit/' . $id);

    }

  }
  
  // Add User and Update
  public function add($id = NULL) {
    $this->load->library('form_validation');
    if (!$this->input->post('member_id')) {
      $this->form_validation->set_rules('member_password', 'Password', 'trim|required|xss_clean|min_length[6]');
      $this->form_validation->set_rules('passconf', 'Konfirmasi password', 'trim|required|xss_clean|min_length[6]|matches[member_password]');
      $this->form_validation->set_message('passconf', 'Password dan konfirmasi password tidak cocok');
    }
    
    $this->form_validation->set_rules('member_name', 'Nama lengkap', 'trim|required|xss_clean');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button position="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
    $data['operation'] = is_null($id) ? 'Tambah' : 'Sunting';

    if ($_POST AND $this->form_validation->run() == TRUE) {

      if ($this->input->post('member_id')) {
        $params['member_id'] = $id;
      } else {
        $params['member_password'] = sha1($this->input->post('member_password'));
      }
   
      $params['member_name']    = $this->input->post('member_name');
      $params['member_gender']  = $this->input->post('member_gender');
      $params['member_email']   = $this->input->post('member_email');
      $params['member_address'] = $this->input->post('member_address');
      $params['member_phone']   = $this->input->post('member_phone');
      
      $status = $this->Member_model->add($params);

      if (!empty($_FILES['member_img']['name'])) {
          $paramsupdate['member_img'] = $this->do_upload($name = 'member_img', $fileName= $params['member_name']);
      } 

      $paramsupdate['member_id'] = $status;
      $this->Member_model->add($paramsupdate);

      // activity log
      $this->load->model('logs/Logs_model');
      $this->Logs_model->add(
        array(
          'log_date' => date('Y-m-d H:i:s'),
          'user_id' => $this->session->userdata('uid'),
          'log_module' => 'member',
          'log_action' => $data['operation'],
          'log_info' => 'ID:' . $status . ';Name:' . $this->input->post('member_name')
        )
      );

      $this->session->set_flashdata('success', $data['operation'] . ' Siswa Berhasil');
      redirect('manage/member');
    } else {
      if ($this->input->post('member_id')) {
        redirect('manage/member/edit/' . $this->input->post('member_id'));
      }

    // Edit mode
      if (!is_null($id)) {
        $object = $this->Member_model->get(array('member_id' => $id));
        if ($object == NULL) {
          redirect('manage/member');
        } else {
          $data['member'] = $object;
        }
      }

      $data['title'] = $data['operation'] . ' Anggota';
      $data['main'] = 'member/member_add';
      $this->load->view('manage/layout', $data);
    }

  }

  function rpw($id = NULL) {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('member_password', 'Password', 'trim|required|xss_clean|min_length[6]');
    $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|xss_clean|min_length[6]|matches[member_password]');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
    if ($_POST AND $this->form_validation->run() == TRUE) {
      $id = $this->input->post('member_id');
      $params['member_password'] = sha1($this->input->post('member_password'));
      $status = $this->Member_model->change_password($id, $params);
  
      $this->session->set_flashdata('success', 'Reset Password Berhasil');
      redirect('manage/member');
    } else {
      if ($this->Member_model->get(array('member_id' => $id)) == NULL) {
        redirect('manage/member');
      }
      $data['member'] = $this->Member_model->get(array('member_id' => $id));
      $data['title'] = 'Reset Password';
      $data['main'] = 'member/change_pass';
      $this->load->view('manage/layout', $data);
    }
  }
  
  public function pass($offset = NULL) {
    $f = $this->input->get(NULL, TRUE);
    $data['f'] = $f;
    $params = array();
    // Kelas
    if (isset($f['pr']) && !empty($f['pr']) && $f['pr'] != '') {
      $params['class_id'] = $f['pr'];
    }
    // Unit
    if (isset($f['u']) && !empty($f['u']) && $f['u'] != '') {
      $params['unit_id'] = $f['u'];
    }
  
    $paramsPage = $params;
    $params['status'] = TRUE;
    $params['offset'] = $offset;
    $config['base_url'] = site_url('manage/student/index');
    $config['suffix'] = '?' . http_build_query($_GET, '', "&");
    $config['total_rows'] = count($this->Student_model->get($paramsPage));
  
    $data['notpass'] = $this->Student_model->get($params);
    $data['pass'] = $this->Student_model->get(array('status'=>0, 'student_move'=>0));
    $data['class'] = $this->Student_model->get_class($params);
    $data['unit'] = $this->Unit_model->get();
  
    $data['title'] = 'Kelulusan Siswa';
    $data['main'] = 'student/student_pass';
    $this->load->view('manage/layout', $data);
  }

// single print pdf
function printPdf($id = NULL) {
  $this->load->helper(array('dompdf'));
  $this->load->helper(array('tanggal'));
  
  if ($id == NULL)
    redirect('manage/member');

    $data['setting_school'] = $this->Setting_model->get(array('id' => SCHOOL_NAME));
    $data['setting_address'] = $this->Setting_model->get(array('id' => SCHOOL_ADRESS));
    $data['setting_phone'] = $this->Setting_model->get(array('id' => SCHOOL_PHONE));
    $data['setting_district'] = $this->Setting_model->get(array('id' => SCHOOL_DISTRICT));
    $data['setting_city'] = $this->Setting_model->get(array('id' => SCHOOL_CITY)); 
    $data['member'] = $this->Member_model->get(array('member_id' => $id));
    $this->barcode2($data['member']['member_id'], '');
    $html = $this->load->view('member/member_pdf', $data, true);
    $data = pdf_create($html, $data['member']['member_name'], TRUE, 'A4', 'potrait');
}

private function barcode2($sparepart_code, $barcode_type=39, $scale=6, $fontsize=1, $thickness=30,$dpi=72) {

  $this->load->library('upload');
  $config['upload_path'] = FCPATH . 'media/barcode_member/';

  /* create directory if not exist */
  if (!is_dir($config['upload_path'])) {
    mkdir($config['upload_path'], 0777, TRUE);
  }
  $this->upload->initialize($config);

  // CREATE BARCODE GENERATOR
  // Including all required classes
  require_once( APPPATH . 'libraries/barcodegen/BCGFontFile.php');
  require_once( APPPATH . 'libraries/barcodegen/BCGColor.php');
  require_once( APPPATH . 'libraries/barcodegen/BCGDrawing.php');

  // Including the barcode technology
  // Ini bisa diganti-ganti mau yang 39, ato 128, dll, liat di folder barcodegen
  require_once( APPPATH . 'libraries/barcodegen/BCGcode39.barcode.php');

  // Loading Font
  // kalo mau ganti font, jangan lupa tambahin dulu ke folder font, baru loadnya di sini
  $font = new BCGFontFile(APPPATH . 'libraries/font/Arial.ttf', $fontsize);

  // Text apa yang mau dijadiin barcode, biasanya kode produk
  $text = $sparepart_code;

  // The arguments are R, G, B for color.
  $color_black = new BCGColor(0, 0, 0);
  $color_white = new BCGColor(255, 255, 255);

  $drawException = null;
  try {
        $code = new BCGcode39(); // kalo pake yg code39, klo yg lain mesti disesuaikan
        $code->setScale($scale); // Resolution
        $code->setThickness($thickness); // Thickness
        $code->setForegroundColor($color_black); // Color of bars
        $code->setBackgroundColor($color_white); // Color of spaces
        $code->setFont($font); // Font (or 0)
        $code->parse($text); // Text
      } catch(Exception $exception) {
        $drawException = $exception;
      }

    /* Here is the list of the arguments
    1 - Filename (empty : display on screen)
    2 - Background color */
    $drawing = new BCGDrawing('', $color_white);
    if($drawException) {
      $drawing->drawException($drawException);
    } else {
      $drawing->setDPI($dpi);
      $drawing->setBarcode($code);
      $drawing->draw();
    }
    // ini cuma labeling dari sisi aplikasi saya, penamaan file menjadi png barcode.
    $filename_img_barcode = $sparepart_code .'_'.$barcode_type.'.png';
    // folder untuk menyimpan barcode
    $drawing->setFilename( FCPATH .'media/barcode_member/'. $sparepart_code.'.png');
    // proses penyimpanan barcode hasil generate
    $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);

    return $filename_img_barcode;

  }

  // Setting Upload File Requied
  function do_upload($name=NULL, $fileName=NULL) {
    $this->load->library('upload');
    $config['upload_path'] = FCPATH . 'uploads/member/';

    /* create directory if not exist */
    if (!is_dir($config['upload_path'])) {
      mkdir($config['upload_path'], 0777, TRUE);
    }

    $config['allowed_types'] = 'gif|jpg|jpeg|png';
    $config['max_size'] = '1024';
    $config['file_name'] = $fileName;
    $this->upload->initialize($config);

    if (!$this->upload->do_upload($name)) {
      $this->session->set_flashdata('success', $this->upload->display_errors('', ''));
      redirect(uri_string());
    }

    $upload_data = $this->upload->data();
    return $upload_data['file_name'];
  }

  public function download() {
    if (majors()=='senior') {
      $data = file_get_contents("./media/template_excel/Template_Data_Siswa_Senior.xls");
      $name = 'Template_Data_Siswa_Senior.xls';
    } else {
      $data = file_get_contents("./media/template_excel/Template_Data_Siswa_Primary.xls");
      $name = 'Template_Data_Siswa_Primary.xls.xls';
    }
  
  $this->load->helper('download');
    force_download($name, $data);
  }

  public function import() {
    if ($_POST) {
      $rows= explode("\n", $this->input->post('rows'));
      $success = 0;
      $failled = 0;
      $exist = 0;
      $nis = '';
      foreach($rows as $row) {
        $exp = explode("\t", $row);
        $nis = trim($exp[0]);
        $ttl = trim($exp[5]);
        $date = str_replace('-', '',$ttl); 
        $arr = [
          'student_nis' => trim($exp[0]),
          'student_nisn' => trim($exp[1]),
          'student_password' => sha1('123456'),
          'student_full_name' => trim($exp[2]),
          'student_gender' => trim($exp[3]),
          'student_born_place' => trim($exp[4]),
          'student_born_date' => trim($exp[5]),
          'student_hobby' => trim($exp[6]),
          'student_phone' => trim($exp[7]),
          'student_address' => trim($exp[8]),
          'student_name_of_mother' => trim($exp[9]),
          'student_name_of_father' => trim($exp[10]),
          'student_parent_phone' => trim($exp[11]),
          'class_class_id' => trim($exp[12]),
          'unit_id' => trim($exp[13]),
          'student_input_date' => date('Y-m-d H:i:s'),
          'student_last_update' => date('Y-m-d H:i:s')
        ];
        $class = $this->Student_model->get_class(array('id'=>trim($exp[12])));
        
        $check = $this->db->where('student_nis', trim($exp[0]))->count_all_results('student');
        if ($check == 0) {
          if (trim($exp[12]) == NULL OR is_null($class)) {
            $this->session->set_flashdata('failed', 'ID Kelas tidak ada');
            redirect('manage/student/import');           
          } else if ($this->db->insert('student', $arr)) {
            $success++;
          } else {
            $failled++;
          }
      } else {
        $exist++;
      }
    }
      $msg = 'Sukses : ' . $success. ' baris, Gagal : '. $failled .', Duplikat : ' . $exist;
      $this->session->set_flashdata('success', $msg);
      redirect('manage/student/import');

    } else {

      $data['title']  = 'Import Data';
      $data['main']   = 'member/member_upload';
      $data['action'] = site_url(uri_string());
      $this->load->view('manage/layout', $data);

    }

  }

  ##############


  public function move($offset = NULL) {
      $f = $this->input->get(NULL, TRUE);
      $data['f'] = $f;
      $params = array();

      // Unit
      if (isset($f['u']) && !empty($f['u']) && $f['u'] != '') {
        $params['unit_id'] = $f['u'];
      }

      // Kelas
      if (isset($f['pr']) && !empty($f['pr']) && $f['pr'] != '') {
        $params['class_id'] = $f['pr'];
      }

      $paramsPage = $params;
      $params['status'] = TRUE;
      $params['offset'] = $offset;
      $config['base_url'] = site_url('manage/student/index');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['total_rows'] = count($this->Student_model->get($paramsPage));

      $data['notmove'] = $this->Student_model->get($params);
      $data['move'] = $this->Student_model->get(array('student_move'=>1));
      $data['class'] = $this->Student_model->get_class($params);
      $data['unit'] = $this->Unit_model->get();

      $data['title'] = 'Proses Siswa Pindah';
      $data['main'] = 'student/student_move';
      $this->load->view('manage/layout', $data);
  }

public function upgrade($offset = NULL) {
    $f = $this->input->get(NULL, TRUE);
    $data['f'] = $f;
    $params = array();
    // Kelas
    if (isset($f['pr']) && !empty($f['pr']) && $f['pr'] != '') {
      $params['class_id'] = $f['pr'];
    }
    // Unit
    if (isset($f['u']) && !empty($f['u']) && $f['u'] != '') {
      $params['unit_id'] = $f['u'];
    }

    $params['status'] =1;
    $paramsPage = $params;
    $params['offset'] = $offset;
    $config['base_url'] = site_url('manage/student/index');
    $config['suffix'] = '?' . http_build_query($_GET, '', "&");
    $config['total_rows'] = count($this->Student_model->get($paramsPage));

    $data['student'] = $this->Student_model->get($params);
    $data['class'] = $this->Student_model->get_class($params);
    $data['unit'] = $this->Unit_model->get();
    $data['upgrade'] = $this->Student_model->get_class();

    $data['title'] = 'Kenaikan Kelas';
    $data['main'] = 'student/student_upgrade';
    $this->load->view('manage/layout', $data);
  }


function print_students(){
    $this->load->helper(array('dompdf'));

    $idcard = $this->input->post('msg');
    for ($i = 0; $i < count($idcard); $i++) {
      $print[] = $idcard[$i]; 
    }

    $params = array();
      // Unit
      if ($this->input->post('unit')) {
        $params['unit_id'] = $this->input->post('unit');
      }
      // Kelas
      if ($this->input->post('class')) {
        $params['class_id'] = $this->input->post('class');
      }

      $data['student'] = $this->Student_model->get($params);
      $data['setting_school'] = $this->Setting_model->get(array('id' => SCHOOL_NAME));
      $data['setting_address'] = $this->Setting_model->get(array('id' => SCHOOL_ADRESS));
      $data['setting_phone'] = $this->Setting_model->get(array('id' => SCHOOL_PHONE));
      $data['setting_district'] = $this->Setting_model->get(array('id' => SCHOOL_DISTRICT));
      $data['setting_city'] = $this->Setting_model->get(array('id' => SCHOOL_CITY)); 
      // print_r($this->input->post('msg'));
      // die;
      for($i = 0; $i < count($data['student']); $i++ ){
        $this->barcode2($data['student'][$i]['student_nis'], '');
      }
      $html = $this->load->view('student/student_multiple_pdf', $data, true);
      $data = pdf_create($html, 'KARTU_'.date('d_m_Y'), TRUE, 'A4', 'potrait');

  }

  function multiple() {
    // $this->output->enable_profiler(TRUE);
    $action = $this->input->post('action');
    $print = array();
    $idcard = array();
    if ($action == "pass") {
      $pass = $this->input->post('msg');
      for ($i = 0; $i < count($pass); $i++) {
        $this->Student_model->add(array('student_id'=> $pass[$i],'student_status'=>0, 'student_last_update'=>date('Y-m-d H:i:s')));
        $this->session->set_flashdata('success', 'Proses Lulus berhasil'); 
      } redirect('manage/student/pass');

    } elseif ($action == "notpass") {
      $notpass = $this->input->post('msg');
      for ($i = 0; $i < count($notpass); $i++) {
        $this->Student_model->add(array('student_id'=> $notpass[$i],'student_status'=>1, 'student_last_update'=>date('Y-m-d H:i:s')));
        $this->session->set_flashdata('success', 'Proses Kembali berhasil'); 
      } redirect('manage/student/pass');

    } elseif ($action == "move") {
      $move = $this->input->post('msg');
      for ($i = 0; $i < count($move); $i++) {
        $this->Student_model->add(array('student_id'=> $move[$i],'student_move'=>1, 'student_status'=>0, 'student_last_update'=>date('Y-m-d H:i:s')));
        $this->session->set_flashdata('success', 'Proses Pindah berhasil'); 
      } redirect('manage/student/move');

    } elseif ($action == "notmove") {
      $notmove = $this->input->post('msg');
      for ($i = 0; $i < count($notmove); $i++) {
        $this->Student_model->add(array('student_id'=> $notmove[$i],'student_move'=>0, 'student_status'=>1, 'student_last_update'=>date('Y-m-d H:i:s')));
        $this->session->set_flashdata('success', 'Proses Kembali berhasil'); 
      } redirect('manage/student/move');

    } elseif ($action == "upgrade") {
      $upgrade = $this->input->post('msg');
      for ($i = 0; $i < count($upgrade); $i++) {
        $this->Student_model->add(array('student_id'=> $upgrade[$i],'class_class_id'=>$this->input->post('class_id'), 'student_last_update'=>date('Y-m-d H:i:s')));
        $this->session->set_flashdata('success', 'Proses Kenaikan Kelas berhasil'); 
      }  redirect('manage/student/upgrade');

    } elseif ($action == "printPdf") {
      $this->load->helper(array('dompdf'));
      $idcard = $this->input->post('msg');
      for ($i = 0; $i < count($idcard); $i++) {
        $print[] = $idcard[$i]; 
      }

      $data['setting_school'] = $this->Setting_model->get(array('id' => SCHOOL_NAME));
      $data['setting_address'] = $this->Setting_model->get(array('id' => SCHOOL_ADRESS));
      $data['setting_phone'] = $this->Setting_model->get(array('id' => SCHOOL_PHONE));
      $data['setting_district'] = $this->Setting_model->get(array('id' => SCHOOL_DISTRICT));
      $data['setting_city'] = $this->Setting_model->get(array('id' => SCHOOL_CITY)); 
      $data['student'] = $this->Student_model->get(array('multiple_id' => $print));
      // print_r($this->input->post('msg'));
      // die;
      for($i = 0; $i < count($data['student']); $i++ ){
        $this->barcode2($data['student'][$i]['student_nis'], '');
      }
      $html = $this->load->view('student/student_multiple_pdf', $data, true);
      $data = pdf_create($html, 'KARTU_'.date('d_m_Y'), TRUE, 'A4', 'potrait');
      
    }

  }



}