<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    // Get From Databases
    function get($params = array()) {
        if (isset($params['member_id'])) {
            $this->db->where('member_id', $params['member_id']);
        }
        if (isset($params['member_name'])) {
            $this->db->where('member_name', $params['member_name']);
        }
        if (isset($params['member_phone'])) {
            $this->db->where('member_phone', $params['member_phone']);
        }
        if (isset($params['member_email'])) {
            $this->db->where('member_email', $params['member_email']);
        }
        if (isset($params['member_password'])) {
            $this->db->like('member_password', $params['member_password']);
        }
        if (isset($params['is_active'])) {
            $this->db->where('is_active', $params['is_active']);
        }
        if (isset($params['multiple_id'])) {
            $this->db->where_in('student.student_id', $params['multiple_id']);
        }
        if(isset($params['member_search'])){
            $this->db->where('member_id', $params['member_search']);
            $this->db->or_like('member_name', $params['member_search']);
        }
       
        if (isset($params['limit'])) {
            if (!isset($params['offset'])) {
                $params['offset'] = NULL;
            }
            $this->db->limit($params['limit'], $params['offset']);
        }
        if (isset($params['order_by'])) {
            $this->db->order_by($params['order_by'], 'desc');
        } else {
            $this->db->order_by('updated_at', 'desc');
        }

        $this->db->select('member.*');
        $res = $this->db->get('member');

        if (isset($params['member_id'])) {
            return $res->row_array();
        } else {
            return $res->result_array();
        }
    }
   
    // add & update data to db
    function add($data = array()) {
        if (isset($data['member_id'])) {
            $this->db->set('member_id', $data['member_id']);
        }
        if (isset($data['member_name'])) {
            $this->db->set('member_name', $data['member_name']);
        }
        if (isset($data['member_gender'])) {
            $this->db->set('member_gender', $data['member_gender']);
        }
        if (isset($data['member_email'])) {
            $this->db->set('member_email', $data['member_email']);
        }
        if (isset($data['member_password'])) {
            $this->db->set('member_password', $data['member_password']);
        }
        if (isset($data['member_address'])) {
            $this->db->set('member_address', $data['member_address']);
        }
        if (isset($data['member_phone'])) {
            $this->db->set('member_phone', $data['member_phone']);
        }
        if (isset($data['member_img'])) {
            $this->db->set('member_img', $data['member_img']);
        }
        if (isset($data['remember_token'])) {
            $this->db->set('remember_token', $data['remember_token']);
        }
        if (isset($data['is_active'])) {
            $this->db->set('is_active', $data['is_active']);
        }

        if (isset($data['member_id'])) {
            $this->db->set('updated_at', date('Y-m-d H:i:s'));
            $this->db->set('updated_by', $this->session->userdata('ufullname'));
            $this->db->where('member_id', $data['member_id']);
            $this->db->update('member');
            $id = $data['member_id'];
        } else {
            $this->db->set('created_at', date('Y-m-d H:i:s'));
            $this->db->set('created_by', $this->session->userdata('ufullname'));
            $this->db->insert('member');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }

    function delete($id) {
        $this->db->where('member_id', $id);
        $this->db->delete('member');
    }

    public function is_exist($field, $value){
        $this->db->where($field, $value);        
        return $this->db->count_all_results('member') > 0 ? TRUE : FALSE;
    }

    function change_password($id, $params) {
        $this->db->where('member_id', $id);
        $this->db->update('member', $params);
    }

}
