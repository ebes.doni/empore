<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Reset Password
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<?= form_open(current_url()); ?>
		<!-- Small cardes (Stat card) -->
		<div class="row">
			<div class="col-md-9">
				<div class="card card-primary">
					<!-- /.card-header -->
					<div class="card-body">
						<?= form_open(current_url()); ?>
						<?= validation_errors(); ?>
						
						<div class="form-group mb-3">
							<label >Password baru*</label>
							<input type="password" name="member_password" class="form-control" placeholder="Password baru">
							<input type="hidden" name="member_id" value="<?= $member['member_id'] ?>" >
						</div>
						<div class="form-group mb-3">
							<label > Konfirmasi password baru*</label>
							<input type="password" name="passconf" class="form-control" placeholder="Konfirmasi password baru" >
						</div>
						<p class="text-muted">*) Kolom wajib diisi.</p>
					</div>
					<!-- /.card-body -->
				</div>
			</div>
			<div class="col-md-3">
				<div class="card card-primary">
					<!-- /.card-header -->
					<div class="card-body">
						<button type="submit" class="btn btn-block btn-success">Simpan</button>
						<a href="<?= site_url('manage/member'); ?>" class="btn btn-block btn-info">Batal</a>

					</div>
					<!-- /.card-body -->
				</div>
			</div>
		</div>
		<?= form_close(); ?>
		<!-- /.row -->
	</section>
</div>