<!DOCTYPE html>
<html>
<head>
    <title><?= $title.' - '.$student['student_full_name'] ?></title>
    <style type="text/css" media="print">
    @page {
      margin-top: 20;  /* this affects the margin in the printer settings */
    	margin-bottom: 180;
    	margin-left: 50;
    	margin-right: 50;
    }
    table{
        border-collapse: collapse;
        border-spacing: 0;       
        width: 100%;
        font-size: 16px;
    }
    table th{
        -webkit-print-color-adjust:exact;
        border: 1px solid;
        padding-top: 5px;
        padding-bottom: 5px;
        /*background-color: #39CCCC;*/
        /*text-align: left;*/
    }
    table td{    
        /*border: 1px solid;*/
    }
    .satu {
   		font-size: 10px;
   	}
   	.dua {
   		font-size: 24px;
   	}
   	.tiga {
   		font-size: 28px;
   	}

    </style>
</head>

<body>

    <h2 style="text-align: center">LEMBAR BUKU INDUK REGISTER</h2>
    
    <table style="margin-bottom: 5px;">    
      <tr style="border: 1px solid;">	
        <td colspan='9' width="430">&nbsp;</td>
        <td width="70" style="border: 1px solid; text-align: center;"><strong>No Urut</strong></td>	
      </tr>   
      <tr style="border: 1px solid;">	
        <td colspan='9' width="430">&nbsp;</td>
        <td width="70" style="height:10px; border-bottom: 1px solid; border-left: 1px solid; 
        border-right: 1px solid; text-align: center;"><span style="font-size: 24px;
                                                                    font-weight: bold;"><?= $student['student_id'] ?></span></td>	
      </tr>                                                   
    </table>

    <table style="margin-bottom: 10px;">    
      <tr style="border: 1px solid;">
        <td width="150">&nbsp;</td>
        <td></td>
        <td>Kelas</td>
        <td>:</td>
        <td width="30" style="border-bottom: 1px dotted;"><?= $student['class_name'] ?></td>
        <td width="30"></td>
        <td>NISN</td>
        <td>:</td>
        <td width="30" style="border-bottom: 1px dotted;"><?= $student['student_nisn'] ?></td>
        <td width="70"></td>	
      </tr>                                                      
    </table>

    <table style="margin-bottom: 10px">	
        <tr>
            <td colspan='3' style="width: 280px; font-weight: bold;">A. KETERANGAN PESERTA DIDIK</td>
            <td></td>
            <td></td>
        </tr>
    </table>

    <table class="word-table" style="margin-bottom: 10px">	
        <tr>		 
            <td style="width: 10px">1</td>   
            <td style="width: 280px">a. Nama Lengkap</td>
            <td style="width: 10px">: </td>
            <td colspan='3' style="border-bottom: 1px dotted;"><?= $student['student_full_name'] ?></td>
            <td style="width: 90px">&nbsp;</td>
            <td rowspan='6' style="width: 10px; text-align:right;
                    border: 1px solid;">
                    <img style="width:130px; height:auto;" src="<?= upload_url('student/'.$student['student_img']) ?>">
            </td>
        </tr>
        <tr>		  
            <td style="width: 10px"></td>  
            <td style="width: 280px">b. Nama Panggilan</td>
            <td style="width: 10px">: </td>
            <td colspan='3' style="border-bottom: 1px dotted"><?= explode(" ", $student['student_full_name'])[0]; ?></td>
        </tr>
        <tr>
            <td style="width: 10px">2</td>    
            <td>Jenis Kelamin</td>
            <td>: </td>
            <td colspan='3' style="border-bottom: 1px dotted">
                <?php if ($student['student_gender']=='L') {
                    echo 'Laki-laki';
                } else {
                    echo 'Perempuan';
                } ?>
            </td>
        </tr>
        <tr>
            <td style="width: 10px">3</td>    
            <td>a. Tempat Lahir</td>
            <td>: </td>
            <td colspan='3' style="border-bottom: 1px dotted"><?= $student['student_born_place'] ?></td>
		    </tr>
		    <tr>    
            <td style="width: 10px"></td>
            <td>b. Tanggal Lahir</td>
            <td>: </td>
            <td colspan='3' style="border-bottom: 1px dotted"><?= $student['student_born_date'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px">4</td>
            <td>Agama</td>
            <td>: </td>
            <td colspan='3' style="border-bottom: 1px dotted"><?= $student['student_religion'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px">5</td>
            <td>Kewarganegaraan</td>
            <td>: </td>
            <td colspan='3' style="border-bottom: 1px dotted"><?= $student['student_citizen'] ?></td>
        </tr>
	  	  <tr>
            <td style="width: 10px">6</td>
            <td>Anak ke</td>
            <td>: </td>
            <td colspan='3' style="border-bottom: 1px dotted"><?= $student['child_order'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px">7</td>
            <td>Jumlah Saudara Kandung</td>
            <td>: </td>
            <td colspan='3' style="border-bottom: 1px dotted"><?= $student['number_of_siblings'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px">8</td>
            <td>NIK</td>
            <td>: </td>
            <td colspan='3' style="border-bottom: 1px dotted"><?= $student['student_nik'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px">9</td>
            <td>No KK</td>
            <td>: </td>
            <td colspan='3' style="border-bottom: 1px dotted"><?= $student['no_kk'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px">10</td>
            <td>Anak Yatim/Yatim Piatu</td>
            <td>: </td>
            <td colspan='3' style="border-bottom: 1px dotted"><?= $student['family_status'] ?></td>
        </tr>
		    <tr> 	  
            <td style="width: 10px">11</td>
            <td>Bahasa sehari-hari</td>
            <td>: </td>
            <td colspan='3' style="border-bottom: 1px dotted"><?= $student['student_suku'] ?></td>
        </tr>
       
        </table><br> 


    <table style="margin-bottom: 10px">	
        <tr>
            <td colspan='3' 
                style="width: 280px; font-weight: bold;">B. KETERANGAN TEMPAT TINGGAL</td>
            <td></td>
            <td></td>
        </tr>
    </table>

    <table class="word-table" style="margin-bottom: 10px">	
        <tr>
            <td style="width: 10px">12</td>
            <td style="width: 280px">Alamat</td>
            <td style="width: 10px">: </td>
            <td style="">Jalan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['student_address'] ?></td>
        </tr>				
		    <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">RT/RW</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['student_address'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Desa</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['student_address'] ?></td>
        </tr>
		    <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Kecamatan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['student_address'] ?></td>
        </tr>
		    <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Kabupaten</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['student_address'] ?></td>
        </tr>
		    <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Provinsi</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['student_address'] ?></td>
        </tr>
		    <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Kode Pos</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['student_address'] ?></td>
        </tr>
		    <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">Nomor Telepon/HP</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['student_phone'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
		    <tr>
            <td style="width: 10px">13</td>
            <td style="width: 280px">Tinggal bersama</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['living_together'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
		    <tr>
            <td style="width: 10px">14</td>
            <td style="width: 280px">Jarak Ke Sekolah</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['distance_to_school'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
		       
        </table>
        <br> 

    <table class="word-table" style="margin-bottom: 10px">	
        <tr>
            <td colspan='3' style="width: 280px; font-weight: bold;">C. KETERANGAN KESEHATAN</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="word-table" style="margin-bottom: 10px">	    				
        <tr>
            <td style="width: 10px">15</td>
            <td style="width: 280px">Golongan Darah</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['student_blood'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px">16</td>
            <td style="width: 280px">Penyakit yang pernah diderita</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['student_disease'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px">17</td>
            <td style="width: 280px">Kelainan jasmani</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['physical_abnormalities'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px">18</td>
            <td style="width: 280px">a. Tinggi Badan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['student_height'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">b. Berat Badan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['student_weight'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
    </table>
    <br> 

    <table class="word-table" style="margin-bottom: 10px">	
        <tr>
            <td colspan='3' style="width: 280px; font-weight: bold;">D. KETERANGAN PENDIDIKAN</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="word-table" style="margin-bottom: 10px">	    				
        <tr>
            <td style="width: 10px">19</td>
            <td style="width: 280px">Pendidikan sebelumnya</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px">20</td>
            <td style="width: 280px">a. Sekolah Asal</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['from_school'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">b. Tanggal dan Nomor Ijazah</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['ijazah_date'].', '.$student['ijazah_no'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">c. Lama Belajar</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['long_study'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px">20</td>
            <td style="width: 280px">Pindahan</td>
            <td style="width: 10px">: </td>
            <td style="width: 20px"></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">a. Dari Sekolah</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['move_from_school'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">b. Alasan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['move_reason'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>

        <tr>
            <td style="width: 10px">21</td>
            <td style="width: 280px">Diterima di sekolah ini</td>
            <td style="width: 10px">: </td>
            <td style="width: 20px"></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">a. Di kelas</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['accept_in_class'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">b. Kelompok</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['accept_in_group'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">c. Jurusan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['accept_in_major'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">d. Tanggal</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['accept_date'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        
    </table>

    <table class="word-table" style="margin-bottom: 10px">	
        <tr>
            <td colspan='3' style="width: 280px; font-weight: bold;">D. KETERANGAN ORANG TUA KANDUNG</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="word-table" style="margin-bottom: 10px">	    				
        <tr>
            <td style="width: 10px">22</td>
            <td style="width: 280px">Ayah Kandung</td>
            <td style="width: 10px">: </td>
            <td style="width: 20px"></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">a. Nama</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['father_name'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">b. Agama</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['father_religion'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">c. Kewarganegaraan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['father_citizen'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">d. Pendidikan Terakhir</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['father_last_education'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">e. Pekerjaan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['father_profession'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">f. Penghasilan per bulan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= 'Rp. ' .number_format($student['father_income'],0) ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>

        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">g. Alamat</td>
            <td style="width: 10px">: </td>
            <td style="">Jalan</td>
            <td style="width: 10px">:</td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['father_addr_road'] ?></td>
        </tr>			
		    <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">RT/RW</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['father_addr_rt_rw'] ?></td>
        </tr>

        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Desa</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['father_addr_village'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Kecamatan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['father_addr_subdistrict'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Kabupaten</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['father_addr_regency'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Provinsi</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['father_addr_province'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Kode Pos</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['father_addr_postalcode'] ?></td>
        </tr>

        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">h. Nomor Telepon/HP</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['father_phone'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>		
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">i. Keadaan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['father_condition'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>		

        <tr>
            <td style="width: 10px">23</td>
            <td style="width: 280px">Ibu Kandung</td>
            <td style="width: 10px">: </td>
            <td style="width: 20px"></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">a. Nama</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['mother_name'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">b. Agama</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['mother_religion'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">c. Kewarganegaraan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['mother_citizen'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">d. Pendidikan Terakhir</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['mother_last_education'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">e. Pekerjaan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['mother_profession'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">f. Penghasilan per bulan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= 'Rp. ' .number_format($student['mother_income'],0) ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>

        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">g. Alamat</td>
            <td style="width: 10px">: </td>
            <td style="">Jalan</td>
            <td style="width: 10px">:</td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['mother_addr_road'] ?></td>
        </tr>			
		    <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">RT/RW</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['mother_addr_rt_rw'] ?></td>
        </tr>

        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Desa</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['mother_addr_village'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Kecamatan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['mother_addr_subdistrict'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Kabupaten</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['mother_addr_regency'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Provinsi</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['mother_addr_province'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Kode Pos</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['mother_addr_postalcode'] ?></td>
        </tr>

        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">h. Nomor Telepon/HP</td>
            <td style="width: 10px">: </td>
            <td style="">Jalan</td>
            <td style="width: 10px">:</td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['mother_phone'] ?></td>
        </tr>		
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">i. Keadaan</td>
            <td style="width: 10px">: </td>
            <td style="">Jalan</td>
            <td style="width: 10px">:</td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['mother_condition'] ?></td>
        </tr>		        
    </table>


    <table class="word-table" style="margin-bottom: 10px">	
        <tr>
            <td colspan='3' style="width: 280px; font-weight: bold;">F. KETERANGAN TENTANG WALI</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="word-table" style="margin-bottom: 10px">	    				
        <tr>
            <td style="width: 10px">24</td>
            <td style="width: 280px">Wali</td>
            <td style="width: 10px">: </td>
            <td style="width: 20px"></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">a. Nama</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['wali_name'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">b. Agama</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['wali_religion'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">c. Kewarganegaraan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['wali_citizen'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">d. Pendidikan Terakhir</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['wali_last_education'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">e. Pekerjaan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['wali_profession'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">f. Penghasilan per bulan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= 'Rp. ' .number_format($student['wali_income'],0) ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>

        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">g. Alamat</td>
            <td style="width: 10px">: </td>
            <td style="">Jalan</td>
            <td style="width: 10px">:</td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['wali_addr_road'] ?></td>
        </tr>			
		    <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">RT/RW</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['wali_addr_rt_rw'] ?></td>
        </tr>

        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Desa</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['wali_addr_village'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Kecamatan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['wali_addr_subdistrict'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Kabupaten</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['wali_addr_regency'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Provinsi</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['wali_addr_province'] ?></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px"></td>
            <td style="width: 10px"></td>
            <td style="">Kode Pos</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['wali_addr_postalcode'] ?></td>
        </tr>

        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">h. Nomor Telepon/HP</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['wali_phone'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>		
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">i. Keadaan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['wali_condition'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>		     
    </table>


    <table class="word-table" style="margin-bottom: 10px">	
        <tr>
            <td colspan='3' style="width: 280px; font-weight: bold;">G. KETERANGAN PENDIDIKAN PESERTA DIDIK</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="word-table" style="margin-bottom: 10px">	    				
        <tr>
            <td style="width: 10px">26</td>
            <td style="width: 280px">Meninggalkan Sekolah</td>
            <td style="width: 10px">: </td>
            <td style="width: 20px"></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">a. Tanggal</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['move_school_date'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">b. Alasan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['move_school_reason'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px">27</td>
            <td style="width: 280px">Akhir Pendidikan</td>
            <td style="width: 10px">: </td>
            <td style="width: 20px"></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">a. Tanggal</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['end_of_edu_date'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">b. Nomor Ijazah</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['end_of_edu_ijazah_no'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>     
    </table>

    <table class="word-table" style="margin-bottom: 10px">	
        <tr>
            <td colspan='3' style="width: 280px; font-weight: bold;">H. KETERANGAN SETELAH SELESAI PENDIDIKAN</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="word-table" style="margin-bottom: 10px">	    				
        <tr>
            <td style="width: 10px">28</td>
            <td style="width: 280px">Melanjutkan ke</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['proceed_to'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px">29</td>
            <td style="width: 280px">Bekerja</td>
            <td style="width: 10px">: </td>
            <td style="width: 20px"></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">a. Tanggal Mulai bekerja</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['start_working'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">b. Nama Perusahaan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= $student['company_name'] ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>    
        <tr>
            <td style="width: 10px"></td>
            <td style="width: 280px">c. Penghasilan</td>
            <td style="width: 10px">: </td>
            <td style="border-bottom: 1px dotted; width: 20px"><?= 'Rp. ' .number_format($student['work_income'],0) ?></td>
            <td style="width: 10px"></td>
            <td style=""></td>
        </tr>      
    </table>
        
</body>
</html>