<?php
if (isset($member)) {
	$member_name 		= $member['member_name'];
	$member_gender 		= $member['member_gender'];
	$member_email 		= $member['member_email'];
	$member_address 	= $member['member_address'];
	$member_phone 		= $member['member_phone'];	
	$is_active 			= $member['is_active'];	
} else {
	$member_name 		= set_value('member_name');
	$member_gender 		= set_value('member_gender');
	$member_email 		= set_value('member_email');
	$member_address 	= set_value('member_address');
	$member_phone 		= set_value('member_phone');	
	$is_active 			= set_value('is_active');	
}
?>

<div class=""> 
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>

	<!-- Main content -->
	<section class="">
		<?= form_open_multipart(current_url()); ?>
		<!-- Small cardes (Stat card) -->
		<div class="row">
			<div class="col-md-9">
				<div class="card">
					<!-- /.card-header -->
					<div class="card-body">
												                                          	
								<?= validation_errors(); ?>
								<?php if (isset($member)) { ?>
									<input type="hidden" name="member_id" value="<?= $member['member_id']; ?>">
								<?php } ?>
								
								<div class="mb-3">
									<label>Nama lengkap <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
									<input name="member_name" type="text" class="form-control" value="<?= $member_name ?>" placeholder="Nama lengkap">
								</div>
																	
								<div class="mb-3">
									<label>Jenis Kelamin</label>										
									<div class="form-check">
										<input type="radio" id="laki_laki" name="member_gender" value="L" <?= ($member_gender == 'L') ? 'checked' : ''; ?> class="form-check-input">
										<label class="form-check-label" for="laki_laki">Laki-laki</label>
									</div>
									<div class="form-check">
										<input type="radio" id="perempuan" name="member_gender" <?= ($member_gender == 'P') ? 'checked' : ''; ?> class="form-check-input">
										<label class="form-check-label" for="perempuan">Perempuan</label>
									</div>										
								</div>								
								
								<div class="mb-3">
									<label>No. Handphone <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
									<input name="member_phone" type="text" class="form-control" value="<?= $member_phone ?>" placeholder="No Handphone">
								</div>

								<div class="mb-3">
									<label>Email <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
									<input name="member_email" type="email" class="form-control" value="<?= $member_email ?>" placeholder="Email">
								</div>

								<div class="mb-3">
									<label>Password <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
									<input name="member_password" type="password" class="form-control" placeholder="Password">
								</div>            

								<div class="mb-3">
									<label>Konfirmasi Password <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
									<input name="passconf" type="password" class="form-control" placeholder="Konfirmasi Password">
								</div>      

								<div class="mb-3">
									<label>Alamat</label>
									<textarea class="form-control" name="member_address" placeholder="Alamat Tempat Tinggal"><?= $member_address ?></textarea>
								</div>
						
							<p class="text-muted">*) Kolom wajib diisi.</p>
					</div>
					<!-- /.card-body -->
				</div>
			</div>

			<div class="col-md-3">
				<div class="card card-primary">
					<!-- /.card-header -->
					<div class="card-body">
						<div class="form-group">
							<label>Status</label>
							<div class="radio">
								<label>
									<input type="radio" name="is_active" value="1" <?= ($is_active == 1) ? 'checked' : ''; ?>> Aktif
								</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="is_active" value="0" <?= ($is_active == 0) ? 'checked' : ''; ?>> Tidak Aktif
								</label>
							</div>
						</div>
						<label >Foto</label>
						<a href="#" class="thumbnail">
							<?php if (isset($member['member_img']) != NULL) { ?>
								<img src="<?= upload_url('member/' . $member['member_img']) ?>" class="img-responsive avatar">
							<?php } else { ?>
								<img src="<?= media_url('img/missing.png') ?>" id="target" class="avatar" alt="Choose image to upload">
							<?php } ?>
						</a>
						                                                   
						<div class="mb-3">
							<input type='file' id="member_img" name="member_img">
						</div>						
						<button type="submit" class="btn btn-block btn-primary">Simpan</button>
						<a href="<?= site_url('manage/member'); ?>" class="btn btn-block btn-danger">Batal</a>
						<?php if (isset($member)) { ?>
							<button type="button" 
									onclick="getId(<?= $member['member_id'] ?>)" 
									class="btn btn-danger btn-block" 
									data-bs-toggle="modal" 
									data-bs-target="#deleteMember">Hapus
							</button>
						<?php } ?>
					</div>
					<!-- /.card-body -->
				</div>
			</div>
		</div>
		<?= form_close(); ?>
		<!-- /.row -->
	</section>
</div>

<?php if (isset($member)) { ?>
	<div class="modal fade" id="deleteMember">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Konfirmasi Hapus</h4>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<form action="<?= site_url('manage/member/delete') ?>" method="POST">
					<div class="modal-body">
						<p>Apakah anda akan menghapus data ini?</p>
						<input type="hidden" name="member_id" id="memberId">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-bs-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-danger">Hapus</button>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php } ?>

<script>
	function getId(id) {
		$('#memberId').val(id)
	}
</script>

<script>
	var classApp = angular.module("classApp", []);
	var SITEURL = "<?= site_url() ?>";

	classApp.controller('classCtrl', function($scope, $http) {
		$scope.classs = [];
		<?php if (isset($member)): ?>
			$scope.class_data = {index: <?= $member['class_class_id']; ?>};
		<?php endif; ?>

		$scope.getClass = function() {

			var url = SITEURL + 'api/get_class/';
			$http.get(url).then(function(response) {
				$scope.classs = response.data;
			});

		};

		$scope.submit = function(member) {
			var postData = $.param(member);
			$.ajax({
				method: "POST",
				url: SITEURL + "manage/member/add_class",
				data: postData,
				success: function(data) {
					$scope.getClass();
					$scope.classForm.class_name = '';
				}
			});
		};

		angular.element(document).ready(function() {
			$scope.getClass();
		});

	});

	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#target').attr('src', e.target.result);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#member_img").change(function() {
		readURL(this);
	});

</script>