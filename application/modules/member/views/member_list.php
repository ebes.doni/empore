<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>


	<div class="mt-2">
		<div class="card-header">
			<a href="<?= site_url('manage/member/add') ?>"
				 class="btn btn-sm btn-success">
				 <i class="fa fa-plus"></i> Tambah
			</a>
		</div>
		<!-- /.card-header -->
	</div>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">				
				<div class="card"> 					
						<div class="card-body table-responsive">
							<table id="dtable" 
								class="table table-hover js-datatable table table-borderless table-thead-bordered table-nowrap table-align-middle card-table">                    
								<thead class="bg-soft-dark">
								<tr>
									<th>No</th>
									<th>#ID</th>
									<th>Nama</th>
									<th>Alamat</th>
									<th>Status</th>
									<th>Aksi</th>
								</tr>
								</thead>
								<tbody>
									<?php
									if (!empty($member)) {
										$i = 1;
										foreach ($member as $row):
											?>
											<tr>
												<td><?= $i; ?></td>
												<td><?= $row['member_id']; ?></td>
												<td><?= $row['member_name']; ?></td>
												<td><?= character_limiter($row['member_address'],15); ?></td>											
												<td><span class="legend-indicator <?= ($row['is_active']==1) ? 'bg-success' : 'bg-danger' ?>"></span><?= ($row['is_active']==1) ? 'Aktif' : 'Tidak Aktif' ?></td>
												<td>
													<div class="btn-group" role="group">
														<a class="btn btn-white btn-sm" href="<?= site_url('manage/member/view/' . $row['member_id']) ?>">
														<i class="bi-eye"></i> View
														</a>
														 <!-- Button Group -->
														 <div class="btn-group">
															<button type="button" 
																	class="btn btn-white btn-icon btn-sm dropdown-toggle dropdown-toggle-empty" 
																	id="productsEditDropdown1" 
																	data-bs-toggle="dropdown" 
																	aria-expanded="false">
															</button>
															<div class="dropdown-menu dropdown-menu-end mt-1" aria-labelledby="productsEditDropdown1">
																<a class="dropdown-item" 
																	href="<?= site_url('manage/member/rpw/' . $row['member_id']) ?>">
																	<i class="bi-unlock dropdown-item-icon"></i> Reset Password
																</a>
																<a class="dropdown-item view-pdf" 
																	href="#" data-href="<?= site_url('manage/member/printPdf/' . $row['member_id']) ?>" 
																	target="_blank"><i class="bi-printer dropdown-item-icon"></i> Cetak Kartu
																</a>															
																<?php if ($this->session->userdata('uroleid') != USER) { ?>
																<a class="dropdown-item" 
																	href="<?= site_url('manage/member/edit/' . $row['member_id']) ?>">
																	<i class="bi-pencil dropdown-item-icon"></i> Edit
																</a>
																<button class="dropdown-item" 
																		onclick="getId(<?= $row['member_id'] ?>)" 
																		data-bs-toggle="modal" 
																		data-bs-target="#deleteMember">
																		<i class="bi-trash dropdown-item-icon"></i> Delete
																</button>
																<?php } ?>															
															</div>
														</div>
														<!-- End Button Group -->														
													</div>
												</td>
											</tr>
											<?php
											$i++;
										endforeach;
									} else {
										?>
										<tr id="row">
											<td colspan="8" align="center">Data Kosong</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						
						<!-- /.box -->
					</div>
				</div>
				
			<!-- </form> -->
	</section>
	<!-- /.content -->
</div>

	<div class="modal fade" id="deleteMember" role="dialog" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Konfirmasi Hapus</h4>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<form action="<?= site_url('manage/member/delete') ?>" method="POST">
					<div class="modal-body">
						<p>Apakah anda akan menghapus data ini?</p>
						<input type="hidden" name="member_id" id="memberId">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-bs-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-danger">Hapus</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Start Popup Cetak Kartu -->
	<div class="modal fade" id="myModal">
		<div class="modal-dialog modal-lg modal-dialog-centered">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header">
				<h4 class="modal-title">Cetak Kartu</h4>
				<button type="button" class="btn-close" data-bs-dismiss="modal"></button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
				<iframe src="" width="600" height="380" frameborder="0" allowtransparency="true"></iframe>
				</div>

				<!-- Modal footer -->
				<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
			</div>
		</div>
	</div>


<script>
	function getId(id) {
		$('#memberId').val(id)
	}
</script>

<script>
	$(document).ready(function() {
	$(".view-pdf").click(function(e) {
		e.preventDefault();
		var url = $(this).attr("data-href");
		$("#myModal iframe").attr("src", url);
		$("#myModal").modal("show");
	});
	});
</script>



