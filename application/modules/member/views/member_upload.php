<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<!-- /.card-header -->
					<div class="card-body table-responsive">
						<h4>Petunjuk Singkat</h4>
						<p>Penginputan data Siswa bisa dilakukan dengan mengcopy data dari file Ms. Excel. Format file excel harus sesuai kebutuhan aplikasi. Silahkan download formatnya <a href="<?= site_url('manage/member/download');?>"><span class="label label-success">Disini</span></a>
							<br><br>
							<strong>CATATAN :</strong>
							<ol>
								<li>Pengisian jenis data <strong>TANGGAL</strong>  diisi dengan format <strong>YYYY-MM-DD</strong> Contoh <strong>2017-12-21</strong><br> Cara ubah : blok semua tanggal pilih format cell di excel ganti dengan format date pilih yang tahunnya di depan</li>  
							</ol>
						</p>
						<hr>

						<?= form_open_multipart(current_url()) ?>

						<div class="form-group">
							<textarea placeholder="Copy data yang akan dimasukan dari file excel, dan paste disini" rows="8" class="form-control" name="rows"></textarea>
						</div>
						<br>
						<div class="form-group">
							<button type="submit" class="btn btn-success btn-sm btn-flat">Import</button>
							<a href="<?= site_url('manage/member') ?>" class="btn btn-info btn-sm btn-flat">Kembali</a>
						</div>
						<?= form_close() ?>

					</div>
					<!-- /.card-body -->
				</div>

				<!-- /.card -->
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>