<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modul_set extends CI_Controller {
  public function __construct(){
    parent::__construct();
    if ($this->session->userdata('logged') == NULL) {
      header("Location:" . site_url('manage/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
    }
    $list_access = array(SUPERUSER);
    if (!in_array($this->session->userdata('uroleid'),$list_access)) {
      redirect('manage');
    }

    $this->load->model(array('modul/Modul_model', 
                            'setting/Setting_model'));
    $this->load->helper(array('form', 'url'));
    $this->load->library('pagination');
  }

    // menu view in list
  public function index($offset = NULL) {
    // Apply Filter
    $f = $this->input->get(NULL, TRUE);
    $data['f'] = $f;
    $params = array();

    if (isset($f['n']) && !empty($f['n']) && $f['n'] != '') {
      $params['class_name'] = $f['n'];
    }
    $data['modul'] = $this->Modul_model->get($params);
    $data['fontawesome'] = $this->Modul_model->get_fontawesome();
    $data['title'] = 'Modul';
    $data['main'] = 'modul/modul_list';
    $this->load->view('manage/layout', $data);
  }
  
  
  // Add User_customer and Update
  public function add() {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Nama Modul', 'trim|required|xss_clean');
    $this->form_validation->set_rules('slug', 'Slug', 'trim|required|xss_clean');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button ket="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
    
    if ($_POST AND $this->form_validation->run() == TRUE) {
      if ($this->input->post('id')) {
        $params['id'] = $this->input->post('id');
        $params['role_id'] = $_POST['role_id'];
        // print_r($_POST['role_id']);
        // die;
        $data['operation'] = 'Sunting';
      }else{
        /**default only superuser */
        $params['role_id'] = '[{"SUPERUSER":"1","USER":"0","AKADEMIK":"0","KESISWAAN":"0","TATA USAHA":"0","GURU":"0","KEUANGAN":"0","KASIR":"0"}]';
        $data['operation'] = 'Tambah';
      }

      $params['name'] = $this->input->post('name');
      $params['slug'] = $this->input->post('slug');
      $params['icon'] = $this->input->post('icon');
      $params['note'] = $this->input->post('note');
      $params['parent_id'] = $this->input->post('parent_id');
      $params['menu'] = $this->input->post('menu');
      $add = $this->Modul_model->add($params);

      if($add){
        $this->session->set_flashdata('success', $data['operation'] . ' Modul Berhasil');
        redirect('manage/modul');
      }
      
    } 

  }


  // Delete to database
  public function delete() {
    if ($this->session->userdata('uroleid')!= SUPERUSER){
        $this->session->set_flashdata('failed', 'Anda tidak mempunyai hak akses untuk menghapus data');
        redirect('manage/modul');
    }
    if ($_POST) {
      $id = $_POST['modul_id'];  
      $this->Modul_model->delete($id);
      // activity log
      $this->load->model('logs/Logs_model');
      $this->Logs_model->add(
        array(
          'log_date' => date('Y-m-d H:i:s'),
          'user_id' => $this->session->userdata('uid'),
          'log_module' => 'Modul',
          'log_action' => 'Hapus',
          'log_info' => 'ID:' . $id . '; Label:' . $_POST['delName']
        )
      );
      $this->session->set_flashdata('success', 'Hapus modul berhasil');
      redirect('manage/modul');
    }  
  }
}
