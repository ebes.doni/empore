<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modul_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    // Get fontawesome from database
    function get_fontawesome($params = array()){
        if(isset($params['id'])){
            $this->db->where('id', $params['id']);
        }
        if(isset($params['limit'])){
            if(!isset($params['offset'])){
                $params['offset'] = NULL;
            }
            $this->db->limit($params['limit'], $params['offset']);
        }

        if(isset($params['order_by'])){
            $this->db->order_by($params['order_by'], 'desc');
        }else{
            $this->db->order_by('id', 'asc');
        }

        $this->db->select('fontawesome.*');
        $res = $this->db->get('fontawesome');

        if(isset($params['id'])){
            return $res->row_array();
        }else{
            return $res->result_array();
        }
    }

    // Get period from database
    function get($params = array()){
        if(isset($params['modul_id'])){
            $this->db->where('modul.id', $params['modul_id']);
        }
        if(isset($params['name'])){
            $this->db->where('name', $params['name']);
        }
        if(isset($params['slug'])){
            $this->db->where('slug', $params['slug']);
        }
        if(isset($params['icon'])){
            $this->db->where('icon', $params['icon']);
        }
        if(isset($params['note'])){
            $this->db->where('note', $params['note']);
        }
        if(isset($params['menu'])){
            $this->db->where('menu', $params['menu']);
        }
        if(isset($params['role_id'])){
            $this->db->where('role_id', $params['role_id']);
        }
        if(isset($params['parent_id'])){
            $this->db->where('parent_id', $params['parent_id']);
        }   
       
        if(isset($params['limit'])){
            if(!isset($params['offset'])){
                $params['offset'] = NULL;
            }
            $this->db->limit($params['limit'], $params['offset']);
        }

        if(isset($params['order_by'])){
            $this->db->order_by($params['order_by'], 'desc');
        }else{
            $this->db->order_by('id', 'asc');
        }

        $this->db->select('modul.*');
        $res = $this->db->get('modul');

        if(isset($params['modul_id'])){
            return $res->row_array();
        }else{
            return $res->result_array();
        }
    }

    // Add and update to database
    function add($data = array()) {
        if(isset($data['id'])) {
            $this->db->set('id', $data['id']);
        }
        if(isset($data['name'])) {
            $this->db->set('name', $data['name']);
        }
        if(isset($data['icon'])) {
            $this->db->set('icon', $data['icon']);
        }
        if(isset($data['slug'])) {
            $this->db->set('slug', $data['slug']);
        }
        if(isset($data['menu'])) {
            $this->db->set('menu', $data['menu']);
        }
        if(isset($data['note'])) {
            $this->db->set('note', $data['note']);
        }
        if(isset($data['role_id'])) {
            $this->db->set('role_id', $data['role_id']);
        }
        if(isset($data['parent_id'])) {
            $this->db->set('parent_id', $data['parent_id']);
        }
        if(isset($data['menu'])) {
            $this->db->set('menu', $data['menu']);
        }
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('modul');
            $id = $data['id'];
        } else {
            $this->db->insert('modul');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }

     // Delete modul to database
     function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('modul');
    }
    

}
