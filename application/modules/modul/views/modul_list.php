<!-- select2 -->
<!-- <link rel="stylesheet" href="<?= site_url('media/select2/dist/css/select2.min.css') ?>">
<style>
    .select2{width:100% !important};
    .select2 {
        font-family: 'FontAwesome'
    }
</style> -->
<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">
                        <button type="button" 
								class="btn btn-success btn-sm" 
                                onclick="resetForm()"
								data-bs-toggle="modal" 
								data-bs-target="#addModul">
								<i class="fa fa-plus"></i> Tambah
						</button>
					</div>
					<!-- /.card-header -->
					<div class="card-body table-responsive">
						<table id="dtable" class="table table-hover">
							<thead>
								<tr>
									<th>No</th>
                                    <th>Icon</th>
									<th>Label</th>
                                    <th>Slug</th>
                                    <th>Parent</th>                                   
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if (!empty($modul)) {
									$i = 1;
									foreach ($modul as $row):
                                        $parent = $this->Modul_model->get(array('modul_id'=>$row['parent_id']))['note'];
										?>
										<tr>
											<td><?= $i; ?></td>
                                            <td><i class="<?= $row['icon'] ?>"></i></td>
											<td><?= $row['note'] ?></td>
                                            <td><?= $row['slug'] ?></td> 
                                            <td><?= isset($parent) ? $parent : '-' ?></td>                                           
											<td>
                                                <button type="button" 
                                                        class="btn btn-warning btn-xs" 
                                                        data-bs-toggle="modal" 
                                                        data-bs-target="#addModul"
                                                        data-id="<?= $row['id']; ?>"
                                                        data-icon="<?= $row['icon']; ?>"
                                                        data-name="<?= $row['name']; ?>"
                                                        data-slug="<?= $row['slug']; ?>"
                                                        data-parent_id="<?= $row['parent_id']; ?>"
                                                        data-note="<?= $row['note']; ?>"
                                                        data-role_id='<?= $row['role_id']; ?>'
                                                        data-menu='<?= $row['menu']; ?>'
                                                        id="editBtn<?= $row['id'];?>" 
                                                        title="Edit Modul"><i class="fa fa-pencil"></i>
                                                </button>
												<button type="button"  
                                                    class="btn btn-danger btn-xs" 
                                                    data-bs-toggle="modal" 
                                                    data-bs-target="#delete"
                                                    data-id="<?= $row['id']; ?>"    
                                                    data-note="<?= $row['note']; ?>"
                                                    id="delBtn<?= $row['id'] ?>">
													<i class="fa fa-trash"></i>
												</button>												
											</td>	
										</tr>
										<?php
										$i++;
									endforeach;
								} else {
									?>
									<tr id="row">
										<td colspan="4" align="center">Data Kosong</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				
				<!-- /.card -->
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>

        <!-- Modal delete data -->
        <div class="modal fade" id="delete">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Konfirmasi Hapus</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form action="<?= site_url('manage/modul/delete') ?>" method="POST">
                        <div class="modal-body">
                            <p>Apakah anda akan menghapus data ini?</p>
                            <input type="hidden" name="modul_id" id="modulID" readonly>
                            <input type="hidden" name="delName" id="delName" readonly>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Hapus</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Modal Add Data-->
        <div class="modal fade" id="addModul" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">						
						<h4 class="modal-title">Tambah Modul</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<?= form_open('manage/modul/add', array('method'=>'post')); ?>
					<div class="modal-body">
                        <div class="form-group mb-2" style="display:none;">
                            <label>Modul ID</label>
                            <input type="text" required="" name="id" id="id" class="form-control" readonly>
                        </div>
                        <div class="form-group mb-2" style="display:none;">
                            <label>Role ID</label>
                            <input type="text" required="" name="role_id" id="role_id" class="form-control" readonly>
                        </div>
						<div class="form-group mb-2">
                            <label>Nama Modul</label>
                            <input type="text" required="" name="name" id="name" class="form-control" placeholder="Contoh: employee">
                        </div>
                        <div class="form-group mb-2">
                            <label>Slug</label>
                            <input type="text" required="" name="slug" id="slug" class="form-control" placeholder="Contoh: manage/employee">
                        </div>
                        <!-- <div class="form-group mb-2">
                            <label>Icon</label>
                            <select name="icon" id="icon" class="form-control" style="font-family: fontAwesome; font-size:16px;">
                                <option value="">Select Icon...</option>                                    
                                <?php if (count($fontawesome)): foreach ($fontawesome as $v_fontawesome) : ?>
                                <option value="<?= $v_fontawesome['name'] ?>">
                                    <?= $v_fontawesome['code']."; ". $v_fontawesome['label'] ?>
                                </option>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </select>                           
                        </div> -->

                        <!-- <div class="form-group mb-2">
                            <label for="varchar">Icon</label>
                            <select class="select2-icon" name="icon" id="icon">
                                <?php if (count($fontawesome)): foreach ($fontawesome as $v_fontawesome) : ?>
                                <option value="<?= $v_fontawesome['name'] ?>" data-icon="<?= $v_fontawesome['name'] ?>"><?= $v_fontawesome['label'] ?></option>
                                <?php endforeach; ?>
                                <?php endif; ?>  
                            </select>
                        </div> -->

                        <!-- Select -->
                        <!-- <div class="tom-select-custom tom-select-custom-with-tags">
                        <select class="js-select form-select" autocomplete="off" id="tomselect"
                                data-hs-tom-select-options='{
                                    "placeholder": "Select a person..."
                                }'>
                            <option value="">Select a person...</option>
                            <option value="4">Thomas Edison</option>
                            <option value="1">Nikola</option>
                            <option value="3">Nikola Tesla</option>
                            <option value="5">Arnold Schwarzenegger</option>
                        </select>
                        </div> -->
                        <!-- End Select -->

                        <div class="form-group mb-2">
                            <label for="varchar">Icon</label>
                            <!-- Select -->
                            <div class="tom-select-custom tom-select-custom-with-tags">
                                <select class="js-select form-select" 
                                        name="icon" 
                                        id="icon"
                                        data-hs-tom-select-options='{
                                            "placeholder": "Select a icon..."
                                        }'>
                                    <?php if (count($fontawesome)): ?>
                                     <!-- <option value="fa fa-ambulance" 
                                            data-icon="fa fa-ambulance"
                                            data-option-template='<span class="d-flex align-items-center active selected">
                                                                    <i class="fa fa-ambulance"></i>&nbsp;
                                                                    <span class="text-truncate"> ambulance</span>
                                                                </span>'>
                                            ambulance
                                    </option> -->
                                     <?php foreach ($fontawesome as $v_fontawesome) : ?>                                           
                                            <option value="<?= $v_fontawesome['name'] ?>" 
                                                    data-icon="<?= $v_fontawesome['name'] ?>"
                                                    data-option-template='<span class="d-flex align-items-center">
                                                                            <i class="<?= $v_fontawesome['name'] ?>"></i>&nbsp;
                                                                            <span class="text-truncate"> <?= $v_fontawesome['label'] ?></span>
                                                                        </span>'>
                                                    <?= $v_fontawesome['label'] ?>
                                            </option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>  
                                </select>
                            </div>
                            <!-- End Select -->
                        </div>

                        <div class="form-group mb-2">
                            <label>Label</label>
                            <input type="text" required="" name="note" id="note" class="form-control" placeholder="Contoh: Pegawai">
                        </div>
                        <div class="form-group mb-2">
                            <label>Parent</label>
                            <select name="parent_id" id="parent_id" class="form-control form-select">
                                <option value=""> -- Pilih Parent -- </option>
                                <?php foreach($modul as $row):?>
                                    <option value="<?= $row['id']?>">
                                        <?= $row['note']?>
                                    </option>
                                <?php endforeach ?>  				
                            </select>
                        </div>
                        <div class="form-group mb-2">
                            <label>Menu</label>
                            <select name="menu" id="menu" class="form-control form-select" required>
                                <option value=""> -- Pilih Tipe Menu -- </option>
                                    <option value="main_menu">main_menu</option>
                                    <option value="sub_menu_1">sub_menu_1</option>
                                    <option value="sub_menu_2">sub_menu_2</option>
                            </select>
                        </div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success">Simpan</button>
						<button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
					</div>
					<?= form_close(); ?>
				</div>
			</div>
		</div>

<script>
    $(document).ready(function(){
        <?php foreach($modul as $row): ?>
        $('#delBtn<?= $row['id'];?>').click(function(){
            var id = $(this).data('id');
            var note = $(this).data('note');
            $('#modulID').val(id);
            $('#delName').val(note);
        });
        <?php endforeach; ?>
    });

    function resetForm(){
        $('#id').val('');
        $('#icon').val('').change();
        $('#name').val('');
        $('#slug').val('');
        $('#parent_id').val('').change();
        $('#note').val('');
        $('#role_id').val('');
        $('#menu').val('').change();
    }
</script>


<script>
$(document).ready(function(){
	<?php foreach($modul as $row): ?>
		$('#editBtn<?= $row['id'];?>').click(function(){
			var id = $(this).data('id');
			var icon = $(this).data('icon');
			var name = $(this).data('name');
			var slug = $(this).data('slug');	
			var parent_id = $(this).data('parent_id');		
            var note = $(this).data('note');	
            var role_id = $(this).data('role_id');		
            var menu = $(this).data('menu');		
			$('#id').val(id);
			$('#name').val(name);
			$('#slug').val(slug);
			$('#parent_id').val(parent_id).change();
            $('#note').val(note);
            $('#role_id').val(JSON.stringify(role_id));
            $('#menu').val(menu).change();      
            
            // Set the value in the Tom Select dropdown
            var selectElement = document.querySelector('.js-select');
            var selectInstance = selectElement.tomselect;
            selectInstance.setValue(icon);

		});
	<?php endforeach; ?>
});

</script>


<!-- <script src="<?= site_url('media/select2/dist/js/select2.full.min.js') ?>"></script>
<script type="text/javascript">
    //select2
    $('.select2').select2();

// reference: https://jsfiddle.net/qCn6p/208/
function formatText (icon) {
    return $('<span><i class="fa ' + $(icon.element).data('icon') + '"></i> ' + icon.text + '</span>');
};

$('.select2-icon').select2({
    width: "50%",
    templateSelection: formatText,
    templateResult: formatText
});
   
</script> -->