<?php
if (isset($period)) {
	$inputStartValue = $period['period_start'];
	$inputEndValue = $period['period_end'];
	$inputStatusValue = $period['period_status'];
} else {
	$inputStartValue = set_value('period_start');
	$inputEndValue = set_value('period_end');
	$inputStatusValue = set_value('period_status');
}
?>

<div class=""> 
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>

	<!-- Main content -->
	<section class="content">
		<?= form_open_multipart(current_url()); ?>
		<!-- Small cardes (Stat card) -->
		<div class="row">
			<div class="col-md-9">
				<div class="card card-primary">
					<!-- /.card-header -->
					<div class="card-body">
						<?= validation_errors(); ?>
						<?php if (isset($period)) { ?>
							<input type="hidden" name="period_id" value="<?= $period['period_id']; ?>">
						<?php } ?>

						<div class="form-group mb-3">
							<label>Tahun Ajaran *</label>
							<div class="row">
								<div class="col-sm-6 col-md-6">
									<input type="text" name="period_start" readonly="" class="form-control years" onchange="getYear(this.value)" placeholder="Tahun Awal">
								</div>
								<div class="col-sm-6 col-md-6">
									<input type="text" class="form-control" readonly="" name="period_end" id="YearEnd" value="<?= $inputEndValue ?>" placeholder="Tahun Akhir">
								</div>
							</div>
						</div>

						<div class="form-group mb-3">
							<label>Keterangan</label>
							<div class="radio">
								<label>
									<input type="radio" name="period_status" value="1" <?= ($inputStatusValue == 1) ? 'checked' : ''; ?>> Aktif
								</label> &nbsp;&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" name="period_status" value="0" <?= ($inputStatusValue == 0) ? 'checked' : ''; ?>> Tidak Aktif
								</label>
							</div>
						</div>

						<p class="text-muted">*) Kolom wajib diisi.</p>
					</div>
					<!-- /.card-body -->
				</div>
			</div>
			<div class="col-md-3">
				<div class="card card-primary">
					<!-- /.card-header -->
					<div class="card-body">
						<button type="submit" class="btn btn-block btn-success">Simpan</button>
						<a href="<?= site_url('manage/period'); ?>" class="btn btn-block btn-info">Batal</a>
					</div>
					<!-- /.card-body -->
				</div>
			</div>
		</div>
		<?= form_close(); ?>
		<!-- /.row -->
	</section>
</div>

<script>
	function getYear(value) {
		var yearsend = parseInt(value) + 1;
		$("#YearEnd").val(yearsend);
	}

</script>
<!-- <script>
      $(".input-group.date").datepicker({autoclose: true, todayHighlight: true});
    </script> -->