<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>
	<div class="mt-2">
		<div class="">
			<a href="<?= site_url('manage/period/add') ?>" 
				class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a>
		</div>
		<!-- /.card-header -->
	</div>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-body table-responsive no-padding">
						<table id="dtable" class="table table-hover">
							<thead class="bg-soft-dark">
							<tr>
								<th>No</th>
								<th>Tahun Ajaran</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
							</thead>
							<tbody>
								<?php
								if (!empty($period)) {
									$i = 1;
									foreach ($period as $row):
										?>
										<tr>
											<td><?= $i; ?></td>
											<td><?= $row['period_start'].'/'.$row['period_end'] ?></td>
											<td><span class="legend-indicator <?= ($row['period_status']==1) ? 'bg-success' : 'bg-danger' ?>"></span><?= ($row['period_status']==1) ? 'Aktif' : 'Tidak Aktif' ?></td>
											<td>
												<a href="<?= site_url('manage/period/edit/' . $row['period_id']) ?>" class="btn btn-xs btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
												<button type="button" onclick="getId(<?= $row['period_id'] ?>)" class="btn btn-danger btn-xs" data-bs-toggle="modal" data-bs-target="#deletePeriod">
													<i class="fa fa-trash"></i>
												</button>
												<?php if($row['period_status']!=1){ ?>
													<a href="<?= site_url('manage/period/period_active/' . $row['period_id']) ?>" class="btn btn-xs btn-success" data-toggle="tooltip" title="Aktifkan"><i class="fa fa-check"></i></a>
												<?php } ?>
											</td>	
										</tr>
										<?php
										$i++;
									endforeach;
								} else {
									?>
									<tr id="row">
										<td colspan="4" align="center">Data Kosong</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<div>
					<?= $this->pagination->create_links(); ?>
				</div>
				<!-- /.card -->
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>

<div class="modal fade" id="deletePeriod">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Konfirmasi Hapus</h4>
			</div>
			<form action="<?= site_url('manage/period/delete') ?>" method="POST">
				<div class="modal-body">
					<p>Apakah anda akan menghapus data ini?</p>
					<input type="hidden" name="period_id" id="periodID">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-bs-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger">Hapus</button>
				</div>
			</form>
		</div>
	</div>
</div>


<script>

	function getId(id) {
		$('#periodID').val(id)
	}
</script>