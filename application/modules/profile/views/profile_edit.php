<?php
if (isset($user)) {
	$id = $user['user_id'];
	$inputFullnameValue = $user['user_full_name'];
	$inputRoleValue = $user['user_role_role_id'];
	$inputEmailValue = $user['user_email'];
	$inputDescValue = $user['user_description'];
} else {
	$inputFullnameValue = set_value('user_full_name');
	$inputRoleValue = set_value('user_role_role_id');
	$inputEmailValue = set_value('user_email');
	$inputDescValue = set_value('user_description');
}
?>

<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>

	<!-- Main content -->
	<!-- Main content -->
	<section class="content">
		<?= form_open_multipart(current_url()); ?>
		<!-- Small cardes (Stat card) -->
		<div class="row">
			<div class="col-md-9">
				<div class="card card-primary">
					<!-- /.card-header -->
					<div class="card-body">
						<?= validation_errors(); ?>
						<?php if (isset($user)) { ?>
						<input type="hidden" name="user_id" value="<?= $user['user_id']; ?>">
						<?php } ?>
						<div class="form-group mb-2">
							<label>Email <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
							<input name="user_email" type="text" class="form-control" <?= (isset($user)) ? 'disabled' : ''; ?> value="<?= $inputEmailValue ?>" placeholder="email">
						</div> 

						<div class="form-group mb-2">
							<label>Nama lengkap <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
							<input name="user_full_name" type="text" class="form-control" value="<?= $inputFullnameValue ?>" placeholder="Nama lengkap">
						</div>

						<?php if (!isset($user)) { ?>
						<div class="form-group mb-2">
							<label>Password <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
							<input name="user_password" type="password" class="form-control" placeholder="Password">
						</div>            

						<div class="form-group mb-2">
							<label>Konfirmasi Password <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
							<input name="passconf" type="password" class="form-control" placeholder="Konfirmasi Password">
						</div>       
						<?php } ?>

						<div class="form-group mb-2">
							<label>Deskripsi</label>
							<textarea class="form-control" name="user_description" placeholder="Deskripsi"><?= $inputDescValue ?></textarea>
						</div>

						<p class="text-muted">*) Kolom wajib diisi.</p>
					</div>
					<!-- /.card-body -->
				</div>
			</div>
			<div class="col-md-3">
				<div class="card card-primary">
					<!-- /.card-header -->
					<div class="card-body">
						<div class="form-group mb-2">
							<label >Foto</label>
							<a href="#" class="thumbnail">
								<?php if (isset($user) AND $user['user_image'] != NULL) { ?>
								<img src="<?= upload_url('users/' . $user['user_image']) ?>" class="img-responsive avatar">
								<?php } else { ?>
								<img id="target" alt="Choose image to upload">
								<?php } ?>
							</a>
							<input type='file' id="user_image" name="user_image">
						</div>
						
						<button type="submit" class="btn btn-block btn-success">Simpan</button>
						<a href="<?= site_url('manage/profile'); ?>" class="btn btn-block btn-info">Batal</a>
					</div>
					<!-- /.card-body -->
				</div>
			</div>
		</div>
		<?= form_close(); ?>
		<!-- /.row -->
	</section>
</div>
<script src="<?= media_url() ?>/js/jquery.min.js"></script>
<script type="text/javascript">
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#target').attr('src', e.target.result);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#user_image").change(function() {
		readURL(this);
	});
</script>