<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="row">
			<div class="col-md-4">

				<!-- Profile Image -->
				<div class="card card-primary">
					<div class="card-body card-profile">
						<?php if (!empty($user['user_image'])) { ?>
						<div class="profile-cover">
							<div class="profile-cover-img-wrapper"></div>
						</div>
						<div class="avatar avatar-xxl avatar-circle profile-cover-avatar">
							<img class="avatar-img" src="<?= upload_url('users/'.$user['user_image']) ?>" alt="Image Description">
							<span class="avatar-status avatar-status-success"></span>
						</div>
						<?php } else { ?>
						<div class="profile-cover">
							<div class="profile-cover-img-wrapper"></div>
						</div>
						<div class="avatar avatar-xxl avatar-circle profile-cover-avatar">
							<img class="avatar-img" src="<?= media_url('img/user.png') ?>" alt="Image Description">
							<span class="avatar-status avatar-status-success"></span>
						</div>
						<?php } ?>

						<h3 class="profile-username text-center"><?= $user['user_full_name']; ?></h3>

						<p class="text-muted text-center"><?= $user['role_name']; ?></p>

						<ul class="list-group list-group-unbordered">
							<li class="list-group-item">
								<b>Followers</b> <a class="pull-right">1,322</a>
							</li>
							
						</ul>
						<br>

						<a href="<?= site_url('manage/profile/cpw/') ?>" class="btn btn-info btn-block"><b>Ubah Password</b></a>
						
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->

			</div>
			<div class="col-md-8">
				<!-- About Me card -->
				<div class="card card-primary">
					<div class="card-header with-border">
						<h3 class="card-title">About Me</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<strong><i class="fa fa-book margin-r-5"></i> Nama</strong>

						<p class="text-muted">
							<?= $user['user_full_name']; ?>
						</p>

						<hr>

						<strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>

						<p class="text-muted"><?= ucfirst($this->session->userdata('uemail')); ?></p>

						<hr>

						<strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

						<p><?= $user['user_description'] ?></p>
						<a href="<?= site_url('manage/profile/edit/') ?>" class="btn btn-success"><b>Edit</b></a>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->

			</div>
		</div>
	</section>
</div>