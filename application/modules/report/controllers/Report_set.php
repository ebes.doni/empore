<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_set extends CI_Controller {

	public function __construct() {
		parent::__construct(TRUE);
		if ($this->session->userdata('logged') == NULL) {
			header("Location:" . site_url('manage/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('payment/Payment_model', 
								'class/Class_model', 
								'unit/Unit_model', 
								'jurnal/Jurnal_model',
								'student/Student_model', 
								'period/Period_model', 
								'pos/Pos_model', 
								'bulan/Bulan_model', 
								'bebas/Bebas_model', 
								'bebas/Bebas_pay_model', 
								'setting/Setting_model', 
								'kredit/Kredit_model', 
								'debit/Debit_model', 
								'logs/Logs_model', 
								'presensi/Presensi_model',
								'users/Users_model',
								'banking/Banking_model',
								'employee/Employee_model',
								'jam/Jam_model'
							));

	}

    // payment view in list
	public function index($offset = NULL) {
        // Apply Filter
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

    	// Date start
		if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
			$params['date_start'] = $q['ds'];
		}

        // Date end
		if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
			$params['date_end'] = $q['de'];
		}

		$paramsPage = $params;
		$data['period'] = $this->Period_model->get($params);
		$data['student'] = $this->Bulan_model->get(array('group'=>true));
		$data['bulan'] = $this->Bulan_model->get($params);
		$data['month'] = $this->Bulan_model->get(array('grup'=>true));
		$data['py'] = $this->Bulan_model->get(array('paymentt'=>true));
		$data['bebas'] = $this->Bebas_model->get(array('grup'=>true));
		$data['free'] = $this->Bebas_model->get($params);
		$data['dom'] = $this->Bebas_pay_model->get($params);

		$config['base_url'] = site_url('manage/report/index');
		$config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['total_rows'] = count($this->Bulan_model->get($paramsPage));

		$data['title'] = 'Laporan Keuangan';
		$data['main'] = 'report/report_list';
		$this->load->view('manage/layout', $data);
	}

	 // report unit pos
	 public function lap_unit_pos($offset = NULL) {
		// $this->output->enable_profiler(TRUE);
        // Apply Filter
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		
		$params = array();
		// Kasir
		if (isset($q['k']) && !empty($q['k']) && $q['k'] != '') {
			$params['user_full_name'] = $q['k'];
		}
		// Unit Pos
		if (isset($q['up']) && !empty($q['up']) && $q['up'] != '') {
			$params['pos_id'] = $q['up'];
		}
		// Date Start
		if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
			$params['date_start'] = $q['ds'];
		}
		// Date End
		if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
			$params['date_end'] = $q['de'];
		}

		$data['kredit'] = $this->Kredit_model->get($params);
		$data['debit'] 	= $this->Debit_model->get($params);
		$data['pos'] 	= $this->Pos_model->get();
		$data['users'] 	= $this->Users_model->get();
		$data['title'] 	= 'Laporan Unit Pos';
		$data['main'] 	= 'report/report_unitpos';
		$this->load->view('manage/layout', $data);

	}

	public function report_unitpos_xls(){
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

        // Date start
		if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
			$params['date_start'] = $q['ds'];
		}
        // Date end
		if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
			$params['date_end'] = $q['de'];
		}
		// Kasir
		if (isset($q['k']) && !empty($q['k']) && $q['k'] != '') {
			$params['user_full_name'] = $q['k'];
		}
		// Unit Pos
		if (isset($q['up']) && !empty($q['up']) && $q['up'] != '') {
			$params['pos_id'] = $q['up'];
		}

		$params['status'] 		= 1;
		$data['bulan'] 			= $this->Bulan_model->get($params);
		$data['bebas'] 			= $this->Bebas_model->get($params);
		$data['free'] 			= $this->Bebas_pay_model->get($params);
		$data['kredit'] 		= $this->Kredit_model->get($params);
		$data['debit'] 			= $this->Debit_model->get($params);
		$data['setting_school'] = $this->Setting_model->get(array('id' => SCHOOL_NAME));

		$this->load->library("PHPExcel");
		$objXLS   = new PHPExcel();
		$objSheet = $objXLS->setActiveSheetIndex(0);            
		$cell     = 6;        
		$no       = 1;

		$objSheet->setCellValue('A1', 'Laporan Unit Pos');
		$objSheet->setCellValue('A2', $data['setting_school']['setting_value'] );
		$objSheet->setCellValue('A3', 'Tanggal Laporan: '.pretty_date($q['ds'],'d F Y',false).' s/d '.pretty_date($q['de'],'d F Y',false));
		$objSheet->setCellValue('A4', 'Tanggal Unduh: '.pretty_date(date('Y-m-d h:i:s'),'d F Y, H:i',false));
		$objSheet->setCellValue('C4', 'Pengunduh: '.$this->session->userdata('ufullname'));
		
		$objSheet->setCellValue('A5', 'NO');
		$objSheet->setCellValue('B5', 'TANGGAL');
		$objSheet->setCellValue('C5', 'KODE JURNAL');
		$objSheet->setCellValue('D5', 'KETERANGAN');
		$objSheet->setCellValue('E5', 'NIS');
		$objSheet->setCellValue('F5', 'NAMA SISWA');
		$objSheet->setCellValue('G5', 'KELAS');     
		$objSheet->setCellValue('H5', 'PENERIMAAN');
		$objSheet->setCellValue('I5', 'PENGELUARAN');
		$objSheet->setCellValue('J5', 'KASIR');
		$objSheet->setCellValue('K5', 'PENERIMA');   
		
		$saldo_awal_db = 0;
		$saldo_awal_cr = 0; 
		$subtotal_debit = 0 ;

		foreach ($data['debit'] as $row) {
			$subtotal_debit += $row['debit_value'];
			if($row['student_id'] != NULL){
				$student_nis 		= $this->Student_model->get(array('id'=>$row['student_id']))['student_nis'];
				$student_fullname 	= $this->Student_model->get(array('id'=>$row['student_id']))['student_full_name']; 
				$class_name 		= $this->Student_model->get(array('id'=>$row['student_id']))['class_name'];	
			}else{
				$student_nis 		= '-';
				$student_fullname	= '-';
				$class_name			= '-';
			}
			$period_ta = '';
			if($row['pos_id'] != NULL || !empty($row['pos_id'])){
				$period = $this->Payment_model->get(array('pos_id'=>$row['pos_id']));
				if(empty($period[0])){
					$period_ta = '';
				}else{
				  $period = $period[0];
				  $period_ta = ' - T.A '.$period['period_start'].'/'.$period['period_end'];  
				}
			}
			$objSheet->setCellValue('A'.$cell, $no);
			$objSheet->setCellValueExplicit('B'.$cell, pretty_date($row['debit_date'], 'm/d/Y', FALSE));
			$objSheet->setCellValue('C'.$cell, $row['jurnal_code']);
			$objSheet->setCellValue('D'.$cell, $row['pos_name'].$period_ta);
			$objSheet->setCellValue('E'.$cell, $student_nis);
			$objSheet->setCellValue('F'.$cell, $student_fullname);
			$objSheet->setCellValue('G'.$cell, $class_name);
			$objSheet->setCellValue('H'.$cell, $row['debit_value']);
			$objSheet->setCellValue('I'.$cell, '-');
			$objSheet->setCellValue('J'.$cell, $row['user_full_name']);
			$objSheet->setCellValue('K'.$cell, $row['recipient']);
			$cell++;
			$no++;    
		}      

		$subtotal_kredit = 0;
		foreach ($data['kredit'] as $row) {
			$subtotal_kredit += $row['kredit_value'];
			if($row['student_id'] != NULL){
				$student_nis 		= $this->Student_model->get(array('id'=>$row['student_id']))['student_nis'];
				$student_fullname 	= $this->Student_model->get(array('id'=>$row['student_id']))['student_full_name']; 
				$class_name 		= $this->Student_model->get(array('id'=>$row['student_id']))['class_name'];	
			}else{
				$student_nis 		= '-';
				$student_fullname	= '-';
				$class_name			= '-';
			}
			if($row['pos_id'] != NULL || !empty($row['pos_id'])){
				$period = $this->Payment_model->get(array('pos_id'=>$row['pos_id']));
				if(empty($period[0])){
					$period_ta = '';
				}else{
				  $period = $period[0];
				  $period_ta = ' - T.A '.$period['period_start'].'/'.$period['period_end'];  
				}
			}

			$objSheet->setCellValue('A'.$cell, $no);
			$objSheet->setCellValueExplicit('B'.$cell, pretty_date($row['kredit_date'], 'm/d/Y', FALSE));
			$objSheet->setCellValue('C'.$cell, $row['jurnal_code']);
			$objSheet->setCellValue('D'.$cell, $row['pos_name'].$period_ta);
			$objSheet->setCellValue('E'.$cell, $student_nis);
			$objSheet->setCellValue('F'.$cell, $student_fullname);
			$objSheet->setCellValue('G'.$cell, $class_name);
			$objSheet->setCellValue('H'.$cell, '-');
			$objSheet->setCellValue('I'.$cell, $row['kredit_value']);
			$objSheet->setCellValue('J'.$cell, $row['user_full_name']);
			$objSheet->setCellValue('K'.$cell, $row['recipient']);
			$cell++;
			$no++;    
		}

		// foreach ($data['bulan'] as $row) {
		// 	$objSheet->setCellValue('A'.$cell, $no);
		// 	$objSheet->setCellValueExplicit('B'.$cell, $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'].'-'.$row['month_name'],PHPExcel_Cell_DataType::TYPE_STRING);
		// 	$objSheet->setCellValueExplicit('C'.$cell, $row['student_full_name'],PHPExcel_Cell_DataType::TYPE_STRING);
		// 	$objSheet->setCellValueExplicit('D'.$cell, $row['class_name'],PHPExcel_Cell_DataType::TYPE_STRING);
		// 	$objSheet->setCellValue('E'.$cell, pretty_date($row['bulan_date_pay'], 'm/d/Y', FALSE));
		// 	$objSheet->setCellValue('F'.$cell, $row['bulan_bill']);
		// 	$objSheet->setCellValue('G'.$cell, ' ');
		// 	$cell++;
		// 	$no++;    
		// }

		// foreach ($data['free'] as $row) {
		// 	$objSheet->setCellValue('A'.$cell, $no);
		// 	$objSheet->setCellValueExplicit('B'.$cell, $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'],PHPExcel_Cell_DataType::TYPE_STRING);
		// 	$objSheet->setCellValueExplicit('C'.$cell, $row['student_full_name'],PHPExcel_Cell_DataType::TYPE_STRING);
		// 	$objSheet->setCellValueExplicit('D'.$cell, $row['class_name'],PHPExcel_Cell_DataType::TYPE_STRING);
		// 	$objSheet->setCellValue('E'.$cell, pretty_date($row['bebas_pay_input_date'], 'm/d/Y', FALSE));
		// 	$objSheet->setCellValue('F'.$cell, $row['bebas_pay_bill']);
		// 	$objSheet->setCellValue('G'.$cell, ' ');
		// 	$cell++;
		// 	$no++;    
		// }

		$objXLS->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objXLS->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$objXLS->getActiveSheet()->getColumnDimension('C')->setWidth(20);		
		$objXLS->getActiveSheet()->getColumnDimension('D')->setWidth(40);
		$objXLS->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$objXLS->getActiveSheet()->getColumnDimension('F')->setWidth(30);
		$objXLS->getActiveSheet()->getColumnDimension('G')->setWidth(7);

		foreach(range('H', 'Z') as $alphabet)
		{
			$objXLS->getActiveSheet()->getColumnDimension($alphabet)->setWidth(20);
		}

		$objXLS->getActiveSheet()->getColumnDimension('N')->setWidth(20);

		$font = array('font' => array( 'bold' => true, 'color' => array(
			'rgb'  => 'FFFFFF')));
		$objXLS->getActiveSheet()
		->getStyle('A5:K5')
		->applyFromArray($font);

		$objXLS->getActiveSheet()
		->getStyle('A5:K5')
		->getFill()
		->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		->getStartColor()
		->setRGB('000');
		$objXLS->getActiveSheet()->getStyle('A1')->getFont()->setBold( true );
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5'); 
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="LAPORAN_UNIT_POS_'.date('dmY').'.xls"'); 
		header('Cache-Control: max-age=0'); 
		$objWriter->save('php://output'); 
		exit();      
	}


	 // report jurnal
	 public function lap_jurnal($offset = NULL) {
		// $this->output->enable_profiler(TRUE);
        // Apply Filter
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		
		$data['jurnal'] = $this->Jurnal_model->get();
		$data['users'] = $this->Users_model->get();
		$data['title'] = 'Laporan Keuangan';
		$data['main'] = 'report/report_jurnal';
		$this->load->view('manage/layout', $data);
	}

	public function report_jurnal_xls(){
    	// Apply Filter
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

        // Date start
		if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
			$params['date_start'] = $q['ds'];
		}
        // Date end
		if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
			$params['date_end'] = $q['de'];
		}
		// Kasir
		if (isset($q['k']) && !empty($q['k']) && $q['k'] != '') {
			$params['user_full_name'] = $q['k'];
		}

		$params['status'] = 1;
		$data['jurnal'] = $this->Jurnal_model->get();

		$data['bulan'] = $this->Bulan_model->get($params);
		$data['bebas'] = $this->Bebas_model->get($params);
		$data['free'] = $this->Bebas_pay_model->get($params);
		$data['kredit'] = $this->Kredit_model->get($params);
		$data['debit'] = $this->Debit_model->get($params);
		$data['setting_school'] = $this->Setting_model->get(array('id' => SCHOOL_NAME));

		$this->load->library("PHPExcel");
		$objXLS   = new PHPExcel();
		$objSheet = $objXLS->setActiveSheetIndex(0);            
		$cell     = 6;        
		$no       = 1;

		$objSheet->setCellValue('A1', 'Laporan Jurnal');
		$objSheet->setCellValue('A2', $data['setting_school']['setting_value'] );
		$objSheet->setCellValue('A3', 'Tanggal Laporan: '.pretty_date($q['ds'],'d F Y',false).' s/d '.pretty_date($q['de'],'d F Y',false));
		$objSheet->setCellValue('A4', 'Tanggal Unduh: '.pretty_date(date('Y-m-d h:i:s'),'d F Y, H:i',false));
		$objSheet->setCellValue('C4', 'Pengunduh: '.$this->session->userdata('ufullname'));
			
		$objSheet->setCellValue('A5', 'NO');
		$objSheet->setCellValue('B5', 'KODE JURNAL');
		$objSheet->setCellValue('C5', 'NAMA JURNAL');
		$objSheet->setCellValue('D5', 'TYPE');
		$objSheet->setCellValue('E5', 'KATEGORI');
		$objSheet->setCellValue('F5', 'TOTAL');
		
		$subtotal_debit = 0;
		$subtotal_kredit = 0;	
		$saldo_awal_db = 0; 
		$total_kas_masuk = 0;
		$total_kas_keluar = 0;

		foreach($this->Debit_model->get(array('date_start'=>$q['ds'], 'date_end'=>$q['de'], 'user_full_name'=>$q['k'])) as $row) {
			$total_kas_masuk += $row['debit_value'];
		}
		foreach($this->Kredit_model->get(array('date_start'=>$q['ds'], 'date_end'=>$q['de'], 'user_full_name'=>$q['k'])) as $row) {
			$total_kas_keluar += $row['kredit_value'];
		}

		foreach($data['jurnal'] as $row){
			$debit_total = 0;
			$credit_total = 0;
			foreach($this->Debit_model->get(array('jurnal_id'=>$row['id'], 'date_start'=>$q['ds'], 'date_end'=>$q['de'], 'user_full_name'=>$q['k'])) as $db){
				$debit_total += $db['debit_value'];
			}
			foreach($this->Kredit_model->get(array('jurnal_id'=>$row['id'], 'date_start'=>$q['ds'], 'date_end'=>$q['de'], 'user_full_name'=>$q['k'])) as $cr){
				$credit_total += $cr['kredit_value'];
			}
			$header_style = array('font' => array( 'bold' => true, 'color' => array('rgb'  => '000')));
			$objSheet->setCellValue('A'.$cell, $no)->getStyle('A'.$cell)->applyFromArray($header_style);
			$objSheet->setCellValueExplicit('B'.$cell, $row['code'],PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('B'.$cell)->applyFromArray($header_style);
			$objSheet->setCellValueExplicit('C'.$cell, $row['desc'],PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('C'.$cell)->applyFromArray($header_style);
			$objSheet->setCellValueExplicit('D'.$cell, $row['type'],PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('D'.$cell)->applyFromArray($header_style);
			$objSheet->setCellValueExplicit('E'.$cell, $row['category'],PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('E'.$cell)->applyFromArray($header_style);
			if($row['type']=='Kas Masuk'){
				if($row['category'] == '#'){
					$objSheet->setCellValue('F'.$cell, $total_kas_masuk)->getStyle('F'.$cell)->applyFromArray($header_style);
				}else{
					$objSheet->setCellValue('F'.$cell, $debit_total)->getStyle('F'.$cell)->applyFromArray($header_style);
				}
			}
			if($row['type']=='Kas Keluar'){
				if($row['category'] == '#'){
					$objSheet->setCellValue('F'.$cell, $total_kas_keluar)->getStyle('F'.$cell)->applyFromArray($header_style);
				}else{
					$objSheet->setCellValue('F'.$cell, $credit_total)->getStyle('F'.$cell)->applyFromArray($header_style);
				}
			}
			
			$cell+=1;
			$objSheet->setCellValue('B'.$cell, 'Nama Pos');
			$objSheet->setCellValue('C'.$cell, 'Tanggal');
			$objSheet->setCellValue('D'.$cell, 'Kasir');
			$objSheet->setCellValue('E'.$cell, 'Penerimaan');
			$objSheet->setCellValue('F'.$cell, 'Pengeluaran');
			$cell+=1;
			$total_pos=1;
			foreach($this->Debit_model->get(array('jurnal_id'=>$row['id'], 'date_start'=>$q['ds'], 'date_end'=>$q['de'], 'user_full_name'=>$q['k'])) as $db){
				$subtotal_debit += $db['debit_value'];
				$objSheet->setCellValueExplicit('B'.$cell, $total_pos.'. '.$db['debit_desc'],PHPExcel_Cell_DataType::TYPE_STRING);
				$objSheet->setCellValue('C'.$cell, pretty_date($db['debit_date'], 'm/d/Y', FALSE));
				$objSheet->setCellValue('D'.$cell, $db['user_full_name']);
				$objSheet->setCellValue('E'.$cell, $db['debit_value']);
				$objSheet->setCellValue('F'.$cell, '-');
				$cell++;
				$total_pos++;
			}

			foreach($this->Kredit_model->get(array('jurnal_id'=>$row['id'], 'date_start'=>$q['ds'], 'date_end'=>$q['de'], 'user_full_name'=>$q['k'])) as $cr){
				$subtotal_kredit += $cr['kredit_value'];
				$objSheet->setCellValueExplicit('B'.$cell, $total_pos.'. '.$cr['kredit_desc'],PHPExcel_Cell_DataType::TYPE_STRING);
				$objSheet->setCellValue('C'.$cell, pretty_date($cr['kredit_date'], 'm/d/Y', FALSE));
				$objSheet->setCellValue('D'.$cell, $cr['user_full_name']);
				$objSheet->setCellValue('E'.$cell, '-');
				$objSheet->setCellValue('F'.$cell, $cr['kredit_value']);
				$cell++;
				$total_pos++;
			}
			$cell++;
			$no++;    
		}

		// foreach ($data['bulan'] as $row) {

		// 	$objSheet->setCellValue('A'.$cell, $no);
		// 	$objSheet->setCellValueExplicit('B'.$cell, $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'].'-'.$row['month_name'],PHPExcel_Cell_DataType::TYPE_STRING);
		// 	$objSheet->setCellValueExplicit('C'.$cell, $row['student_full_name'],PHPExcel_Cell_DataType::TYPE_STRING);
		// 	$objSheet->setCellValueExplicit('D'.$cell, $row['class_name'],PHPExcel_Cell_DataType::TYPE_STRING);
		// 	$objSheet->setCellValue('E'.$cell, pretty_date($row['bulan_date_pay'], 'm/d/Y', FALSE));
		// 	$objSheet->setCellValue('F'.$cell, $row['bulan_bill']);
		// 	$objSheet->setCellValue('G'.$cell, ' ');
		// 	$cell++;
		// 	$no++;    
		// }

		// foreach ($data['free'] as $row) {

		// 	$objSheet->setCellValue('A'.$cell, $no);
		// 	$objSheet->setCellValueExplicit('B'.$cell, $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'],PHPExcel_Cell_DataType::TYPE_STRING);
		// 	$objSheet->setCellValueExplicit('C'.$cell, $row['student_full_name'],PHPExcel_Cell_DataType::TYPE_STRING);
		// 	$objSheet->setCellValueExplicit('D'.$cell, $row['class_name'],PHPExcel_Cell_DataType::TYPE_STRING);
		// 	$objSheet->setCellValue('E'.$cell, pretty_date($row['bebas_pay_input_date'], 'm/d/Y', FALSE));
		// 	$objSheet->setCellValue('F'.$cell, $row['bebas_pay_bill']);
		// 	$objSheet->setCellValue('G'.$cell, ' ');
		// 	$cell++;
		// 	$no++;    
		// }

		
		$objXLS->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$objXLS->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$objXLS->getActiveSheet()->getColumnDimension('C')->setWidth(20);

		foreach(range('D', 'Z') as $alphabet)
		{
			$objXLS->getActiveSheet()->getColumnDimension($alphabet)->setWidth(20);
		}

		$objXLS->getActiveSheet()->getColumnDimension('N')->setWidth(20);

		$font = array('font' => array( 'bold' => true, 'color' => array(
			'rgb'  => 'FFFFFF')));
		$objXLS->getActiveSheet()
		->getStyle('A5:F5')
		->applyFromArray($font);

		$objXLS->getActiveSheet()
		->getStyle('A5:F5')
		->getFill()
		->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		->getStartColor()
		->setRGB('000');
		$objXLS->getActiveSheet()->getStyle('A1')->getFont()->setBold( true );
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5'); 
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="LAPORAN_JURNAL_'.date('dmY').'.xls"'); 
		header('Cache-Control: max-age=0'); 
		$objWriter->save('php://output'); 
		exit();
	}

	 // report tabungan
	 public function lap_banking($offset = NULL) {
        // Apply Filter
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

    	// Unit
		if (isset($q['u']) && !empty($q['u']) && $q['u'] != '') {
			$params['unit_id'] = $q['u'];
		}
		// Period
		if (isset($q['p']) && !empty($q['p']) && $q['p'] != '') {
			$params['period_id'] = $q['p'];
		}
		// Class
		if (isset($q['c']) && !empty($q['c']) && $q['c'] != '') {
			$params['class_id'] = $q['c'];
		}
		// Student
		if (isset($q['s']) && !empty($q['s']) && $q['s'] != '') {
			$params['student_id'] = $q['s'];
		}
		// // Kasir
		// if (isset($q['k']) && !empty($q['k']) && $q['k'] != '') {
		// 	$params['created_by'] = $q['k'];
		// }
		
		$params['student_status'] = 1;
		$data['period'] 	= $this->Period_model->get();
		$data['unit'] 		= $this->Unit_model->get();
		$data['class'] 		= $this->Class_model->get();
		$data['student'] 	= $this->Banking_model->get_student($params);
		$data['users']		= $this->Users_model->get();
		$data['title'] 		= 'Laporan Tabungan Siswa';
		$data['main'] 		= 'report/report_banking';
		$this->load->view('manage/layout', $data);
	}

	public function report_banking_xls(){
    	// Apply Filter
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

        // Unit
		if (isset($q['u']) && !empty($q['u']) && $q['u'] != '') {
			$params['unit_id'] = $q['u'];
		}
		// Period
		if (isset($q['p']) && !empty($q['p']) && $q['p'] != '') {
			$params['period_id'] = $q['p'];
		}
		// Class
		if (isset($q['c']) && !empty($q['c']) && $q['c'] != '') {
			$params['class_id'] = $q['c'];
		}
		// Student
		if (isset($q['s']) && !empty($q['s']) && $q['s'] != '') {
			$params['student_id'] = $q['s'];
		}
		// Kasir
		if (isset($q['k']) && !empty($q['k']) && $q['k'] != '') {
			$params['created_by'] = $q['k'];
		}

		$data['student'] = $this->Banking_model->get_student($params);
		$data['setting_school'] = $this->Setting_model->get(array('id' => SCHOOL_NAME));
		$period = $this->Period_model->get(array('period_id'=>$q['p']));
		$unit = $this->Unit_model->get(array('unit_id'=>$q['u']));
		$class = $this->Class_model->get(array('class_id'=>$q['c']));

		$this->load->library("PHPExcel");
		$objXLS   = new PHPExcel();
		$objSheet = $objXLS->setActiveSheetIndex(0);            
		$cell     = 8;        
		$no       = 1;

		$objSheet->setCellValue('A1', 'Rekap Tabungan');
		$objSheet->setCellValue('A2', $data['setting_school']['setting_value'] );
		$objSheet->setCellValue('A3', 'Tahun Ajaran: '.$period['period_start'].'/'.$period['period_end']);
		if(isset($unit['name'])){
			$objSheet->setCellValue('A4', 'Unit: '.$unit['name']);
		}else{
			$objSheet->setCellValue('A4', 'Unit: Semua Unit');
		}
		if(isset($class['class_name'])){
			$objSheet->setCellValue('A5', 'Kelas: '.$class['class_name']);
		}else{
			$objSheet->setCellValue('A5', 'Kelas: Semua Kelas');
		}
		
		$objSheet->setCellValue('A6', 'Tanggal Unduh: '.pretty_date(date('Y-m-d h:i:s'),'d F Y, H:i',false));
		$objSheet->setCellValue('C6', 'Pengunduh: '.$this->session->userdata('ufullname'));	
		$objSheet->setCellValue('A7', 'NO');
		$objSheet->setCellValue('B7', 'NIS');
		$objSheet->setCellValue('C7', 'NAMA');
		$objSheet->setCellValue('D7', 'KELAS');
		$objSheet->setCellValue('E7', 'DEBIT');
		$objSheet->setCellValue('F7', 'KREDIT');
		$objSheet->setCellValue('G7', 'SALDO');
		
		foreach($data['student'] as $row){
			$debit_total = 0;
			$credit_total = 0;
			foreach($this->Banking_model->get(array('student_id'=>$row['student_id'], 'period_id'=>$q['p'])) as $debit){
										$debit_total += $debit['debit_val'];
									}
			foreach($this->Banking_model->get(array('student_id'=>$row['student_id'], 'period_id'=>$q['p'])) as $credit){
				$credit_total += $credit['credit_val'];
			}
			$saldo_tabungan = $debit_total - $credit_total;
			
			if($saldo_tabungan == 0){
				continue;
			}else{
				$objSheet->setCellValue('A'.$cell, $no)->getStyle('A'.$cell);
				$objSheet->setCellValueExplicit('B'.$cell, $row['student_nis'],PHPExcel_Cell_DataType::TYPE_STRING);
				$objSheet->setCellValueExplicit('C'.$cell, $row['student_full_name'],PHPExcel_Cell_DataType::TYPE_STRING);
				$objSheet->setCellValueExplicit('D'.$cell, $row['class_name'],PHPExcel_Cell_DataType::TYPE_STRING);
				$objSheet->setCellValueExplicit('E'.$cell, 'Rp. ' .number_format($debit_total));
				$objSheet->setCellValueExplicit('F'.$cell, 'Rp. ' .number_format($credit_total));
				$objSheet->setCellValueExplicit('G'.$cell, 'Rp. ' .number_format($saldo_tabungan));	
			}
			
			$cell++;
			$no++;    
		}
		
		$objXLS->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$objXLS->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objXLS->getActiveSheet()->getColumnDimension('C')->setWidth(40);

		foreach(range('D', 'Z') as $alphabet){
			$objXLS->getActiveSheet()->getColumnDimension($alphabet)->setWidth(20);
		}

		$objXLS->getActiveSheet()->getColumnDimension('N')->setWidth(20);

		$font = array('font' => array( 'bold' => true, 'color' => array(
			'rgb'  => 'FFFFFF')));
		$objXLS->getActiveSheet()->getStyle('A7:G7')->applyFromArray($font);

		$objXLS->getActiveSheet()
		->getStyle('A7:G7')
		->getFill()
		->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		->getStartColor()
		->setRGB('000');
		$objXLS->getActiveSheet()->getStyle('A1')->getFont()->setBold( true );
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5'); 
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="LAPORAN_TABUNGAN_SISWA_'.date('dmY').'.xls"'); 
		header('Cache-Control: max-age=0'); 
		$objWriter->save('php://output'); 
		exit();
	}

	function get_mclass(){
		$unit_id = $this->input->post('id_majors');
		$data['class'] = $this->Class_model->get(array('unit_id'=>$unit_id));
		$msg = '<div class="col-md-2">  ';
			$msg .= '<div class="form-group">';
			$msg .= '<label>Kelas</label>';
				$msg .= '<select class="form-control" name="c" id="class_id" required>';
					$msg .= '<option value="">-- Pilih Kelas --</option>';
					$msg .= '<option selected="" value="all">Semua Kelas</option>';
				foreach($data['class'] as $row){
					$msg .= '<option value="'.$row['class_id'].'">'.$row['class_name'].'</option>';
				}  
				$msg .= '</select>';
			$msg .='</div>
				</div>';		
		echo $msg;
	}

	function get_mstudent(){
		$unit_id = $this->input->post('id_majors');
		$class_id = $this->input->post('id_class');
		$data['student'] = $this->Student_model->get(array('unit_id'=>$unit_id, 'class_id'=>$class_id, 'status'=>1));

		$msg = '<div class="col-md-2">  ';
			$msg .= '<div class="form-group">';
			$msg .= '<label>Siswa</label>';
				$msg .= '<select class="form-control" name="s" id="student_id" required>';
					$msg .= '<option value="">-- Pilih Siswa --</option>';
					$msg .= '<option selected="" value="all">Semua Siswa</option>';
				foreach($data['student'] as $row){
					$msg .= '<option value="'.$row['student_id'].'">'.$row['student_full_name'].'</option>';
				}  
		
		$msg .= '</select>';
		$msg .='</div>
				</div>';
				
		echo $msg;
	}

	// report presensi employee
	public function presensi_employee_date($offset = NULL) {
        // Apply Filter
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

    	// Date start
		if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
			$params['date_start'] = $q['ds'];
		}
        // Date end
		if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
			$params['date_end'] = $q['de'];
		}
		// Employee id
		if (isset($q['ei']) && !empty($q['ei']) && $q['ei'] != '') {
			$params['employee_id'] = $q['ei'];
		}

		$data['employee'] = $this->Employee_model->get();
		$data['jam'] = $this->Jam_model->get(array('jam_id'=>1));
	
		$data['title'] = 'Laporan Presensi Pegawai';
		$data['main'] = 'report/presensi_employee_date';
		$this->load->view('manage/layout', $data);
	
	}

	// report presensi employee by month
	public function presensi_employee_month($offset = NULL) {
		// Apply Filter
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

		// Monthyears
		if (isset($q['my']) && !empty($q['my']) && $q['my'] != '') {
			$params['monthyears'] = $q['my'];
		}

		$data['presensi'] = $this->Presensi_model->get($params);
		$data['employee'] = $this->Employee_model->get();
		/*for period month*/
		$date 	= $this->input->get('my', TRUE);
		$month 	= date('n', strtotime($date));
		$year 	= date('Y', strtotime($date));
		$data['days_in_month'] = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		$data['bulan'] = date('F', strtotime($date));
		$data['tahun'] = date('Y', strtotime($date));
		$data['title'] = 'Laporan Presensi Pegawai';
		$data['main'] = 'report/presensi_employee_month';
		$this->load->view('manage/layout', $data);
	}

	// export xls presensi employee by month
	public function presensi_employee_month_xls(){
        // Apply Filter
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

       // Monthyears
		if (isset($q['my']) && !empty($q['my']) && $q['my'] != '') {
			$params['monthyears'] = $q['my'];
		}

		$data['setting_school'] = $this->Setting_model->get(array('id' => SCHOOL_NAME));
		$date 	= $this->input->get('my', TRUE);
		$data['bulan'] = date('F', strtotime($date));
		$data['tahun'] = date('Y', strtotime($date));

		$this->load->library("PHPExcel");
		$objXLS   = new PHPExcel();
		$objSheet = $objXLS->setActiveSheetIndex(0);            
		$cell     = 7;        
		$no       = 1;

		$font = array('font' => array( 'bold' => true, 'color' => array('rgb'  => 'FFFFFF')));
		$styleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array(
							'rgb'  => 'FFFFFF' 
						),
					),
				),
				'wrap' => true,
			),
		);

		$objSheet->setCellValue('A1', 'LAPORAN PRESENSI GURU BERDASARKAN BULAN');
		$objSheet->setCellValue('A2', $data['setting_school']['setting_value'] );
		$objSheet->setCellValue('A3', 'Bulan : '.$data['bulan'].' '.$data['tahun']);
		$objSheet->setCellValue('A4', 'Tanggal Unduh: '.pretty_date(date('Y-m-d h:i:s'),'d F Y, H:i',false));
		$objSheet->setCellValue('D4', 'Pengunduh: '.$this->session->userdata('ufullname'));

		$objSheet->setCellValue('A5', 'NO');
		$objSheet->setCellValue('B5', 'NIP');
		$objSheet->setCellValue('C5', 'NAMA GURU');

		$objSheet->mergeCells('A5:A6');
		$objSheet->mergeCells('B5:B6');
		$objSheet->mergeCells('C5:C6');

		$objXLS->getActiveSheet()->getStyle('A5:A6')->applyFromArray($styleArray);
		$objXLS->getActiveSheet()->getStyle('B5:B6')->applyFromArray($styleArray);
		$objXLS->getActiveSheet()->getStyle('C5:C6')->applyFromArray($styleArray);

		/*for period month*/
		$date 	= $this->input->get('my', TRUE);
		$month 	= date('n', strtotime($date));
		$year 	= date('Y', strtotime($date));
		$data['days_in_month'] = cal_days_in_month(CAL_GREGORIAN, $month, $year);

		

		/**Header */
		$col_h=0;
		$start_alphabet=4;		
		$merge_end_tgl=0;
		for($i=1; $i<=$data['days_in_month']; $i++) {
			if(($start_alphabet+$col_h) <= 26){
				$objSheet->setCellValue(getCell($start_alphabet+$col_h).'6', $i); 
				$objXLS->getActiveSheet()->getStyle(getCell($start_alphabet+$col_h).'6')->applyFromArray($font);
				$col_h++;
				$col_h2=0;
				$start_alphabet2=1;
			}elseif(($start_alphabet+$col_h) > 26){				
				$objSheet->setCellValue('A'.getCell($start_alphabet2+$col_h2).'6', $i); 
				$objXLS->getActiveSheet()->getStyle('A'.getCell($start_alphabet2+$col_h2).'6')->applyFromArray($font);
				// set width column
				$objXLS->getActiveSheet()->getColumnDimension('A'.getCell($start_alphabet2+$col_h2))->setWidth(5);
				$col_h2++;
				$col_ttl_jam = $start_alphabet2+$col_h2;					
				$merge_end_tgl = $start_alphabet2+($col_h2-1);
			}			
			
		}
		
		$objSheet->setCellValue('D5', 'TANGGAL');
		$objSheet->mergeCells(getCell($start_alphabet).'5:'.'A'.getCell($merge_end_tgl).'5');
		$objSheet->getStyle('D5:'.'A'.getCell($merge_end_tgl).'5')->applyFromArray($styleArray);
		
		//set merge cells total jam  
		$objSheet->setCellValue('A'.getCell($col_ttl_jam).'5', 'TOTAL JAM MASUK');	
		$objSheet->mergeCells('A'.getCell($col_ttl_jam).'5:'.'A'.getCell($col_ttl_jam).'6');
		$objSheet->getStyle('A'.getCell($col_ttl_jam).'5:'.'A'.getCell($col_ttl_jam).'6')->applyFromArray($styleArray);
		// set width column
		$objXLS->getActiveSheet()->getColumnDimension('A'.getCell($col_ttl_jam))->setWidth(15);

		$col_ttl_alpa = $col_ttl_jam+1;
		//set merge cells total jam  
		$objSheet->setCellValue('A'.getCell($col_ttl_alpa).'5', 'TOTAL JAM ALPA');	
		$objSheet->mergeCells('A'.getCell($col_ttl_alpa).'5:'.'A'.getCell($col_ttl_alpa).'6');
		$objSheet->getStyle('A'.getCell($col_ttl_alpa).'5:'.'A'.getCell($col_ttl_alpa).'6')->applyFromArray($styleArray);
		// set width column
		$objXLS->getActiveSheet()->getColumnDimension('A'.getCell($col_ttl_alpa))->setWidth(15);
					
		$data['employee'] = $this->Employee_model->get();

		foreach ($data['employee'] as $emp){
			
			$objSheet->setCellValue('A'.$cell, $no);
			$objSheet->setCellValueExplicit('B'.$cell, $emp['nip'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objSheet->setCellValueExplicit('C'.$cell, $emp['name'],PHPExcel_Cell_DataType::TYPE_STRING);
			
			$total_jam_msk 	= 0;
			$total_alpa = 0;
			$monthyears = isset($q['my']) ? $q['my'] : '';
			
			/**Data Details */
			$col_d=0;
			$start_alphabet_presensi=4;
			for($i=1; $i<=$data['days_in_month']; $i++) {
				$ttl_jam_employee = $this->Presensi_model->get_total_jam($emp['id'], $monthyears.'-'.$i, 'approved' )['total_jam'] ? : 0;	
				$total_jam_msk += $ttl_jam_employee;	

				/**get setting jam mengajar*/
				$set_jam = $this->Jam_model->get(array('jam_id'=>1))['jam'];
				$ttl_alpa_emp = $set_jam * 2;
				$total_alpa += $ttl_alpa_emp;
				
				if(($start_alphabet_presensi+$col_d) <= 26){
					$objSheet->setCellValue(getCell($start_alphabet_presensi+$col_d).$cell, $ttl_jam_employee); 
					$col_d++;
					$col_d2=0;
					$start_alphabet_presensi2=1;
				}elseif(($start_alphabet_presensi+$col_d) > 26){				
					$objSheet->setCellValue('A'.getCell($start_alphabet_presensi2+$col_d2).$cell, $ttl_jam_employee); 
					$col_d2++;
					$objSheet->setCellValue('A'.getCell($start_alphabet_presensi2+$col_d2).$cell, $total_jam_msk);
					$objSheet->setCellValue('A'.getCell($start_alphabet_presensi2+$col_d2+1).$cell, $total_alpa-$total_jam_msk);
				}				
			}
			
			$cell++;
			$no++;    
		}

		$objXLS->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$objXLS->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objXLS->getActiveSheet()->getColumnDimension('C')->setWidth(35);

		foreach(range('D', 'Z') as $alphabet){
			$objXLS->getActiveSheet()->getColumnDimension($alphabet)->setWidth(5);
		}

		
		$font = array('font' => array( 'bold' => true, 'color' => array('rgb'  => 'FFFFFF')));
		$objXLS->getActiveSheet()->getStyle('A5:'.'A'.getCell($col_ttl_alpa).'6')->applyFromArray($font);

		$objXLS->getActiveSheet()->getStyle('A5:'.'A'.getCell($col_ttl_alpa).'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		->getStartColor()->setRGB('000');

		$objXLS->getActiveSheet()->getStyle('A1')->getFont()->setBold( true );
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5'); 
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="LAPORAN_PRESENSI_GURU_BY_MONTH'.date('dmY').'.xls"'); 
		header('Cache-Control: max-age=0'); 
		$objWriter->save('php://output'); 
		exit();      
	}
	// end export xls presensi employee by month

	// export xls presensi employee by date
	public function presensi_employee_xls(){
        // Apply Filter
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

        // Date start
		if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
			$params['date_start'] = $q['ds'];
		}
        // Date end
		if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
			$params['date_end'] = $q['de'];
		}
		// Employee
		if (isset($q['ei']) && !empty($q['ei']) && $q['ei'] != '') {
			$params['employee_id'] = $q['ei'];
		}

		$jam = $this->Jam_model->get(array('jam_id'=>1))['jam'];
		$data['setting_school'] = $this->Setting_model->get(array('id' => SCHOOL_NAME));

		$this->load->library("PHPExcel");
		$objXLS   = new PHPExcel();
		$objSheet = $objXLS->setActiveSheetIndex(0);            
		$cell     = 6;        
		$no       = 1;

		$objSheet->setCellValue('A1', 'Laporan Presensi Guru');
		$objSheet->setCellValue('A2', $data['setting_school']['setting_value'] );
		$objSheet->setCellValue('A3', 'Tanggal Laporan: '.pretty_date($q['ds'],'d F Y',false).' s/d '.pretty_date($q['de'],'d F Y',false));
		$objSheet->setCellValue('A4', 'Tanggal Unduh: '.pretty_date(date('Y-m-d h:i:s'),'d F Y, H:i',false));
		$objSheet->setCellValue('C4', 'Pengunduh: '.$this->session->userdata('ufullname'));

		$objSheet->setCellValue('A5', 'NO');
		$objSheet->setCellValue('B5', 'TANGGAL');
		$objSheet->setCellValue('C5', 'NIP');
		$objSheet->setCellValue('D5', 'NAMA GURU');
		
		$i=1;
		foreach(range('e','k') as $v){
			$objSheet->setCellValue($v.'5', 'JAM '.$i);
			$i++;
		}
		
		$objSheet->setCellValue('L5', 'TOTAL JAM');     

		$begin 		= new DateTime($q['ds']);
		$end 		= new DateTime($q['de']);
		$interval 	= DateInterval::createFromDateString('1 day');
		$period 	= new DatePeriod($begin, $interval, $end);
		
		foreach ($period as $dt){
			$presence_date =  $dt->format("Y-m-d"); 
			$employee_id = $q['ei'];
			
			$total_jam=0; 
			foreach($this->Presensi_model->get_presence(array('presence_date'=>$presence_date, 
														'employee_id'=>$employee_id
														)) as $p):
				$total_jam += $p['durasi'];
			endforeach;

			$objSheet->setCellValue('A'.$cell, $no);
			$objSheet->setCellValueExplicit('B'.$cell, $presence_date,PHPExcel_Cell_DataType::TYPE_STRING);
			$objSheet->setCellValueExplicit('C'.$cell, $this->Employee_model->get(array('employee_id'=>$employee_id))['nip'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objSheet->setCellValueExplicit('D'.$cell, $this->Employee_model->get(array('employee_id'=>$employee_id))['name'],PHPExcel_Cell_DataType::TYPE_STRING);
			
				$objSheet->setCellValue('E'.$cell, $this->Presensi_model->get(array('presence_date'=>$presence_date,
				'employee_id'=>$employee_id,
				'jam'=>1))['class_name']);	
			
				$objSheet->setCellValue('F'.$cell, $this->Presensi_model->get(array('presence_date'=>$presence_date,
				'employee_id'=>$employee_id,
				'jam'=>2))['class_name']);
				
				$objSheet->setCellValue('G'.$cell, $this->Presensi_model->get(array('presence_date'=>$presence_date,
				'employee_id'=>$employee_id,
				'jam'=>3))['class_name']);
				
				$objSheet->setCellValue('H'.$cell, $this->Presensi_model->get(array('presence_date'=>$presence_date,
				'employee_id'=>$employee_id,
				'jam'=>4))['class_name']);
				
				$objSheet->setCellValue('I'.$cell, $this->Presensi_model->get(array('presence_date'=>$presence_date,
				'employee_id'=>$employee_id,
				'jam'=>5))['class_name']);

				$objSheet->setCellValue('J'.$cell, $this->Presensi_model->get(array('presence_date'=>$presence_date,
				'employee_id'=>$employee_id,
				'jam'=>6))['class_name']);
			
				$objSheet->setCellValue('K'.$cell, $this->Presensi_model->get(array('presence_date'=>$presence_date,
				'employee_id'=>$employee_id,
				'jam'=>7))['class_name']);
			
			$objSheet->setCellValue('L'.$cell, $total_jam);
			$cell++;
			$no++;    
		}

		$objXLS->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$objXLS->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objXLS->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objXLS->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objXLS->getActiveSheet()->getColumnDimension('E')->setWidth(2);
		$objXLS->getActiveSheet()->getColumnDimension('F')->setWidth(2);
		$objXLS->getActiveSheet()->getColumnDimension('G')->setWidth(2);
		$objXLS->getActiveSheet()->getColumnDimension('H')->setWidth(2);
		$objXLS->getActiveSheet()->getColumnDimension('I')->setWidth(2);
		$objXLS->getActiveSheet()->getColumnDimension('J')->setWidth(2);
		$objXLS->getActiveSheet()->getColumnDimension('K')->setWidth(2);

		foreach(range('D', 'Z') as $alphabet){
			$objXLS->getActiveSheet()->getColumnDimension($alphabet)->setWidth(20);
		}

		$objXLS->getActiveSheet()->getColumnDimension('N')->setWidth(20);

		$font = array('font' => array( 'bold' => true, 'color' => array(
			'rgb'  => 'FFFFFF')));
		$objXLS->getActiveSheet()
		->getStyle('A5:L5')
		->applyFromArray($font);

		$objXLS->getActiveSheet()
		->getStyle('A5:L5')
		->getFill()
		->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		->getStartColor()
		->setRGB('000');
		$objXLS->getActiveSheet()->getStyle('A1')->getFont()->setBold( true );
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5'); 
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="LAPORAN_PRESENSI_GURU_'.date('dmY').'.xls"'); 
		header('Cache-Control: max-age=0'); 
		$objWriter->save('php://output'); 
		exit();      
	}
	// end export xls presensi employee by date

	// view report tagihan santri
	public function tagihan_santri($offset = NULL) {
        // Apply Filter
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

    	// Month start
		if (isset($q['d']) && !empty($q['d']) && $q['d'] != '') {
			$params['month_id'] = $q['d'];
		}
        // Month end
		if (isset($q['s']) && !empty($q['s']) && $q['s'] != '') {
			$params['month_id'] = $q['s'];
		}
		// Class
		if (isset($q['c']) && !empty($q['c']) && $q['c'] != '') {
			$params['class_id'] = $q['c'];
		}
		// Unit
		if (isset($q['u']) && !empty($q['u']) && $q['u'] != '') {
			$params['unit_id'] = $q['u'];
		}
		// Period
		if (isset($q['p']) && !empty($q['p']) && $q['p'] != '') {
			$params['period_id'] = $q['p'];
		}

		$params['status'] 	= 1;
		$data['student'] 	= $this->Student_model->get($params);		 
		$data['month'] 		= $this->Bulan_model->get_month();
		$data['class'] 		= $this->Class_model->get();
		$data['unit'] 		= $this->Unit_model->get();
		$data['period'] 	= $this->Period_model->get();
		$data['title'] 		= 'Tagihan Santri';
		$data['main'] 		= 'report/tagihan_santri';
		$this->load->view('manage/layout', $data);
	}

	public function tagihan_santri_xls(){
		// Apply Filter
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

		// Unit
		if (isset($q['u']) && !empty($q['u']) && $q['u'] != '') {
			$params['unit_id'] = $q['u'];
		}
		// Period
		if (isset($q['p']) && !empty($q['p']) && $q['p'] != '') {
			$params['period_id'] = $q['p'];
		}
		// Class
		if (isset($q['c']) && !empty($q['c']) && $q['c'] != '') {
			$params['class_id'] = $q['c'];
		}
		// Student
		if (isset($q['s']) && !empty($q['s']) && $q['s'] != '') {
			$params['student_id'] = $q['s'];
		}

		$data['student'] = $this->Student_model->get($params);
		$data['setting_school'] = $this->Setting_model->get(array('id' => SCHOOL_NAME));
		$period = $this->Period_model->get(array('period_id'=>$q['p']));
		$unit = $this->Unit_model->get(array('unit_id'=>$q['u']));
		$class = $this->Class_model->get(array('class_id'=>$q['c']));

		$this->load->library("PHPExcel");
		$objXLS   = new PHPExcel();
		$objSheet = $objXLS->setActiveSheetIndex(0);            
		$cell     = 8;        
		$no       = 1;

		$objSheet->setCellValue('A1', 'Tagihan Siswa');
		$objSheet->setCellValue('A2', $data['setting_school']['setting_value'] );
		$objSheet->setCellValue('A3', 'Tahun Ajaran: '.$period['period_start'].'/'.$period['period_end']);
		if(isset($unit['name'])){
			$objSheet->setCellValue('A4', 'Unit: '.$unit['name']);
		}else{
			$objSheet->setCellValue('A4', 'Unit: Semua Unit');
		}
		if(isset($class['class_name'])){
			$objSheet->setCellValue('A5', 'Kelas: '.$class['class_name']);
		}else{
			$objSheet->setCellValue('A5', 'Kelas: Semua Kelas');
		}

		$objSheet->setCellValue('A6', 'Tanggal Unduh: '.pretty_date(date('Y-m-d h:i:s'),'d F Y, H:i',false));
		$objSheet->setCellValue('C6', 'Pengunduh: '.$this->session->userdata('ufullname'));	
		$objSheet->setCellValue('A7', 'NO');
		$objSheet->setCellValue('B7', 'NIS');
		$objSheet->setCellValue('C7', 'NAMA');
		$objSheet->setCellValue('D7', 'KELAS');
		$objSheet->setCellValue('E7', 'TOTAL TAGIHAN');

		$total_bill_kelas = 0;
		foreach($data['student'] as $row){
			$total_bill = 0;
			// bulanan
			foreach($this->Bulan_model->get(array('student_id'=>$row['student_id'], 'period_id'=>$q['p'], 'status'=>0)) as $bill){
				$total_bill += $bill['bulan_bill'];
			}
			// angsuran
			foreach($this->Bebas_model->get(array('student_id'=>$row['student_id'], 'period_id'=>$q['p'])) as $bill){
				$total_bill += $bill['bebas_bill']-$bill['bebas_total_pay'];
			}
			$total_bill_kelas += $total_bill;
			
			$objSheet->setCellValue('A'.$cell, $no)->getStyle('A'.$cell);
			$objSheet->setCellValueExplicit('B'.$cell, $row['student_nis'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objSheet->setCellValueExplicit('C'.$cell, $row['student_full_name'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objSheet->setCellValueExplicit('D'.$cell, $row['class_name'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objSheet->setCellValueExplicit('E'.$cell, 'Rp. ' .number_format($total_bill));			
			$cell++;
			$no++;    
		}

		$objXLS->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$objXLS->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objXLS->getActiveSheet()->getColumnDimension('C')->setWidth(40);

		foreach(range('D', 'Z') as $alphabet){
			$objXLS->getActiveSheet()->getColumnDimension($alphabet)->setWidth(20);
		}

		$objXLS->getActiveSheet()->getColumnDimension('N')->setWidth(20);
		$font = array('font' => array( 'bold' => true, 'color' => array('rgb'  => 'FFFFFF')));
		$objXLS->getActiveSheet()->getStyle('A7:E7')->applyFromArray($font);

		$objXLS->getActiveSheet()
		->getStyle('A7:E7')
		->getFill()
		->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		->getStartColor()
		->setRGB('000');
		$objXLS->getActiveSheet()->getStyle('A1')->getFont()->setBold( true );
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5'); 
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="LAPORAN_TAGIHAN_SISWA_'.date('dmY').'.xls"'); 
		header('Cache-Control: max-age=0'); 
		$objWriter->save('php://output'); 
		exit();
	}
	
	public function search_report_jurnal(){
		$q = $this->input->post(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

        // Date start
		if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
			$params['date_start'] = $q['ds'];
			$date_start = $q['ds'];
		}
        // Date end
		if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
			$params['date_end'] = $q['de'];
			$date_end = $q['de'];
		}
		
		// print_r($params);
		// die;
		$data['bulan'] = $this->Bulan_model->get($params);
		$data['bebas'] = $this->Bebas_model->get($params);
		$data['free'] = $this->Bebas_pay_model->get($params);
		$data['kredit'] = $this->Kredit_model->get($params);
		$data['debit'] = $this->Debit_model->get($params);
		$data['setting_school'] = $this->Setting_model->get(array('id' => SCHOOL_NAME));
		$saldo_awal_db = 0;
		$saldo_awal_cr = 0;
		
		$result = '<div class="box box-primary box-solid">';
		$result .= '<div class="box-header with-border">';
		$result .= '<h3 class="box-title"><span class="fa fa-file-text-o"></span> Laporan per Tanggal '.$q['ds'].' Sampai '.$q['de'].'</h3>
		</div>
		<div class="box-body table-responsive">
			<div id="xtable_wrapper" class="dataTables_wrapper no-footer">
			<table class="table table-responsive table-hover table-bordered dataTable no-footer" id="xtable" style="white-space: nowrap; width: 1203px;" role="grid" aria-describedby="xtable_info">
				<thead>
				<tr role="row">
					<th>Akun</th>
					<th>Tanggal</th>
					<th>Kode Akun</th>
					<th>Keterangan</th>
					<th>NIS</th>
					<th>Nama Siswa</th>
					<th>Kelas</th>
					<th>Penerimaan</th>
					<th>Pengeluaran</th>
				</tr>
				</thead>
				<tbody>';
				$subtotal_debit = 0;
				foreach($data['debit'] as $row){
					$subtotal_debit += $row['debit_value'];
					if($row['student_id'] != NULL){
						$student_nis 		= $this->Student_model->get(array('id'=>$row['student_id']))['student_nis'];
						$student_fullname 	= $this->Student_model->get(array('id'=>$row['student_id']))['student_full_name']; 
						$class_name 		= $this->Student_model->get(array('id'=>$row['student_id']))['class_name'];	
					}else{
						$student_nis 		= '-';
						$student_fullname	= '-';
						$class_name			= '-';
					}
					
				$result .= '<tr role="row" class="odd">
					<td>'.$this->Account_model->get(array('account_id'=>$row['account_id']))['name'].'</td>
					<td>'.$row['debit_date'].'</td>
					<td>'.$this->Account_model->get(array('account_id'=>$row['account_id']))['code'].'</td>
					<td>'.$row['debit_desc'].'</td>
					<td>'.$student_nis.'</td>
					<td>'.$student_fullname.'</td>
					<td>'.$class_name.'</td>
					<td>'.number_format($row['debit_value']).'</td>
					<td> - </td>
				</tr>';
				}
				$subtotal_kredit = 0;
				foreach($data['kredit'] as $row){
				$subtotal_kredit += $row['kredit_value'];
				if($row['student_id'] != NULL){
					$student_nis 		= $this->Student_model->get(array('id'=>$row['student_id']))['student_nis'];
					$student_fullname 	= $this->Student_model->get(array('id'=>$row['student_id']))['student_full_name']; 
					$class_name 		= $this->Student_model->get(array('id'=>$row['student_id']))['class_name'];	
				}else{
					$student_nis 		= '-';
					$student_fullname	= '-';
					$class_name			= '-';
				}
				
				$result .= '<tr role="row" class="odd">
					<td>'.$this->Account_model->get(array('account_id'=>$row['account_id']))['name'].'</td>
					<td>'.$row['kredit_date'].'</td>
					<td>'.$this->Account_model->get(array('account_id'=>$row['account_id']))['code'].'</td>
					<td>'.$row['kredit_desc'].'</td>
					<td>'.$student_nis.'</td>
					<td>'.$student_fullname.'</td>
					<td>'.$class_name.'</td>
					<td> - </td>
					<td>'.number_format($row['kredit_value']).'</td>
				</tr>';
				}
				
				$total = $subtotal_debit + $saldo_awal_db;
				$saldo_akhir = $total - $subtotal_kredit;
				$result .= '</tbody>
				<tbody>
					<tr style="background-color: #E2F7FF;">
						<td colspan="7" align="right"><strong>Sub Total</strong></td>
						<td>'.number_format($subtotal_debit).'</td>
						<td>'.number_format($subtotal_kredit).'</td>
					</tr>
					<tr style="background-color: #F0B2B2;">
						<td colspan="7" align="right"><strong>Saldo Awal</strong></td>
						<td>'.$saldo_awal_db.'</td>
						<td> - </td>
					</tr>
					<tr style="background-color: #FFFCBE;">
						<td colspan="7" align="right"><strong>Total (Sub Total + Saldo Awal)</strong></td>
						<td>'.number_format($total).'</td>
						<td>'.number_format($subtotal_kredit).'</td>
					</tr>
						<tr style="background-color: #c2d2f6;">
						<td colspan="7" align="right"><strong>Saldo Akhir</strong></td>
						<td>'.number_format($saldo_akhir).'</td>
						<td> - </td>
					</tr>
				</tbody>
			</table>
		</div>';

		$result .= '<div class="box-footer">';
		$result .=	'<table class="table">
				<tbody>';
					$result.='<tr>';
						$hide='<td>
							<div class="md-6">
								<a class="btn btn-danger" target="_blank" href="'.site_url("manage/report/cetak_detail_jurnal/2022-10-24/2022-10-29/all").'"><span class="fa fa-file-pdf-o"></span> PDF Per Jenis Anggaran
								</a>
								<a class="btn btn-success" target="_blank" href="'.site_url("manage/report/excel_detail_jurnal/2022-10-24/2022-10-29/all").'"><span class="fa fa-file-excel-o"></span> Excel Per Jenis Anggaran
								</a>
							</div>
						</td>';
						$result.='<td>
							<div class="pull-right">';
								$hide='<a class="btn btn-danger" target="_blank" href="'.site_url("manage/report/report_pdf?ds=$date_start&de=$date_end").'"><span class="fa fa-file-pdf-o"></span> PDF Rekap Laporan
								</a>';
								$result.='<a class="btn btn-success" target="_blank" href="'.base_url("manage/report/report?ds=$date_start&de=$date_end").'"><span class="fa fa-file-excel-o"></span> Excel Rekap Laporan
								</a>
							</div>
						</td>
					</tr>';
				$result .= '</tbody>
			</table>';
		$result .= '</div>';
		
	$result .= '</div>';

	echo $result;

	}

	
	
	public function report_bill() {
		// $this->output->enable_profiler(TRUE);
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();
		$param = array();
		$stu = array();
		$free = array();

		if (isset($q['p']) && !empty($q['p']) && $q['p'] != '') {
			$params['period_id'] = $q['p'];
			$param['period_id'] = $q['p'];
			$stu['period_id'] = $q['p'];
			$free['period_id'] = $q['p'];
		}

		if (isset($q['c']) && !empty($q['c']) && $q['c'] != '') {
			$params['class_id'] = $q['c'];
			$param['class_id'] = $q['c'];
			$stu['class_id'] = $q['c'];
			$free['class_id'] = $q['c'];
		}

		if (isset($q['k']) && !empty($q['k']) && $q['k'] != '') {
			$params['majors_id'] = $q['k'];
			$param['majors_id'] = $q['k'];
			$stu['majors_id'] = $q['k'];
			$free['majors_id'] = $q['k'];
		}

		$param['paymentt'] = TRUE;
		$params['grup'] = TRUE;
		$stu['group'] = TRUE;

		$data['period'] = $this->Period_model->get();
		$data['class'] = $this->Student_model->get_class($params);
		$data['majors'] = $this->Student_model->get_majors($params);
		$data['student'] = $this->Bulan_model->get($stu);
		$data['bulan'] = $this->Bulan_model->get($free);
		$data['month'] = $this->Bulan_model->get($params);
		// $data['month'] = $this->Bulan_model->get_month();
		$data['py'] = $this->Bulan_model->get($param);
		$data['bebas'] = $this->Bebas_model->get($params);
		$data['free'] = $this->Bebas_model->get($free);

		$config['suffix'] = '?' . http_build_query($_GET, '', "&");

		$data['title'] = 'Rekapitulasi';
		$data['main'] = 'report/report_bill_list';
		$this->load->view('manage/layout', $data);
	}


	public function report(){
        // Apply Filter
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

        // Date start
		if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
			$params['date_start'] = $q['ds'];
		}
        // Date end
		if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
			$params['date_end'] = $q['de'];
		}
		$params['status'] = 1;

		$data['bulan'] = $this->Bulan_model->get($params);
		$data['bebas'] = $this->Bebas_model->get($params);
		$data['free'] = $this->Bebas_pay_model->get($params);
		$data['kredit'] = $this->Kredit_model->get($params);
		$data['debit'] = $this->Debit_model->get($params);
		$data['setting_school'] = $this->Setting_model->get(array('id' => SCHOOL_NAME));

		$this->load->library("PHPExcel");
		$objXLS   = new PHPExcel();
		$objSheet = $objXLS->setActiveSheetIndex(0);            
		$cell     = 6;        
		$no       = 1;

		$objSheet->setCellValue('A1', 'Laporan Keuangan');
		$objSheet->setCellValue('A2', $data['setting_school']['setting_value'] );
		$objSheet->setCellValue('A3', 'Tanggal Laporan: '.pretty_date($q['ds'],'d F Y',false).' s/d '.pretty_date($q['de'],'d F Y',false));
		$objSheet->setCellValue('A4', 'Tanggal Unduh: '.pretty_date(date('Y-m-d h:i:s'),'d F Y, H:i',false));
		$objSheet->setCellValue('C4', 'Pengunduh: '.$this->session->userdata('ufullname'));
		

		$objSheet->setCellValue('A5', 'NO');
		$objSheet->setCellValue('B5', 'PEMBAYARAN');
		$objSheet->setCellValue('C5', 'NAMA SISWA');
		$objSheet->setCellValue('D5', 'KELAS');
		$objSheet->setCellValue('E5', 'TANGGAL');
		$objSheet->setCellValue('F5', 'PENERIMAAN');
		$objSheet->setCellValue('G5', 'PENGELUARAN');     
		$objSheet->setCellValue('H5', 'KETERANGAN');     


		foreach ($data['bulan'] as $row) {

			$objSheet->setCellValue('A'.$cell, $no);
			$objSheet->setCellValueExplicit('B'.$cell, $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'].'-'.$row['month_name'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objSheet->setCellValueExplicit('C'.$cell, $row['student_full_name'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objSheet->setCellValueExplicit('D'.$cell, $row['class_name'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objSheet->setCellValue('E'.$cell, pretty_date($row['bulan_date_pay'], 'm/d/Y', FALSE));
			$objSheet->setCellValue('F'.$cell, $row['bulan_bill']);
			$objSheet->setCellValue('G'.$cell, ' ');
			$cell++;
			$no++;    
		}

		foreach ($data['free'] as $row) {

			$objSheet->setCellValue('A'.$cell, $no);
			$objSheet->setCellValueExplicit('B'.$cell, $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objSheet->setCellValueExplicit('C'.$cell, $row['student_full_name'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objSheet->setCellValueExplicit('D'.$cell, $row['class_name'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objSheet->setCellValue('E'.$cell, pretty_date($row['bebas_pay_input_date'], 'm/d/Y', FALSE));
			$objSheet->setCellValue('F'.$cell, $row['bebas_pay_bill']);
			$objSheet->setCellValue('G'.$cell, ' ');
			$cell++;
			$no++;    
		}

		foreach ($data['kredit'] as $row) {

			$objSheet->setCellValue('A'.$cell, $no);
			$objSheet->setCellValueExplicit('B'.$cell, $row['kredit_desc'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objSheet->setCellValue('C'.$cell, '-');
			$objSheet->setCellValue('D'.$cell, '-');
			$objSheet->setCellValue('E'.$cell, pretty_date($row['kredit_date'], 'm/d/Y', FALSE));
			$objSheet->setCellValue('F'.$cell, '');
			$objSheet->setCellValue('G'.$cell, $row['kredit_value']);
			$cell++;
			$no++;    
		}

		foreach ($data['debit'] as $row) {

			$objSheet->setCellValue('A'.$cell, $no);
			$objSheet->setCellValueExplicit('B'.$cell, $row['debit_desc'],PHPExcel_Cell_DataType::TYPE_STRING);
			$objSheet->setCellValue('C'.$cell, '-');
			$objSheet->setCellValue('D'.$cell, '-');
			$objSheet->setCellValue('E'.$cell, pretty_date($row['debit_date'], 'm/d/Y', FALSE));
			$objSheet->setCellValue('F'.$cell, $row['debit_value']);
			$objSheet->setCellValue('G'.$cell, '');
			$cell++;
			$no++;    
		}                      

		$objXLS->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$objXLS->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$objXLS->getActiveSheet()->getColumnDimension('C')->setWidth(20);

		foreach(range('D', 'Z') as $alphabet)
		{
			$objXLS->getActiveSheet()->getColumnDimension($alphabet)->setWidth(20);
		}

		$objXLS->getActiveSheet()->getColumnDimension('N')->setWidth(20);

		$font = array('font' => array( 'bold' => true, 'color' => array(
			'rgb'  => 'FFFFFF')));
		$objXLS->getActiveSheet()
		->getStyle('A5:H5')
		->applyFromArray($font);

		$objXLS->getActiveSheet()
		->getStyle('A5:H5')
		->getFill()
		->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		->getStartColor()
		->setRGB('000');
		$objXLS->getActiveSheet()->getStyle('A1')->getFont()->setBold( true );
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5'); 
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="LAPORAN_KEUANGAN_'.date('dmY').'.xls"'); 
		header('Cache-Control: max-age=0'); 
		$objWriter->save('php://output'); 
		exit();      
	}

	public function rekap_pembayaran() {
		// $this->output->enable_profiler(TRUE);
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();

		if (isset($q['p']) && !empty($q['p']) && $q['p'] != '') {
			$params['period_id'] = $q['p'];
		}
		if (isset($q['c']) && !empty($q['c']) && $q['c'] != '' && $q['c'] != 'all') {
			$params['class_id'] = $q['c'];
		}

		$param['paymentt'] 	= TRUE;
		$params['grup'] 	= TRUE;

		$data['period'] = $this->Period_model->get();
		$data['class'] 	= $this->Student_model->get_class($params);
		$data['month'] 	= $this->Bulan_model->get($params);
		$data['py'] 	= $this->Bulan_model->get($params);
		
		$data['title'] = 'Rekap Pembayaran';
		$data['main'] = 'report/rekap_pembayaran';
		$this->load->view('manage/layout', $data);
	}

	// Laporan Rekap Pembayaran
	public function report_bill_xls(){
		$q = $this->input->get(NULL, TRUE);
		$data['q'] = $q;
		$params = array();
		$param = array();
		$stu = array();
		$free = array();

		if (isset($q['p']) && !empty($q['p']) && $q['p'] != '') {
			$params['period_id'] = $q['p'];
			$param['period_id'] = $q['p'];
			$stu['period_id'] = $q['p'];
			$free['period_id'] = $q['p'];
		}
		if (isset($q['c']) && !empty($q['c']) && $q['c'] != '') {
			$params['class_id'] = $q['c'];
			$param['class_id'] = $q['c'];
			$stu['class_id'] = $q['c'];
			$free['class_id'] = $q['c'];
		}
		
		$param['paymentt'] = TRUE;
		$params['grup'] = TRUE;
		$stu['group'] = TRUE;
		
		$data['period'] = $this->Period_model->get($params);
		$data['class'] = $this->Student_model->get_class($stu);
		$data['student'] = $this->Bulan_model->get($stu);
		$data['bulan'] = $this->Bulan_model->get($free);
		$data['month'] = $this->Bulan_model->get($params);
		$data['py'] = $this->Bulan_model->get($param);
		$data['bebas'] = $this->Bebas_model->get($params);
		$data['free'] = $this->Bebas_model->get($free);
		$data['setting_school'] = $this->Setting_model->get(array('id' => SCHOOL_NAME));
		$this->load->library("PHPExcel");
		$objXLS   = new PHPExcel();
		$objSheet = $objXLS->setActiveSheetIndex(0);            
		$cell     = 7;        
		$no       = 1;
		$font = array('font' => array( 'bold' => true, 'color' => array('rgb'  => 'FFFFFF')));

		$objXLS->setActiveSheetIndex(0);        
		$styleArray = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array(
							'rgb'  => 'FFFFFF' 
						),
					),
				),
			),
		);

		$borderStyle = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array(
						'rgb'  => '111111' 
					),
				),
			),
		);
		
		$objSheet->setCellValue('A1', 'REKAPITULASI PEMBAYARAN SISWA');
		$objSheet->setCellValue('A2', $data['setting_school']['setting_value'] );

		$year = $data['period']['period_start'].'/'.$data['period']['period_end'];
		$periode = ($q['p']==$data['period']['period_id']) ? $year : '';
		$objSheet->setCellValue('A3', 'Periode Laporan: '. $periode);
		
		$objSheet->setCellValue('A4', 'Tanggal Unduh: '.pretty_date(date('Y-m-d h:i:s'),'d F Y, H:i',false));
		$objSheet->setCellValue('C4', 'Pengunduh: '.$this->session->userdata('ufullname'));
		
		$objSheet->setCellValue('A5', 'BULANAN');
		$objSheet->mergeCells('A5:P5');		
		$objSheet->setCellValue('A6', 'NO');
		$objSheet->setCellValue('B6', 'KELAS');
		$objSheet->setCellValue('C6', 'NAMA SISWA');
		$objSheet->setCellValue('D6', 'JENIS PEMBAYARAN');
		$objXLS->getActiveSheet()->getStyle('A5:D6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('000');
		$objXLS->getActiveSheet()->getStyle('A5:D6')->applyFromArray($font);
		$objSheet->getStyle('A5:D6')->applyFromArray($styleArray);
		$objSheet->getStyle('A6:C6')->applyFromArray($styleArray);
		
		// Judul Pembayaran Bulanan
		$col_m=0;
		$start_alpha_month=5;
		foreach ($data['month'] as $row) {
			$objSheet->setCellValue(getCell($start_alpha_month+$col_m).'6', $row['month_name']); 
			$objSheet->mergeCells(getCell($start_alpha_month+$col_m).'6:'.getCell($start_alpha_month+$col_m).'6');
			$objXLS->getActiveSheet()->getStyle(getCell($start_alpha_month+$col_m).'6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('000');
			$objXLS->getActiveSheet()->getStyle(getCell($start_alpha_month+$col_m).'6')->applyFromArray($font);
			$objSheet->getStyle('D6:'.getCell($start_alpha_month+$col_m).'6')->applyFromArray($styleArray);
			$col_m++;
		}

		// $class_id 	= $q['c']; 
		// $period_id 	= $q['p'];
		$class_id = $q['c']; 
			if($q['c'] == 'all'){
				$param_class = ''; 
				$hclass = '';
			}else{
				$param_class =	'AND student.class_class_id = '.$class_id;	
				$hclass = '&c='.$class_id;							
			}
		$period_id = $q['p'];
		
		$alpha_monthbill=4;
		foreach ($this->db->query("SELECT DISTINCT student.*, bulan.`payment_payment_id`, 
									bulan.`student_student_id`, `payment`.`period_period_id`, `pos`.`pos_name` FROM student 
									LEFT JOIN bulan ON bulan.`student_student_id` = student.`student_id`
									LEFT JOIN `payment` ON `payment`.`payment_id` = `bulan`.`payment_payment_id`
									LEFT JOIN `pos` ON `pos`.`pos_id` = `payment`.`pos_pos_id`
									WHERE bulan.`payment_payment_id` <> '' $param_class
									AND `payment`.`period_period_id` = $period_id")->result_array() as $row)  {

			$period = $this->Period_model->get(array('period_id'=>$row['period_period_id']));	
			$objSheet->setCellValue('A'.$cell, $no);	
			$objSheet->setCellValue('B'.$cell, $this->Class_model->get(array('class_id'=>$row['class_class_id']))['class_name']);	
			$objSheet->setCellValue('C'.$cell, $row['student_full_name']);	
			$objSheet->setCellValue('D'.$cell, $row['pos_name'].' - T.A '.$period['period_start'].'/'.$period['period_end']);	
			
			$objSheet->mergeCells('A'.intval($cell).':A'.intval($cell+2));
			$objSheet->mergeCells('B'.intval($cell).':B'.intval($cell+2));
			$objSheet->mergeCells('C'.intval($cell).':C'.intval($cell+2));
			$objSheet->mergeCells('D'.intval($cell).':D'.intval($cell+2));
			
			$styleCenter = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				)
			);
		
			$objSheet->getStyle('A'.intval($cell).':A'.intval($cell+2))->applyFromArray($styleCenter);
			$objSheet->getStyle('B'.intval($cell).':B'.intval($cell+2))->applyFromArray($styleCenter);
			$objSheet->getStyle('C'.intval($cell).':C'.intval($cell+2))->applyFromArray($styleCenter);
			$objSheet->getStyle('D'.intval($cell).':D'.intval($cell+2))->applyFromArray($styleCenter);			

			$ttl_bln=1;
			foreach($this->Bulan_model->get_month() as $m){
					$data_bulan = $this->Bulan_model->get(array('student_id'=>$row['student_student_id'],
													'payment_id'=>$row['payment_payment_id'],
													'month_id'=>$m['month_id']));

				if(isset($data_bulan) OR !empty($data_bulan)) {
					if($data_bulan[0]['bulan_status']==1){
						$status_bill = 'Lunas';
						$fontStatus = array('font' => array( 'bold' => true, 'color' => array('rgb'  => '00a65a')),
											'alignment' => array(
													'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
													'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
													)
											);
					}else{
						$status_bill = number_format($data_bulan[0]['bulan_bill']);
						$fontStatus = array('font' => array( 'bold' => true, 'color' => array('rgb'  => 'ff0000')),
											'alignment' => array(
													'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
													'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
													)
											);
					}
				}
				$objSheet->setCellValue(getCell($alpha_monthbill+$ttl_bln).$cell, $status_bill)
				->getStyle(getCell($alpha_monthbill+$ttl_bln).$cell)->applyFromArray($fontStatus);
				
				$objSheet->setCellValue(getCell($alpha_monthbill+$ttl_bln).($cell+1), isset($data_bulan[0]['bulan_date_pay']) ? '('.pretty_date($data_bulan[0]['bulan_date_pay'], 'd/m/y', false).')' : '' )
				->getStyle(getCell($alpha_monthbill+$ttl_bln).($cell+1))->applyFromArray($styleCenter);
				$objSheet->setCellValue(getCell($alpha_monthbill+$ttl_bln).($cell+2), $data_bulan[0]['user_full_name'])
				->getStyle(getCell($alpha_monthbill+$ttl_bln).($cell+2))->applyFromArray($styleCenter);
				$ttl_bln++;
			}

			// $cell++;
			$cell+=3;
			$no++;				
		}

		// echo $cell+3;
		// die;
		
		/**Table Bebas */
		$cell+=1;
		$objSheet->setCellValue('A'.$cell, 'ANGSURAN');
		$objSheet->mergeCells('A'.intval($cell).':E'.intval($cell));		
		$objSheet->setCellValue('A'.intval($cell+1), 'NO');
		$objSheet->setCellValue('B'.intval($cell+1), 'KELAS');
		$objSheet->setCellValue('C'.intval($cell+1), 'NAMA SISWA');
		$objSheet->setCellValue('D'.intval($cell+1), 'JENIS PEMBAYARAN');
		$objSheet->setCellValue('E'.intval($cell+1), 'TOTAL TAGIHAN');
		$objXLS->getActiveSheet()->getStyle('A'.$cell.':E'.intval($cell+1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('000');
		$objXLS->getActiveSheet()->getStyle('A'.$cell.':E'.intval($cell+1))->applyFromArray($font);
		$objSheet->getStyle('A'.$cell.':E'.intval($cell+1))->applyFromArray($styleArray);
		$objSheet->getStyle('A'.$cell.':E'.intval($cell+1))->applyFromArray($styleArray);
		$no=1;
		$cell+=2;
		
		foreach ($this->db->query("SELECT DISTINCT student.*, bebas.`payment_payment_id`, 
									bebas.`student_student_id`, `payment`.`period_period_id`, `pos`.`pos_name` FROM student 
									LEFT JOIN bebas ON bebas.`student_student_id` = student.`student_id`
									LEFT JOIN `payment` ON `payment`.`payment_id` = `bebas`.`payment_payment_id`
									LEFT JOIN `pos` ON `pos`.`pos_id` = `payment`.`pos_pos_id`
									WHERE bebas.`payment_payment_id` <> '' $param_class
									AND `payment`.`period_period_id` = $period_id")->result_array() as $row){

			$period = $this->Period_model->get(array('period_id'=>$row['period_period_id']));
			$objSheet->setCellValue('A'.$cell, $no);	
			$objSheet->setCellValue('B'.$cell, $this->Class_model->get(array('class_id'=>$row['class_class_id']))['class_name']);	
			$objSheet->setCellValue('C'.$cell, $row['student_full_name']);	
			$objSheet->setCellValue('D'.$cell, $row['pos_name'].' - T.A '.$period['period_start'].'/'.$period['period_end']);	

			$bebas = $this->Bebas_model->get(array('student_id'=>$row['student_student_id'],
																				  'payment_id'=>$row['payment_payment_id']
																				));
				if(isset($bebas) OR !empty($bebas)) {
					if($bebas[0]['bebas_total_pay']==0){
						$bebas_bill = 'Lunas';
					}else{
						$bebas_bill = number_format($bebas[0]['bebas_bill']);
					}
				}
				$objSheet->setCellValue('E'.$cell, $bebas_bill);
				$cell++;

		}
		/**end table bebas */
		
		$objXLS->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$objXLS->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$objXLS->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objXLS->getActiveSheet()->getColumnDimension('D')->setWidth(40);

		foreach(range('E', 'Z') as $alphabet){
			$objXLS->getActiveSheet()->getColumnDimension($alphabet)->setWidth(15);
		}

		foreach ($data['class'] as $row) {
			if ($q['c']==$row['class_id']){
				$kelas = $row['class_name'];
			} else {
				$kelas = 'PEMBAYARAN_SISWA';
			}
		}

		$objXLS->getActiveSheet()->getColumnDimension('N')->setWidth(20);
		$objXLS->getActiveSheet()->getStyle('A1')->getFont()->setBold( true );
		$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5'); 
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="REKAPITULASI_'.$kelas.'_'.date('dmY').'.xls"'); 
		header('Cache-Control: max-age=0'); 
		$objWriter->save('php://output'); 
		exit();      
	}

}

/* End of file Report_set.php */
/* Location: ./application/modules/report/controllers/Report_set.php */