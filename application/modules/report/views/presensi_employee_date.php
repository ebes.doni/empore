<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>
	<section class="content">
		<div class="row" >
			<div class="col-md-12">
				<div class="card card-success">
					<div class="card-header">
						<?= form_open(current_url(), array('method' => 'get')) ?> <br>
							<div class="row mb-3">

								<div class="col-md-3 mb-2">  
									<div class="form-group">
										<!-- <div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
											<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
											<input class="form-control" type="text" name="ds" id="ds" required="" readonly 
											<?= (isset($q['ds'])) ? 'value="'.$q['ds'].'"' : '' ?> placeholder="Tanggal Awal">
										</div> -->
										<div id="dsFlatpickr" class="js-flatpickr flatpickr-custom input-group"
											data-hs-flatpickr-options='{
												"appendTo": "#dsFlatpickr",
												"dateFormat": "Y-m-d",
												"wrap": true
											}'>
											<div class="input-group-prepend input-group-text" data-bs-toggle>
												<i class="bi-calendar-week"></i>
											</div>
											<input class="flatpickr-custom-form-control form-control" 
													type="text" 
													name="ds" 
													id="ds" 
													placeholder="Tanggal Awal" 
													data-input <?= (isset($q['ds'])) ? 'value="'.$q['ds'].'"' : '' ?> 
													required>
										</div>
									</div>
								</div>

								<div class="col-md-3 mb-2">  
									<div class="form-group">
										<!-- <div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
											<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
											<input class="form-control" type="text" name="de" id="de" required="" readonly 
											<?= (isset($q['de'])) ? 'value="'.$q['de'].'"' : '' ?> placeholder="Tanggal Akhir">
										</div> -->
										<div id="deFlatpickr" class="js-flatpickr flatpickr-custom input-group"
											data-hs-flatpickr-options='{
												"appendTo": "#deFlatpickr",
												"dateFormat": "Y-m-d",
												"wrap": true
											}'>
											<div class="input-group-prepend input-group-text" data-bs-toggle>
												<i class="bi-calendar-week"></i>
											</div>
											<input class="flatpickr-custom-form-control form-control" 
													type="text" 
													name="de" 
													id="de" 
													placeholder="Tanggal Akhir" 
													data-input <?= (isset($q['de'])) ? 'value="'.$q['de'].'"' : '' ?> 
													required>
										</div>
									</div>
								</div>

								<div class="col-md-3 mb-2">  
									<div class="form-group">
										<select required="" name="ei" id="ei" class="form-control form-select">
											<option value="">-- Pilih Employee --</option>
											<?php foreach($employee as $row):?>
												<option value="<?= $row['id'] ?>" 
													<?php if(isset($q['ei'])) if($q['ei']==$row['id']) echo 'selected' ?>>
													<?= $row['name'] ?>
												</option>
											<?php endforeach ?>
										</select>
									</div>
								</div>

								<div class="col-md-1 mb-2">
									<button type="submit" class="btn btn-primary">Filter</button>
								</div>
								
								<?php if ($q) { ?>
								<div class="col-md-2 mb-2">
									<a class="btn btn-success" 
										href="<?= site_url('manage/report/presensi_employee_xls' . '/?' . http_build_query($q)) ?>">
										<i class="fa fa-file-excel-o" ></i> Export Excel
									</a>
								</div>								
								<?php } ?>

							</div>
						<?= form_close() ?>		
						
			<?php if($q):?>
			<!-- view report -->
			<div class="card card-info">
				<div class="card-body table-responsive">
					<table class="table table-responsive table-hover table-bordered" style="white-space: nowrap;">
						<thead class="bg-soft-dark">						
						<tr>
							<th>Tanggal</th> 
							<th>NIP</th> 
							<th>Nama Guru</th>
							<?php for($i=1; $i<=$jam['jam']; $i++):?>
								<th>Jam <?= $i ?></th>
							<?php endfor ?>
							<th>Total Jam</th>								
						</tr>
						</thead>
						<?php
							$begin 		= new DateTime($q['ds']);
							$end 		= new DateTime($q['de']);
							$interval 	= DateInterval::createFromDateString('1 day');
							$period 	= new DatePeriod($begin, $interval, $end->modify('+1 day'));
							
							foreach ($period as $dt):
								$presence_date =  $dt->format("Y-m-d"); 
								$employee_id = $q['ei'];
								$total_jam=0; 
									foreach($this->Presensi_model->get_presence(array('presence_date'=>$presence_date, 
																			'employee_id'=>$employee_id,
																			'status'=>'approved'
																			)) as $p):
									$total_jam += $p['durasi'];
									endforeach;						

						?>
						<tbody>
							<tr>
								<td><?= $presence_date ?></td> 
								<td><?= $this->Employee_model->get(array('employee_id'=>$q['ei']))['nip'] ?></td> 
								<td><?= $this->Employee_model->get(array('employee_id'=>$q['ei']))['name'] ?></td>
								<?php 										
								for($i=1; $i<=$jam['jam']; $i++):
								?>  
									<td>
										<?php if($this->Presensi_model
												->get(array('presence_date'=>$presence_date,
															'employee_id'=>$employee_id,
															'jam'=>$i, 
															'status'=>'approved'))['class_name'] !== NULL){
																echo $this->Presensi_model->get(array('presence_date'=>$presence_date,
																'employee_id'=>$employee_id,
																'jam'=>$i, 
																'status'=>'approved'))['class_name'];
															}
																				 
										?>
									</td>
								<?php endfor 
								?>
								<td><?= $total_jam ?></td>
							</tr>
						</tbody>
						<?php endforeach ?>

					</table>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- end view report -->
			<?php endif ?>

					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
