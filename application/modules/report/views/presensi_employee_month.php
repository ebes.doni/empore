<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-success">
					<div class="card-header">
						<?= form_open(current_url(), array('method' => 'get')) ?> <br>
						<div class="row mb-3">

							<!-- for period month -->
							<div class="form-group col-md-3">
								<div class="input-group">
									<div class="input-group-prepend input-group-text" data-bs-toggle>
										<i class="bi-calendar-week"></i>
									</div>
									<input type="text"
											class="form-control monthyears"
											name="my" id="my" 
											readonly="readonly" data-date-format="yyyy-mm"
											<?= (isset($q['my'])) ? 'value="'.$q['my'].'"' : '' ?> 
											placeholder="Pilih Bulan">
								</div>
								<!-- <div class="input-group">
								<span class="input-group-addon"><i class="bi-calendar-week"></i></span>
									<input type="text"
											class="form-control monthyears"
											name="my" id="my" 
											readonly="readonly" data-date-format="yyyy-mm"
											<?= (isset($q['my'])) ? 'value="'.$q['my'].'"' : '' ?> 
											placeholder="Select Month">
								</div> -->
							</div> 
							<!---->	
							<div class="col-md-1">
								<button type="submit" class="btn btn-primary">Filter</button>
							</div>
							
							<?php if ($q) { ?>
								<div class="col-md-2 mb-2">
									<a class="btn btn-success" 
										href="<?= site_url('manage/report/presensi_employee_month_xls' . '/?' . http_build_query($q)) ?>">
										<i class="fa fa-file-excel-o" ></i> Export Excel
									</a>
								</div>
							<?php } ?>

						</div>
						<?= form_close() ?>		

			<?php if(isset($q['my'])):?>
			<!-- view report -->
			<div class="card card-info">
			<br>
			<h4 style="text-align:center">Daftar Penghitungan Honor Mengajar</h4>
			<h4 style="text-align:center">Bulan <?= $bulan ?></h4>
			<h4 style="text-align:center">Tahun <?= $tahun ?></h4>
				<div class="card-body table-responsive">
				<table class="table table-responsive table-hover table-bordered" style="white-space: nowrap;">
						<tr class="info">
							<th></th> 
							<th></th>
							<th></th>
							<th style="text-align:center" colspan="<?= $days_in_month+2 ?>">Tanggal</th>							
						</tr>
						<tr class="info"> 
							<th>No</th>
							<th>NIP</th> 
							<th>Nama Guru</th>
							<?php for($i=1; $i<=$days_in_month; $i++):?>
							<th><?= $i ?></th>
							<?php endfor ?>
							<th>Total Jam Masuk</th>	
							<th>TOTAL JAM ALPA</th>								
						</tr>
						<?php 
						$monthyears = isset($q['my']) ? $q['my'] : '';
						$no = 1;
						foreach($employee as $row):						
						?>
							<tr> 
								<td><?= $no ?></td>
								<td><?= $row['nip']?></td> 
								<td><?= $row['name']?></td>
								<?php 
									$total_jam = 0;
									$total_alpa=0;
									for($i=1; $i<=$days_in_month; $i++):
										$ttl_jam_employee = $this->Presensi_model->get_total_jam($row['id'], $monthyears.'-'.$i, 'approved' )['total_jam'] ? : 0;	
										$total_jam += $ttl_jam_employee;

										/**get setting jam mengajar*/
										$set_jam = $this->Jam_model->get(array('jam_id'=>1))['jam'];
										$ttl_alpa_emp = $set_jam * 2;
										$total_alpa += $ttl_alpa_emp;
								?>
								<td <?php
										$dt=strtolower(date('l', strtotime($monthyears.'-'.$i)));
										if($dt == "sunday" OR $ttl_jam_employee == 0) echo 'style="background-color:red"';
									?>>
									<?= $ttl_jam_employee ?>
								</td>
								<?php 
									endfor 
								?>
								<td style="font-weight:bold"><?= $total_jam ?></td>
								<td style="font-weight:bold"><?= $total_alpa-$total_jam ?></td>
							</tr>
						<?php $no++; 
						endforeach ?>

					</table>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- end view report -->
			<?php endif ?>

					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>

<script>
$(".monthyear").datepicker( {
    format: "mm-yyyy",
    startView: "months", 
    minViewMode: "months"
});
</script>
