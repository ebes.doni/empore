
<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>
	<section class="content" style="font-size: 12px;">
		<div class="row mb-3">
			<div class="col-md-12">
				<div class="card card-success">
					<div class="card-body">
						<?= form_open(current_url(), array('method' => 'get')) ?> <br>
						<div class="row mb-3">

							<div class="col-md-3 mb-2">  
								<div class="form-group">
									<select class="form-control form-select" name="p" required >
										<option value="">-- Tahun Ajaran --</option>
										<?php foreach ($period as $row): ?>
											<option <?= (isset($q['p']) AND $q['p'] == $row['period_id']) ? 'selected' : '' ?> 
											value="<?= $row['period_id'] ?>"><?= $row['period_start'].'/'.$row['period_end'] ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>

							<div class="col-md-3 mb-2">  
								<div class="form-group">
									<select class="form-control form-select" name="c" required >
										<option value="">-- Kelas --</option>
										<option value="all" <?php if(isset($q['c'])) if($q['c'] == 'all') echo 'selected' ?>>Semua Kelas</option>
										<?php foreach ($class as $row): ?>
											<option <?= (isset($q['c']) AND $q['c'] == $row['class_id']) ? 'selected' : '' ?> value="<?= $row['class_id'] ?>"><?= $row['class_name'] ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>

							<div class="col-md-1 mb-2">
                                <button type="submit" class="btn btn-primary">Filter</button>
                            </div>
							
							<?php if ($q) { ?>
							<?php 
								if($q['c'] == 'all'){
									$param_class = ''; 
									$hclass = '&c=all';
								}else{
									$param_class =	'AND student.class_class_id = '.$q['c'];	
									$hclass = '&c='.$q['c'];							
								}
								$period_id = $q['p'];
							?>
							 <div class="col-md-2 mb-2">
								<a class="btn btn-success" 
									href="<?= site_url('manage/report/report_bill_xls' . '/?' . 'p='.$q['p'].$hclass) ?>">
									<i class="fa fa-file-excel-o" ></i> Export Excel
								</a>
							 </div>

							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
				

			<?php if ($q) { ?>
				<div class="card card-info mb-3">
					<div class="card-body table-responsive">
						<table class="table table-responsive table-hover table-bordered" style="white-space: nowrap;">
							<thead class="bg-soft-dark">
								<tr>
									<th colspan="15" style="text-align:center">BULANAN</th>
								</tr>							
								<tr>
									<th>Kelas</th> 
									<th>Nama</th>
									<th>Jenis Pembayaran</th>
									<?php foreach ($month as $key) : ?>
									<th><?= $key['month_name'] ?></th>
									<?php endforeach ?>
								</tr>
							</thead>
							<tbody>
                                <?php 	
								foreach ($this->db->query("SELECT DISTINCT student.*, bulan.`payment_payment_id`, 
									bulan.`student_student_id`, `payment`.`period_period_id`, `pos`.`pos_name` FROM student 
									LEFT JOIN bulan ON bulan.`student_student_id` = student.`student_id`
									LEFT JOIN `payment` ON `payment`.`payment_id` = `bulan`.`payment_payment_id`
									LEFT JOIN `pos` ON `pos`.`pos_id` = `payment`.`pos_pos_id`
									WHERE bulan.`payment_payment_id` <> '' $param_class AND student.student_status = 1
									AND `payment`.`period_period_id` = $period_id")->result_array() as $row) :?> 
								<?php $period = $this->Period_model->get(array('period_id'=>$row['period_period_id'])); ?>                               
									<tr>
										<td><?= $this->Class_model->get(array('class_id'=>$row['class_class_id']))['class_name'] ?></td> 
										<td><?= $row['student_full_name']?></td>
										<td><?= $row['pos_name'].' - T.A '.$period['period_start'].'/'.$period['period_end'] ?></td>
										<?php foreach($this->Bulan_model->get_month() as $m):
											$data = $this->Bulan_model->get(array('student_id'=>$row['student_student_id'],
																					'payment_id'=>$row['payment_payment_id'],
																					'month_id'=>$m['month_id']));
										?>
										<td style="color:<?= ($data[0]['bulan_status']==1) ? '#00E640' : 'red' ?>">
											<?php if(isset($data) OR !empty($data)) echo ($data[0]['bulan_status']==1) ? 'Lunas' : number_format($data[0]['bulan_bill']) ?>
										</td>
										<?php endforeach ?>
									</tr>										
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				
				<!-- start table angsuran -->
				<div class="card card-info">
					<div class="card-body table-responsive">
						<table class="table table-responsive table-hover table-bordered" 
						style="white-space: nowrap;">
							<thead class="bg-soft-dark">
								<tr>
									<th colspan="15" style="text-align:center">ANGSURAN</th>
								</tr>
								<tr>
									<th>Kelas</th> 
									<th>Nama</th>
									<th>Jenis Pembayaran</th>
									<th>Total Tagihan</th>
								</tr>
							</thead>	
							<tbody>
                                <?php //$class_id = $q['c']; $period_id = $q['p'];
								foreach ($this->db->query("SELECT DISTINCT student.*, bebas.`payment_payment_id`, 
									bebas.`student_student_id`, `payment`.`period_period_id`, `pos`.`pos_name` FROM student 
									LEFT JOIN bebas ON bebas.`student_student_id` = student.`student_id`
									LEFT JOIN `payment` ON `payment`.`payment_id` = `bebas`.`payment_payment_id`
									LEFT JOIN `pos` ON `pos`.`pos_id` = `payment`.`pos_pos_id`
									WHERE bebas.`payment_payment_id` <> '' $param_class
									AND `payment`.`period_period_id` = $period_id")->result_array() as $row) :?> 
								<?php $period = $this->Period_model->get(array('period_id'=>$row['period_period_id'])); ?>                               
									<tr>
										<td><?= $this->Class_model->get(array('class_id'=>$row['class_class_id']))['class_name'] ?></td> 
										<td><?= $row['student_full_name']?></td>
										<td><?= $row['pos_name'].' - T.A '.$period['period_start'].'/'.$period['period_end'] ?></td>
										<?php $bebas = $this->Bebas_model->get(array('student_id'=>$row['student_student_id'],
																				  'payment_id'=>$row['payment_payment_id']
																				));
												
										?>
										<td style="color:<?php if(isset($bebas) OR !empty($bebas)) echo ($bebas[0]['bebas_total_pay']==$bebas[0]['bebas_bill']) ? '#00E640' : 'red' ?>">
											<?php if(isset($bebas) OR !empty($bebas)) echo ($bebas[0]['bebas_total_pay']==$bebas[0]['bebas_bill']) ? 'Lunas' : number_format($bebas[0]['bebas_bill']-$bebas[0]['bebas_total_pay']) ?>
										</td>
									</tr>										
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- end table angsuran -->

				<?php } ?>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>