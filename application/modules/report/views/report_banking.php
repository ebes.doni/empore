<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-success">
					<div class="card-header">
                    <?= form_open(current_url(), array('method' => 'get')) ?> <br>
						<div class="row mb-3">
                            <div class="col-md-2 mb-2">  
								<div class="form-group">									
									<select class="form-control form-select" name="p" required>
										<option value="">-- Pilih T.A. --</option>
										<?php foreach($period as $row): ?>
                                        <option <?php if(isset($q['p'])) if($q['p']==$row['period_id']) echo 'selected' ?> value="<?= $row['period_id'] ?>"><?= $row['period_start'].'/'.$row['period_end'] ?></option>
                                        <?php endforeach ?>
                                    </select>
								</div>
							</div>

                            <div class="col-md-2 mb-2">  
								<div class="form-group">									
									<select class="form-control form-select" name="u" id="majors_id" required>
										<option value="">-- Pilih Unit --</option>
                                        <option <?php if(isset($q['u'])) if($q['u']=='all') echo 'selected' ?> value="all">Semua Unit</option>
                                        <?php foreach($unit as $row):?>
                                        <option <?php if(isset($q['u']) AND $q['u']==$row['id']) echo 'selected' ?> value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                                        <?php endforeach ?>
                                    </select>
								</div>
							</div>

                            <div class="col-md-2 mb-2">  
                                <div id="td_kelas">                                
                                    <div class="form-group">                                       
                                        <select class="form-control form-select" name="c" id="class_id" required onchange="get_siswa()">
                                            <option value="">-- Pilih Kelas --</option>
                                            <option <?php if(isset($q['c'])) if($q['c']=='all') echo 'selected' ?> value="all">Semua Kelas</option>
                                            <?php foreach($class as $row):?>
                                            <option <?php if(isset($q['c']) AND $q['c']==$row['class_id']) echo 'selected' ?> value="<?= $row['class_id'] ?>"><?= $row['class_name'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
							</div>

                            <div class="col-md-2 mb-2">  
                                <div id="td_siswa">                                
								    <div class="form-group">
                                    <select class="form-control form-select" name="s" id="student_id" required>
                                        <option value="">-- Pilih Siswa --</option>
                                        <option <?php if(isset($q['s'])) if($q['s']=='all') echo 'selected' ?> value="all">Semua Siswa</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- <div class="col-md-2">  
                                <div class="form-group">
                                <label>Kasir</label>
                                <select class="form-control" name="k" id="created_by">
                                    <option value="">-- Pilih Kasir --</option>
                                    <option <?php if(isset($q['k'])) if($q['k']=='all') echo 'selected' ?> value="all">Semua Kasir</option>
                                    <?php foreach($users as $row):?>
                                        <option <?php if(isset($q['k']) AND $q['k']==$row['user_full_name']) echo 'selected' ?> value="<?= $row['user_full_name'] ?>"><?= $row['user_full_name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                                </div>
                            </div> -->
                            <div class="col-md-1 mb-2">
                                <button type="submit" class="btn btn-primary">Filter</button>
                            </div>
                            <?php if ($q) { ?>
                            <div class="col-md-2 mb-2">
                                <a class="btn btn-success" target="_blank" href="<?= site_url('manage/report/report_banking_xls' . '/?' . http_build_query($q)) ?>"><i class="fa fa-file-excel-o"></i> Excel</a>
                            </div>
                            <?php } ?>
							
						</div>
                    <?= form_close() ?>		
                    
			<?php if($q):?>
			<!-- view report -->
                <div class="card card-info">
                    <div class="card-body table-responsive">                            
                        <table id="dtable" class="table table-responsive">
                            <thead class="bg-soft-dark">
                                <tr>
                                    <th>No.</th>
                                    <th>NIS</th>
                                    <th>Nama</th>
                                    <th>Kelas</th>
                                    <th>Total Debit</th>
                                    <th>Total Kredit</th>
                                    <th>Saldo Akhir</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; $total_banking_kelas = 0;	
                                foreach($student as $row): 
                                    $debit_total = 0;
                                    $credit_total = 0;
                                    foreach($this->Banking_model->get(array('student_id'=>$row['student_id'], 'period_id'=>$q['p'])) as $debit){
                                        $debit_total += $debit['debit_val'];
                                    }
                                    foreach($this->Banking_model->get(array('student_id'=>$row['student_id'], 'period_id'=>$q['p'])) as $credit){
                                        $credit_total += $credit['credit_val'];
                                    }
                                    $saldo = $debit_total - $credit_total;
                                    $total_banking_kelas += $saldo;
                                  
                                ?>
                                <?php if($saldo == 0){
                                    continue;
                                }else{ ?>                               
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= $row['student_nis'] ?></td> 
                                    <td><?= $row['student_full_name'] ?></td>
                                    <td><?= $row['class_name'] ?></td>
                                    <td class="numeric"><?= $debit_total ?></td>
                                    <td class="numeric"><?= $credit_total ?></td>
                                    <td class="numeric"><?= $saldo ?></td>
                                    <td><a href="<?= site_url('manage/banking/banking_book' . '/?n='.$q['p'].'&r='.$row['student_nis'] ) ?>" 
                                            target="_blank" class="btn btn-xs btn-danger">
                                            <i class="fa fa-print"></i> Buku Tabungan
                                        </a>
                                        <a data-bs-toggle="collapse" 
                                            href="#collapse<?= $row['student_id'] ?>">
                                            <button class="btn btn-info btn-xs">
                                            <i class="fa fa-list"></i>  Rincian</button>
                                        </a>
                                    </td>
                                </tr>
                                    <!-- rincian -->
                                    <tr id="collapse<?= $row['student_id'] ?>" class="collapse">
										<td></td>
										<td colspan="6">
											<table id="dtable" class="table table-no-bordered table-responsive" style="white-space: nowrap;">
												<thead>
													<tr>
														<th>Kategori</th>
														<th>Tanggal</th>
														<th>Kasir</th>
														<th>Debit</th>
														<th>Kredit</th>
                                                        <th>Catatan</th>
													</tr>
												</thead>
												<tbody>

                                                <?php												
													foreach($this->Banking_model->get(array('student_id'=>$row['student_id'], 'period_id'=>$q['p'])) as $cr):
												?>
                                                    <?php if($cr['credit_val']==0){ continue; }else{ ?>
													<tr>
														<td><?= $cr['code'] ?></td>
														<td><?= $cr['trans_at'] ?></td>
														<td><?= $cr['created_by'] ?></td>
														<td> - </td>
														<td>Rp. <?= number_format($cr['credit_val'])?></td>
                                                        <td><?= $cr['note'] ?></td>
													</tr>
                                                    <?php } ?> 
												<?php endforeach ?>

												<?php                                   																					
													foreach($this->Banking_model->get(array('student_id'=>$row['student_id'], 'period_id'=>$q['p'])) as $db):
												?>
                                                    <?php if($db['debit_val']==0){ continue; }else{ ?>
													<tr>
														<td><?= $db['code'] ?></td>
														<td><?= $db['trans_at'] ?></td>
														<td><?= $db['created_by'] ?></td>
														<td>Rp.<?= number_format($db['debit_val'])?></td>
														<td> - </td>
                                                        <td><?= $db['note'] ?></td>
													</tr>
                                                    <?php } ?> 
												<?php endforeach ?>

												

												</tbody>
											</table>
										</td>
									</tr>
                                    <!-- end rincian -->

                                <?php } ?>
                                <?php $no++; 
                                endforeach ?>
                            </tbody>
                        </table>    
                        <!-- footer table calculate total -->
                        <tr style="background-color: #f0f0f0;">
                            <td></td>
                            <td colspan="4" align="center">
                                <b>Total Tabungan <?=  $q['c']!='all' ? 'Kelas '.$this->Class_model->get(array('class_id'=>$q['c']))['class_name'] :'Semua Kelas' ?> </b>
                            </td>
                            <td colspan="2">
                                <span style="font-size:18px;"><strong>Rp <?= number_format($total_banking_kelas) ?></strong></span>							            
                            </td>
                        </tr>            
                    </div>
                    <!-- /.card-body -->
                </div>
			<!-- view report -->
			<?php endif ?>

					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>

<script>
    function get_kelas(){
        var id_majors    = $("#majors_id").val();
        //alert(id_jurusan+id_kelas);
        $.ajax({ 
            url: '<?= site_url('manage/report/get_mclass') ?>',
            type: 'POST', 
            data: {
                    'id_majors': id_majors,
            },    
            success: function(msg) {
                    $("#td_kelas").html(msg);
            },
			error: function(msg){
					alert('msg');
			}
            
        });
    }
    
    function get_siswa(){
        var id_majors    = $("#majors_id").val();
        var id_class     = $("#class_id").val();
        // alert(id_majors+id_class);
        $.ajax({ 
            url: '<?= site_url('manage/report/get_mstudent') ?>',
            type: 'POST', 
            data: {
                    'id_majors': id_majors,
                    'id_class': id_class,
            },    
            success: function(msg) {
                    $("#td_siswa").html(msg);
            },
			error: function(msg){
					alert('msg');
			}
            
        });
    }
</script>
