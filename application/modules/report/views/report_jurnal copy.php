
<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-success">
					<div class="box-header">
						<?= form_open(current_url(), array('method' => 'get')) ?> <br>
						<div class="row">
							<div class="col-md-3">  
								<div class="form-group">
									<div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
										<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
										<input class="form-control" type="text" name="ds" id="ds" readonly="readonly" <?php echo (isset($q['ds'])) ? 'value="'.$q['ds'].'"' : '' ?> placeholder="Tanggal Awal">
									</div>
								</div>
							</div>
							<div class="col-md-3">  
								<div class="form-group">
									<div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
										<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
										<input class="form-control" type="text" name="de" id="de" readonly="readonly" <?php echo (isset($q['de'])) ? 'value="'.$q['de'].'"' : '' ?> placeholder="Tanggal Akhir">
										
									</div>
								</div>
							</div>
							<button type="submit" class="btn btn-primary">Filter</button>
							<?php if ($q) { ?>
							<a class="btn btn-success" href="<?php echo site_url('manage/report/report' . '/?' . http_build_query($q)) ?>"><i class="fa fa-file-excel-o" ></i> Export Excel</a>
							<?php } ?>
						</div>
						<?= form_close() ?>		

			<?php if(isset($q['ds'])):?>
			<!-- view report -->
			<div id="div_show_data">
				<div class="box box-primary box-solid">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="fa fa-file-text-o"></span> Laporan per Tanggal <?= $q['ds'] ?> Sampai <?= $q['de'] ?></h3>
					</div>
					<div class="box-body table-responsive">
						<div id="xtable_wrapper" class="dataTables_wrapper no-footer">
							<table class="table table-responsive table-hover table-bordered dataTable no-footer" id="xtable" style="white-space: nowrap; width: 1203px;" role="grid" aria-describedby="xtable_info">
								<thead>
									<tr role="row">
										<th>Jurnal</th>
										<th>Tanggal</th>
										<th>Kode Jurnal</th>
										<th>Keterangan</th>
										<th>NIS</th>
										<th>Nama Siswa</th>
										<th>Kelas</th>
										<th>Penerimaan</th>
										<th>Pengeluaran</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$saldo_awal_db = 0;
										$saldo_awal_cr = 0; 
										$subtotal_debit = 0;
										foreach($debit as $row):
											$subtotal_debit += $row['debit_value'];
											if($row['student_id'] != NULL){
												$student_nis 		= $this->Student_model->get(array('id'=>$row['student_id']))['student_nis'];
												$student_fullname 	= $this->Student_model->get(array('id'=>$row['student_id']))['student_full_name']; 
												$class_name 		= $this->Student_model->get(array('id'=>$row['student_id']))['class_name'];	
											}else{
												$student_nis 		= '-';
												$student_fullname	= '-';
												$class_name			= '-';
											}
									?>										
									<tr role="row" class="odd">
										<td><?= $this->Jurnal_model->get(array('jurnal_id'=>$row['account_id']))['desc'] ?></td>
										<td><?= $row['debit_date'] ?></td>
										<td><?= $this->Jurnal_model->get(array('jurnal_id'=>$row['account_id']))['code'] ?></td>
										<td><?= $row['debit_desc'] ?></td>
										<td><?= $student_nis ?></td>
										<td><?= $student_fullname ?></td>
										<td><?= $class_name ?></td>
										<td><?= number_format($row['debit_value']) ?></td>
										<td> - </td>
									</tr>
									<?php endforeach ?>		

									<?php $subtotal_kredit = 0;
										foreach($kredit as $row):
											$subtotal_kredit += $row['kredit_value'];
											if($row['student_id'] != NULL){
												$student_nis 		= $this->Student_model->get(array('id'=>$row['student_id']))['student_nis'];
												$student_fullname 	= $this->Student_model->get(array('id'=>$row['student_id']))['student_full_name']; 
												$class_name 		= $this->Student_model->get(array('id'=>$row['student_id']))['class_name'];	
											}else{
												$student_nis 		= '-';
												$student_fullname	= '-';
												$class_name			= '-';
											}
									?>										
									<tr role="row" class="odd">
										<td><?= $this->Jurnal_model->get(array('jurnal_id'=>$row['account_id']))['desc'] ?></td>
										<td><?= $row['kredit_date'] ?></td>
										<td><?= $this->Jurnal_model->get(array('jurnal_id'=>$row['account_id']))['code'] ?></td>
										<td><?= $row['kredit_desc'] ?></td>
										<td><?= $student_nis ?></td>
										<td><?= $student_fullname ?></td>
										<td><?= $class_name ?></td>
										<td> - </td>
										<td><?= number_format($row['kredit_value']) ?></td>
									</tr>
									<?php endforeach ?>		
									
									<?php
										$total = $subtotal_debit + $saldo_awal_db;
										$saldo_akhir = $total - $subtotal_kredit;
									?>

								</tbody>
								<tbody>
									<tr style="background-color: #E2F7FF;">
										<td colspan="7" align="right"><strong>Sub Total</strong></td>
										<td><?= number_format($subtotal_debit) ?></td>
										<td><?= number_format($subtotal_kredit) ?></td>
									</tr>
									<tr style="background-color: #F0B2B2;">
										<td colspan="7" align="right"><strong>Saldo Awal</strong></td>
										<td><?= $saldo_awal_db ?></td>
										<td> - </td>
									</tr>
									<tr style="background-color: #FFFCBE;">
										<td colspan="7" align="right"><strong>Total (Sub Total + Saldo Awal)</strong></td>
										<td><?= number_format($total) ?></td>
										<td><?= number_format($subtotal_kredit) ?></td>
									</tr>
										<tr style="background-color: #c2d2f6;">
										<td colspan="7" align="right"><strong>Saldo Akhir</strong></td>
										<td><?= number_format($saldo_akhir) ?></td>
										<td> - </td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="box-footer">
							<table class="table">
								<tbody>
									<tr>
										<td>
											<div class="pull-right">
												<a class="btn btn-success" target="_blank" href="<?= site_url('manage/report/report?ds=2022-11-01&amp;de=2022-11-09')?>">
													<span class="fa fa-file-excel-o"></span> Excel Rekap Laporan
												</a>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- view report -->
			<?php endif ?>

					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
