
<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-success">
					<div class="card-header">
						<?= form_open(current_url(), array('method' => 'get')) ?> <br>
						<div class="row mb-3">

							<div class="col-md-2 mb-2">  
								<div class="form-group">
									<div id="dsFlatpickr" class="js-flatpickr flatpickr-custom input-group"
										data-hs-flatpickr-options='{
											"appendTo": "#dsFlatpickr",
											"dateFormat": "Y-m-d",
											"wrap": true
										}'>
										<div class="input-group-prepend input-group-text" data-bs-toggle>
											<i class="bi-calendar-week"></i>
										</div>
										<input class="flatpickr-custom-form-control form-control" 
												type="text" 
												name="ds" 
												id="ds" 
												placeholder="Tanggal Awal" 
												data-input <?= (isset($q['ds'])) ? 'value="'.$q['ds'].'"' : '' ?> 
												required>
									</div>
								</div>
							</div>

							<div class="col-md-2 mb-2">  
								<div class="form-group">
									<div id="deFlatpickr" class="js-flatpickr flatpickr-custom input-group"
										data-hs-flatpickr-options='{
											"appendTo": "#deFlatpickr",
											"dateFormat": "Y-m-d",
											"wrap": true
										}'>
										<div class="input-group-prepend input-group-text" data-bs-toggle>
											<i class="bi-calendar-week"></i>
										</div>
										<input class="flatpickr-custom-form-control form-control" 
												type="text" 
												name="de" 
												id="de" 
												placeholder="Tanggal Akhir" 
												data-input <?= (isset($q['de'])) ? 'value="'.$q['de'].'"' : '' ?> 
												required>
									</div>
								</div>
							</div>

							<div class="col-md-3 mb-2">  
								<div class="form-group">
									<select required="" name="k" id="k" class="form-control form-select">
										<option value="">-- Pilih Kasir --</option>
										<option value="all" <?php if(isset($q['k'])) if($q['k']=='all') echo 'selected' ?>>Semua Kasir</option>
										<?php foreach($users as $row):?>
											<option value="<?= $row['user_full_name'] ?>" <?php if(isset($q['k'])) if($q['k']==$row['user_full_name']) echo 'selected' ?>><?= $row['user_full_name'] ?></option>
										<?php endforeach ?>
									</select>
								</div>
							</div>

							<div class="col-md-1 mb-2">
								<button type="submit" class="btn btn-primary">Filter</button>
							</div>							
							<?php if ($q) { ?>
							<div class="col-md-2 mb-2">
								<a class="btn btn-success" 
								    href="<?= site_url('manage/report/report_jurnal_xls' . '/?' . http_build_query($q)) ?>">
								    <i class="fa fa-file-excel-o" ></i> Export Excel
								</a>
							</div>
							<?php } ?>
							
						</div>
						<?= form_close() ?>		
						
			<?php if(isset($q['ds'])):?>
			<!-- view report -->
			<div id="div_show_data">
				<div class="card card-primary card-solid">
					<div class="card-header with-border">
						<h3 class="card-title"><span class="fa fa-file-text-o"></span> Laporan per Tanggal <?= pretty_date($q['ds'], 'd F Y', false) ?> s/d <?= pretty_date($q['de'], 'd F Y', false) ?></h3>
					</div>
					<div class="card-body table-responsive">
						<div id="xtable_wrapper" class="dataTables_wrapper no-footer">
							<table class="table table-responsive table-hover table-bordered dataTable no-footer" id="xtable" style="white-space: nowrap; width: 1203px;" role="grid" aria-describedby="xtable_info">
								<thead class="bg-soft-dark">
									<tr role="row">
										<th>Kode Jurnal</th>
										<th>Nama Jurnal</th>
										<th>Type</th>
										<th>Kategori</th>
										<th>Total</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$subtotal_debit = 0;
									$subtotal_kredit = 0;	
									$saldo_awal_db = 0; 
									$total_kas_masuk = 0;
									$total_kas_keluar = 0;
									foreach($this->Debit_model->get(array('date_start'=>$q['ds'], 'date_end'=>$q['de'], 'user_full_name'=>$q['k'])) as $row) {
										$total_kas_masuk += $row['debit_value'];
									}
									foreach($this->Kredit_model->get(array('date_start'=>$q['ds'], 'date_end'=>$q['de'], 'user_full_name'=>$q['k'])) as $row) {
										$total_kas_keluar += $row['kredit_value'];
									}

									foreach($jurnal as $row): 
										$debit_total = 0;
										$credit_total = 0;
										foreach($this->Debit_model->get(array('jurnal_id'=>$row['id'], 'date_start'=>$q['ds'], 'date_end'=>$q['de'], 'user_full_name'=>$q['k'])) as $db){
											$debit_total += $db['debit_value'];
										}
										foreach($this->Kredit_model->get(array('jurnal_id'=>$row['id'], 'date_start'=>$q['ds'], 'date_end'=>$q['de'], 'user_full_name'=>$q['k'])) as $cr){
											$credit_total += $cr['kredit_value'];
										}
									?>										
									<tr role="row" class="odd" <?php if($row['category'] == '#') echo 'style="font-weight:bold;"' ?> >
										<td><?= $row['code'] ?></td>
										<td><?= $row['desc'] ?></td>
										<td><?= $row['type'] ?></td>
										<td><?= $row['category'] ?></td>
										<?php if($row['type']=='Kas Masuk'):?>
											<?php if($row['category'] == '#'){ ?>
												<td><?= 'Rp '.number_format($total_kas_masuk) ?></td>
											<?php }else{ ?>
												<td><?= 'Rp '.number_format($debit_total) ?></td>
											<?php } ?>
										<?php endif ?>
										<?php if($row['type']=='Kas Keluar'):?>
											<?php if($row['category'] == '#'){ ?>
												<td><?= 'Rp '.number_format($total_kas_keluar) ?></td>
											<?php } else { ?>
											<td><?= 'Rp '.number_format($credit_total) ?></td>
											<?php } ?>
										<?php endif ?>
										<td>
											<?php if($row['category'] !== '#'): ?>
											<a data-bs-toggle="collapse" href="#collapse<?= $row['id'] ?>"><button class="btn btn-info btn-sm"><i class="fa fa-list"></i>  Rincian</button></a>
											<!-- <a href="<?= site_url("manage/report/print_bill") ?>" target="_blank"><button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o"></i>  Cetak</button></a> -->
											<?php endif ?>
										</td>
									</tr>
									<tr id="collapse<?= $row['id'] ?>" class="collapse">
										<td></td>
										<td colspan="6">
											<table id="dtable" class="table table-no-bordered table-responsive" style="white-space: nowrap;">
												<thead class="thead-light">
													<tr>
														<th>Nama Pos</th>
														<th>Tanggal</th>
														<th>Kasir</th>
														<th>Penerimaan</th>
														<th>Pengeluaran</th>
													</tr>
												</thead>
												<tbody>

												<?php																							
													foreach($this->Debit_model->get(array('jurnal_id'=>$row['id'], 'date_start'=>$q['ds'], 'date_end'=>$q['de'], 'user_full_name'=>$q['k'])) as $db):
														$subtotal_debit += $db['debit_value'];
												?>
													<tr>
														<td><?= $db['pos_name'] ?></td>
														<td><?= $db['debit_date'] ?></td>
														<td><?= $db['user_full_name'] ?></td>
														<td>Rp.<?= number_format($db['debit_value'])?></td>
														<td> - </td>
													</tr>
												<?php endforeach ?>

												<?php												
													foreach($this->Kredit_model->get(array('jurnal_id'=>$row['id'], 'date_start'=>$q['ds'], 'date_end'=>$q['de'], 'user_full_name'=>$q['k'])) as $cr):
														$subtotal_kredit += $cr['kredit_value'];
												?>
													<tr>
														<td><?= $cr['pos_name'] ?></td>
														<td><?= $cr['kredit_date'] ?></td>
														<td><?= $cr['user_full_name'] ?></td>
														<td> - </td>
														<td>Rp. <?= number_format($cr['kredit_value'])?></td>
													</tr>
												<?php endforeach ?>

												</tbody>
											</table>
										</td>
									</tr>
									
									<?php endforeach ?>		

									
								</tbody>
								<tbody>
									<?php
										$total = $subtotal_debit + $saldo_awal_db;
										$saldo_akhir = $total - $subtotal_kredit;
									?>
									<tr style="background-color: #E2F7FF;">
										<td colspan="4" align="right"><strong>Sub Total</strong></td>
										<td>Rp. <?= number_format($subtotal_debit) ?></td>
										<td>Rp. <?= number_format($subtotal_kredit) ?></td>
									</tr>
									<tr style="background-color: #F0B2B2;">
										<td colspan="4" align="right"><strong>Saldo Awal</strong></td>
										<td><?//= $saldo_awal_db ?></td>
										<td> - </td>
									</tr>
									<tr style="background-color: #FFFCBE;">
										<td colspan="4" align="right"><strong>Total (Sub Total + Saldo Awal)</strong></td>
										<td><?//= number_format($total) ?></td>
										<td>Rp. <?= number_format($subtotal_kredit) ?></td>
									</tr>
										<tr style="background-color: #c2d2f6;">
										<td colspan="4" align="right"><strong>Saldo Akhir</strong></td>
										<td>Rp. <?= number_format($saldo_akhir) ?></td>
										<td> - </td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="card-footer">
							<table class="table">
								<tbody>
									<tr>
										<td>
											<!-- <div class="pull-right">
												<a class="btn btn-success" target="_blank" href="<?= site_url('manage/report/report_jurnal_xls'. '/?' . http_build_query($q))?>">
													<span class="fa fa-file-excel-o"></span> Excel Rekap Laporan
												</a>
											</div> -->
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- view report -->
			<?php endif ?>

					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
