
<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-success">
					<div class="box-header">
						<?php //echo form_open(current_url(), array('method' => 'get')) ?> <br>
						<div class="row">
							<div class="col-md-3">  
								<div class="form-group">
									<div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
										<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
										<input class="form-control" type="text" name="ds" id="ds" readonly="readonly" <?php echo (isset($q['ds'])) ? 'value="'.$q['ds'].'"' : '' ?> placeholder="Tanggal Awal">
									</div>
								</div>
							</div>
							<div class="col-md-3">  
								<div class="form-group">
									<div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
										<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
										<input class="form-control" type="text" name="de" id="de" readonly="readonly" <?php echo (isset($q['de'])) ? 'value="'.$q['de'].'"' : '' ?> placeholder="Tanggal Akhir">
										
									</div>
								</div>
							</div>
							<button type="submit" class="btn btn-primary" onclick="cari_data()">Filter</button>
							<?php if ($q) { ?>
							<a class="btn btn-success" href="<?php echo site_url('manage/report/report' . '/?' . http_build_query($q)) ?>"><i class="fa fa-file-excel-o" ></i> Export Excel</a>
							<?php } ?>
						</div>

			<!-- view report -->
			<div id="div_show_data"> 
    				
				<script>
				$(document).ready(function(){
					$("#xtable").DataTable({
						"order": [[ 0, "asc" ]],
						"columnDefs": [
						{
							"targets": [ 0 ],
							"visible": false,
							"searchable": false
						}
					]
					} );
				});
				</script>
			</div>
			<!-- view report -->


					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>

<script>
    function cari_data(){
        var ds          = $("#ds").val();
        var de          = $("#de").val();
        //alert(id_jurusan+id_kelas);
        if(ds != '' && de != ''){
			$.ajax({ 
				url: '<?= site_url('manage/report/search_report_jurnal')?>',
				type: 'POST', 
				data: {
						'ds'        : ds,
						'de'        : de,
				},    
				success: function(msg) {
						$("#div_show_data").html(msg);
				},
				error: function(msg){
						alert('msg');
				}
				
			});
        }else{
			// $("#ds").prop('required',true);
			// $("#de").prop('required',true);
		}
    }
</script>