<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-success">
					<div class="card-header">
						<?= form_open(current_url(), array('method' => 'get')) ?> <br>
						<div class="row mb-3">
							<div class="col-md-2 mb-2">  
								<div class="form-group">
									<!-- <div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
										<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
										<input class="form-control" type="text" name="ds" id="ds" readonly="readonly" <?= (isset($q['ds'])) ? 'value="'.$q['ds'].'"' : '' ?> placeholder="Tanggal Awal">
									</div> -->
                                    <div id="dsFlatpickr" class="js-flatpickr flatpickr-custom input-group"
										data-hs-flatpickr-options='{
											"appendTo": "#dsFlatpickr",
											"dateFormat": "Y-m-d",
											"wrap": true
										}'>
										<div class="input-group-prepend input-group-text" data-bs-toggle>
											<i class="bi-calendar-week"></i>
										</div>
										<input class="flatpickr-custom-form-control form-control" 
												type="text" 
												name="ds" 
												id="ds" 
												placeholder="Tanggal Awal" 
												data-input <?= (isset($q['ds'])) ? 'value="'.$q['ds'].'"' : '' ?> 
												required>
									</div>
								</div>
							</div>
							<div class="col-md-2 mb-2">  
								<div class="form-group">
									<!-- <div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
										<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
										<input class="form-control" type="text" name="de" id="de" readonly="readonly" <?= (isset($q['de'])) ? 'value="'.$q['de'].'"' : '' ?> placeholder="Tanggal Akhir">
									</div> -->
                                    <div id="deFlatpickr" class="js-flatpickr flatpickr-custom input-group"
										data-hs-flatpickr-options='{
											"appendTo": "#deFlatpickr",
											"dateFormat": "Y-m-d",
											"wrap": true
										}'>
										<div class="input-group-prepend input-group-text" data-bs-toggle>
											<i class="bi-calendar-week"></i>
										</div>
										<input class="flatpickr-custom-form-control form-control" 
												type="text" 
												name="de" 
												id="de" 
												placeholder="Tanggal Akhir" 
												data-input <?= (isset($q['de'])) ? 'value="'.$q['de'].'"' : '' ?> 
												required>
									</div>
								</div>
							</div>					
                            <div class="col-md-3 mb-2">  
    							<div class="form-group">
    							    <div id="div_item"> 
                                    <select name="up" id="up" class="form-control form-select" required="">
                                        <option value="">-- Pilih Unit Pos --</option>
                                        <option value="all" <?php if(isset($q['up'])) if($q['up'] == 'all') echo 'selected' ?>>Semua Unit Pos</option>
                                        <?php foreach($pos as $row):?>
                                        <option <?php if(isset($q['up'])) if($q['up'] == $row['pos_id']) echo 'selected' ?> value="<?= $row['pos_id'] ?>"><?= $row['jurnal_code'].' | '.$row['jurnal_desc'].' | '.$row['pos_name'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                    </div>
							    </div>
							</div>
                            <div class="col-md-2 mb-2">  
								<div class="form-group">
									<select required="" name="k" id="k" class="form-control form-select">
										<option value="">-- Pilih Kasir --</option>
										<option value="all" <?php if(isset($q['k'])) if($q['k']=='all') echo 'selected' ?>>Semua Kasir</option>
										<?php foreach($users as $row):?>
											<option value="<?= $row['user_full_name'] ?>" <?php if(isset($q['k'])) if($q['k']==$row['user_full_name']) echo 'selected' ?>><?= $row['user_full_name'] ?></option>
										<?php endforeach ?>
									</select>
								</div>
							</div>
                            <div class="col-md-1 mb-2">
							    <button type="submit" class="btn btn-primary">Filter</button>
                            </div>
							<?php if ($q) { ?>
                            <div class="col-md-2 mb-2">
							    <a class="btn btn-success" href="<?= site_url('manage/report/report_unitpos_xls' . '/?' . http_build_query($q)) ?>"><i class="fa fa-file-excel-o" ></i> Export Excel</a>
                            </div>
                            <?php } ?>
						</div>
						<?= form_close() ?>		
                        
			<?php if($q):?>
			<!-- view report -->
			<div id="div_show_data"> 
                <div class="card card-primary card-solid">
            		    <div class="card-header with-border">
            			  <h3 class="card-title"><span class="fa fa-file-text-o"></span> Laporan per Tanggal <?= pretty_date($q['ds'], 'd F Y', false) ?> s/d <?= pretty_date($q['de'], 'd F Y', false) ?></h3>
            			</div>
            			<div class="card-body table-responsive">
                            <table id="dtable">
            			        <thead class="bg-soft-dark">
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Kode Jurnal</th>
                                        <th>Nama POS</th>
                                        <th>NIS</th>
                                        <th>Nama Siswa</th>
                                        <th>Kelas</th>
                                        <th>Penerimaan</th>
                                        <th>Pengeluaran</th>
                                        <th>Keterangan</th>
                                        <th>Kasir</th>
                                        <th>Penerima</th>
                                    </tr>
    						    </thead>
    						    <tbody>         
                                <?php 
                                $saldo_awal_db = 0;
                                $saldo_awal_cr = 0; 
                                $subtotal_debit = 0 ;
                                foreach($debit as $row):
                                    $subtotal_debit += $row['debit_value'];
                                    if($row['student_id'] != NULL){
                                        $student_nis 		= $this->Student_model->get(array('id'=>$row['student_id']))['student_nis'];
                                        $student_fullname 	= $this->Student_model->get(array('id'=>$row['student_id']))['student_full_name']; 
                                        $class_name 		= $this->Student_model->get(array('id'=>$row['student_id']))['class_name'];	
                                    }else{
                                        $student_nis 		= '-';
                                        $student_fullname	= '-';
                                        $class_name			= '-';
                                    }
                                    $period_ta = '';
                                    if($row['pos_id'] != NULL || !empty($row['pos_id'])){
                                        $period = $this->Payment_model->get(array('pos_id'=>$row['pos_id']));
                                        if(empty($period[0])){
                                            $period_ta = '';
                                        }else{
                                          $period = $period[0];
                                          $period_ta = ' - T.A '.$period['period_start'].'/'.$period['period_end'];  
                                        }
                                    }
                                    ?>
                                    <tr>
                                        <td><?= $row['debit_date'] ?></td>
                                        <td><?= $row['jurnal_code'] ?></td>
                                        <td><?= $row['pos_name'].$period_ta ?></td>
                                        <td><?= $student_nis ?></td>
                                        <td><?= $student_fullname ?></td>
                                        <td><?= $class_name ?></td>
                                        <td class="numeric"><?= $row['debit_value'] ?></td>
                                        <td> - </td>
                                        <td><?= $row['debit_desc'] ?></td>
                                        <td><?= $row['user_full_name'] ?></td>
                                        <td><?= $row['recipient'] ?></td>
                                    </tr>
                                <?php endforeach ?>

                                <?php $subtotal_kredit = 0;
                                foreach($kredit as $row):
                                    $subtotal_kredit += $row['kredit_value'];
                                    if($row['student_id'] != NULL){
                                        $student_nis 		= $this->Student_model->get(array('id'=>$row['student_id']))['student_nis'];
                                        $student_fullname 	= $this->Student_model->get(array('id'=>$row['student_id']))['student_full_name']; 
                                        $class_name 		= $this->Student_model->get(array('id'=>$row['student_id']))['class_name'];	
                                    }else{
                                        $student_nis 		= '-';
                                        $student_fullname	= '-';
                                        $class_name			= '-';
                                    }
                                    if($row['pos_id'] != NULL || !empty($row['pos_id'])){
                                        $period = $this->Payment_model->get(array('pos_id'=>$row['pos_id']));
                                        if(empty($period[0])){
                                            $period_ta = '';
                                        }else{
                                          $period = $period[0];
                                          $period_ta = ' - T.A '.$period['period_start'].'/'.$period['period_end'];  
                                        }
                                    }
                                    ?>
                                    <tr>
                                        <td><?= $row['kredit_date'] ?></td>
                                        <td><?= $row['jurnal_code'] ?></td>
                                        <td><?= $row['pos_name'].$period_ta ?></td>
                                        <td><?= $student_nis ?></td>
                                        <td><?= $student_fullname ?></td>
                                        <td><?= $class_name ?></td>
                                        <td> - </td>
                                        <td class="numeric"><?= $row['kredit_value'] ?></td>
                                        <td><?= $row['kredit_desc'] ?></td>
                                        <td><?= $row['user_full_name'] ?></td>
                                        <td><?= $row['recipient'] ?></td>
                                    </tr>
                                <?php 
                                    endforeach;                         
                                    $total = $subtotal_debit + $saldo_awal_db;
                                    $saldo_akhir = $total - $subtotal_kredit;
                                ?>

                                </tbody>
    						    <tbody>
                                    <tr style="background-color: #E2F7FF;">
                                        <td colspan="9" align="right"><span class="h5">Sub Total</span></td>
                                        <td class="numeric h5"><?= $subtotal_debit ?></td>
                                        <td class="numeric h5"><?= $subtotal_kredit ?></td>
    						        </tr>
                                    <tr style="background-color: #F0B2B2;">
                                        <td colspan="9" align="right"><span class="h5">Saldo Awal</span></td>
                                        <td class="numeric h5"><?= $saldo_awal_db ?></td>
                                        <td> - </td>
    						        </tr>
                                    <tr style="background-color: #FFFCBE;">
                                        <td colspan="9" align="right"><span class="h5">Total (Sub Total + Saldo Awal)</span></td>
                                        <td class="numeric h5"><?= $total ?></td>
                                        <td class="numeric h5"><?= $subtotal_kredit ?></td>
                                    </tr>
                                    <tr style="background-color: #c2d2f6;">
                                        <td colspan="9" align="right"><span class="h5">Saldo Akhir</span></td>
                                        <td class="numeric h5"><?= $saldo_akhir ?></td>
                                        <td> - </td>
                                    </tr>
                            </tbody>
                        </table>
                		
    					<div class="card-footer">
    					<table class="table">
        			        <tbody>
                            <!-- <tr>
        					    <td>
                					<div class="md-6">
                					    <a class="btn btn-danger" target="_blank" href="<?= site_url('manage/unit_pos/cetak_detail'. '/?' . http_build_query($q))?>"><span class="fa fa-file-pdf-o"></span> PDF Per Jenis Anggaran
                					    </a>
                					    <a class="btn btn-success" target="_blank" href="<?= site_url('manage/unit_pos/excel_detail'. '/?' . http_build_query($q))?>"><span class="fa fa-file-excel-o"></span> Excel Per Jenis Anggaran
                					    </a>
                					</div>
        					    </td>
        					    <td>
        					        <div class="pull-right">
                					    <a class="btn btn-danger" target="_blank" href="<?= site_url('manage/unit_pos/cetak_rekap'. '/?' . http_build_query($q))?>"><span class="fa fa-file-pdf-o"></span> PDF Rekap Laporan
                					    </a>
                					    <a class="btn btn-success" target="_blank" href="<?= site_url('manage/unit_pos/excel_rekap'. '/?' . http_build_query($q))?>"><span class="fa fa-file-excel-o"></span> Excel Rekap Laporan
                					    </a>
                					</div>
        					    </td>
    					    </tr> -->
    					</tbody>
                    </table>
                </div>
                </div>
            
                <script>
                    $(document).ready(function(){
                        $("#xtable").DataTable({
                            "order": [[ 0, "asc" ]],
                            "columnDefs": [
                            {
                                "targets": [ 0 ],
                                "visible": false,
                                "searchable": false
                            }
                        ]
                        } );
                    });
                </script>
            </div>
			<!-- view report -->
			<?php endif ?>

					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
