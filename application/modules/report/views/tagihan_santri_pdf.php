<!DOCTYPE html>
<html>
<head>
	<title>Tagihan Santri</title>
</head>

<style type="text/css">
@page {
	margin-top: 0.5cm;
	/*margin-bottom: 0.1em;*/
	margin-left: 1cm;
	margin-right: 1cm;
	margin-bottom: 0.1cm;
}
.name-school{
	font-size: 15pt;
	font-weight: bold;
	padding-bottom: -15px;
}
.alamat{
	font-size: 9pt;
	margin-bottom: -10px;
}
.detail{
	font-size: 11pt;
	font-weight: bold;
	padding-top: -15px;
	padding-bottom: -12px;
}
body {
	font-family: sans-serif;
}
table {
	font-family: verdana,arial,sans-serif;
	font-size:12px;
	color:#333333;
	border-width: none;
	/*border-color: #666666;*/
	border-collapse: collapse;
	width: 100%;
}

th {
	padding-bottom: 8px;
	padding-top: 8px;
	border-color: #666666;
	background-color: #dedede;
	/*border-bottom: solid;*/
	text-align: left;
}

td {
	text-align: left;
	border-color: #666666;
	background-color: #ffffff;
}

hr {
	border: none;
	height: 2px;
	/* Set the hr color */
	color: #333; /* old IE */
	background-color: #333; /* Modern Browsers */
}
.container {
	position: relative;
}

.topright {
	position: absolute;
	top: 0;
	right: 0;
	font-size: 18px;
	border-width: thin;
	padding: 5px;
}
.topright2 {
	position: absolute;
	top: 30px;
	right: 50px;
	font-size: 18px;
	border: 1px solid;
	padding: 5px;
	color: red;
}
</style>
<body>

	<div class="container">
		<div class="topright">TAGIHAN SISWA</div>
	</div>
	<p class="name-school"><?= $setting_school['setting_value'] ?></p>
	<p class="alamat"><?= $setting_address['setting_value'] ?><br>
		<?= $setting_phone['setting_value'] ?></p>
		<hr>

		<p style="text-align: justify; font-family: verdana,arial,sans-serif; font-size:12px; color:#333333; font-weight: bold;">Hal : Pemberitahuan Pembayaran Uang Pembelajaran <br/>
		Kepada Yth. <br/>
		bapak/ibu Orang Tua/Wali Ananda :</p>
	
		<table style="padding-top: 1px; padding-bottom: 1px">
			<tbody>
				<tr>
					<td style="width: 50px;">Nama</td>
					<td style="width: 5px;">:</td>					
					<td style="width: 150px;"><?= $student['student_full_name'] ?></td>				
				</tr>
				<tr>
					<td style="width: 50px;">NIS</td>
					<td style="width: 5px;">:</td>					
					<td style="width: 150px;"><?= $student['student_nis'] ?></td>					
				</tr>
				<tr>
					<td style="width: 50px;">Unit</td>
					<td style="width: 5px;">:</td>
					<td style="width: 150px;"><?= $student['unit_name'] ?></td>
				</tr>
				<tr>
					<td style="width: 50px;">Kelas</td>
					<td style="width: 5px;">:</td>
					<td style="width: 150px;"><?= $student['class_name'] ?></td>
				</tr>
				<tr>
					<td style="width: 50px;">Tahun Ajaran</td>
					<td style="width: 5px;">:</td>
					<td style="width: 150px;"><?= $period['period_start'].'/'.$period['period_end'] ?></td>
				</tr>
			</tbody>
		</table>
		<p style="text-align: justify; font-family: verdana,arial,sans-serif; font-size:12px; color:#333333; font-weight: bold;">
		Dengan ini kami memberitahukan bahwa bapak/ibu dari ananda tersebut diatas masih memiliki tunggakan dengan rincian sebagai berikut :
		</p>
		<hr>
		<p class="detail">Dengan rincian sebagai berikut:</p>

		<table style="border-style: solid;">
					
			<tr>
				<th style="border-bottom: 1px solid; border-top: 1px solid; border-left: 1px solid; width:5px;">No</th>
				<th style="border-bottom: 1px solid; border-top: 1px solid; border-left: 1px solid; padding-top: 5px; padding-bottom: 5px;"></th>
				<th style="border-bottom: 1px solid; border-top: 1px solid; ">Nama Tunggakan</th>
				<th style="border-bottom: 1px solid; border-top: 1px solid;"></th>
				<th style="border-bottom: 1px solid; border-top: 1px solid; text-align: center;"></th>
				<!-- <td style="border-bottom: 1px solid; border-top: 1px solid; border-left: 1px solid; text-align: right;"></td> -->
				<th colspan=2 style="border-bottom: 1px solid; border-top: 1px solid; border-left: 1px solid; text-align: center; border-right: 1px solid;">Nominal</th>
			</tr>

			<!-- get period -->
			<?php $priod=$this->Period_model->get(array('id'=>$q['p'])); ?>	

			<!-- bulanan -->
            <?php $total_tagihan=0; $no=1;
			foreach($bulan as $row):
			$total_tagihan += $row['bulan_bill'];
			?>
			<tr>				
				<td style="border-bottom: 1px solid; border-top: 1px solid; border-left: 1px solid; width:5px;"><?= $no ?></td>
				<td style="border-bottom: 1px solid; border-left: 1px solid;" colspan="2"><?= $this->Payment_model->get(array('id'=>$row['payment_payment_id']))['pos_name'] ?> - T.A <?= $priod['period_start'].'/'.$priod['period_end'] ?>
					- <?= $this->Bulan_model->get_month(array('id'=>$row['month_month_id']))['month_name'] ?>
				</td>
				<td style="border-bottom: 1px solid;"></td>
				<td style="border-bottom: 1px solid; text-align: center;"></td>
				<td style="border-bottom: 1px solid; border-left: 1px solid; text-align: right;">Rp.</td>
				<td style="border-bottom: 1px solid; text-align: right; border-right: 1px solid;"><?= number_format($row['bulan_bill']) ?></td>
			</tr>
			<?php $no++; 
				endforeach ?>
			
			<!-- angsuran -->
			<?php 
			foreach($bebas as $row):
			$total_tagihan += $row['bebas_bill']-$row['bebas_total_pay'];
			?>
			<tr>				
				<td style="border-bottom: 1px solid; border-top: 1px solid; border-left: 1px solid; width:5px;"><?= $no ?></td>
				<td style="border-bottom: 1px solid; border-left: 1px solid;" colspan="2"><?= $this->Payment_model->get(array('id'=>$row['payment_payment_id']))['pos_name'] ?> - T.A <?= $priod['period_start'].'/'.$priod['period_end'] ?></td>
				<td style="border-bottom: 1px solid;"></td>
				<td style="border-bottom: 1px solid; text-align: center;"></td>
				<td style="border-bottom: 1px solid; border-left: 1px solid; text-align: right;">Rp.</td>
				<td style="border-bottom: 1px solid; text-align: right; border-right: 1px solid;"><?= number_format($row['bebas_bill']-$row['bebas_total_pay']) ?></td>
			</tr>
			<?php $no++; 
			endforeach ?>

            <tr>
				<td style="width:5px;"></td>
				<td style="padding-top: 5px; padding-bottom: 5px; font-weight:bold;">Total Tagihan Siswa</td>
				<td style=""></td>
				<td style=""></td>
				<td style="font-weight:bold;"></td>
				<td style="text-align: right; font-weight:bold;">Rp.</td>
				<td style="text-align: right; font-weight:bold;"><?= number_format($total_tagihan) ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			
			<tr>
				<td colspan="7" style="padding-top: 5px; padding-bottom: 5px; text-align: justify;">
				Kami mohon dengan sangat kepada bapak/ibu untuk melunasi tunggakan biaya diatas secepatnya. Jika tidak, maka dengan terpakasa anak bapak/ibu tersebut tidak bisa mengikuti ulangan/ujian.
				</td>				
			</tr>
			<tr>
				<td colspan="7" style="padding-top: 5px; padding-bottom: 5px; text-align: justify;">
				Demikian surat pemberitahuan ini kami sampaikan atas perhatian dan kerjasamanya kami ucapkan terima kasih. Apabila ada kekeliruan dengan surat
				pemberitahuan ini. Kami mohon bapak/ibu melakukan konfirmasi ke bagian keuangan dengan membawa tanda bukti.
				</td>				
			</tr>
			<tr>
				<td colspan="7" style="padding-top: 5px; padding-bottom: 5px;">
				Atas perhatian dan partisipasinya kami ucapkan terima kasih.
				</td>				
			</tr>
			<tr>
				<td colspan="7" style="border-bottom: 0px solid;">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3" style="border-bottom: 0px solid;padding-top: 5px; padding-bottom: 5px; text-align: center;"></td>
				<td style="border-bottom: 0px solid;"></td>
				<td style="border-bottom: 0px solid;"></td>
				<td style="border-bottom: 0px solid; text-align: right;"></td>
				<td style="border-bottom: 0px solid; text-align: right;"><?= $setting_district['setting_value'] ?>, <?= pretty_date(date('Y-m-d'),'d F Y',false) ?></td>
			</tr>
			
			<tr>
				<td colspan="3" style="text-align: center;"></td>
				<td style="border-bottom: 0px solid;"></td>
				<td style="border-bottom: 0px solid;"></td>
				<td style="border-bottom: 0px solid; text-align: right;"></td>
				<td style="border-bottom: 0px solid; text-align: right;"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3" style="text-align: center"></td>
				<td style="border-bottom: 0px solid;"></td>
				<td style="border-bottom: 0px solid;"></td>
				<td style="border-bottom: 0px solid; text-align: right;"></td>
				<td style="border-bottom: 0px solid; text-align: right;">(<?= ucfirst($this->session->userdata('ufullname')); ?>)</td>
			</tr>
		</table>
		<br>

		
		

	</body>
	</html>