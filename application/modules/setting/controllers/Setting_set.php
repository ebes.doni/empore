<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting_set extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('manage/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
    }

    public function index() {
        $this->load->model('setting/Setting_model');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('setting_school', 'Nama', 'trim|required|xss_clean');
        $this->form_validation->set_rules('setting_address', 'Alamat', 'trim|required|xss_clean');
        $this->form_validation->set_rules('setting_phone', 'Nomor Telephone', 'trim|required|xss_clean');
        $this->form_validation->set_rules('setting_district', 'Nama Kecamatan', 'trim|required|xss_clean');
        $this->form_validation->set_rules('setting_city', 'Nama Kota/Kab', 'trim|required|xss_clean');
       
        if ($_POST AND $this->form_validation->run() == TRUE) {
            $param['setting_school'] = $this->input->post('setting_school');
            $param['setting_address'] = $this->input->post('setting_address');
            $param['setting_phone'] = $this->input->post('setting_phone');
            $param['setting_district'] = $this->input->post('setting_district');
            $param['setting_city'] = $this->input->post('setting_city');
            $param['setting_level'] = $this->input->post('setting_level');

            $param['server_api_midtrans'] = $this->input->post('server_api_midtrans');
            $param['client_api_midtrans'] = $this->input->post('client_api_midtrans');

            $param['setting_sms'] = $this->input->post('setting_sms');
            $param['setting_twilio_id'] = $this->input->post('setting_twilio_id');
            $param['setting_twilio_token'] = $this->input->post('setting_twilio_token');
            $param['setting_whatsapp'] = $this->input->post('setting_whatsapp');
            $param['setting_whatsapp_no'] = $this->input->post('setting_whatsapp_no');

            $status =$this->Setting_model->save($param);
            
            if (!empty($_FILES['setting_logo']['name'])) {
                $paramsupdate['setting_logo'] = $this->do_upload($name = 'setting_logo', $fileName= $param['setting_school']);

                $paramsupdate['setting_id'] = $status;
                $this->Setting_model->save($paramsupdate);
            } 

            $this->session->set_flashdata('success', ' Sunting pengaturan berhasil');
            redirect('manage/setting');
        } else {
            $data['title'] = 'Pengaturan';
            $data['setting_school'] = $this->Setting_model->get(array('id' => 1));
            $data['setting_address'] = $this->Setting_model->get(array('id' => 2));
            $data['setting_phone'] = $this->Setting_model->get(array('id' => 3));
            $data['setting_district'] = $this->Setting_model->get(array('id' => 4));
            $data['setting_city'] = $this->Setting_model->get(array('id' => 5));
            $data['setting_logo'] = $this->Setting_model->get(array('id' => 6));
            $data['setting_level'] = $this->Setting_model->get(array('id' => 7));
            $data['setting_user_sms'] = $this->Setting_model->get(array('id' => 8));
            $data['setting_pass_sms'] = $this->Setting_model->get(array('id' => 9));
            $data['setting_sms'] = $this->Setting_model->get(array('id' => 10));
            $data['setting_twilio_id'] = $this->Setting_model->get(array('id' => 11));
            $data['setting_twilio_token'] = $this->Setting_model->get(array('id' => 12));
            $data['setting_whatsapp'] = $this->Setting_model->get(array('id' => 13));    
            $data['setting_whatsapp_no'] = $this->Setting_model->get(array('id' => 14));    
            $data['setting_head_school'] = $this->Setting_model->get(array('id' => 15)); 
            $data['setting_head_finance'] = $this->Setting_model->get(array('id' => 16));      
            $data['server_api_midtrans'] = $this->Setting_model->get(array('id' => 17));   
            $data['client_api_midtrans'] = $this->Setting_model->get(array('id' => 18));   

            $data['main'] = 'setting/setting_list';
            $this->load->view('manage/layout', $data);
        }
    }

    // Setting Upload File Requied
    function do_upload($name=NULL, $fileName=NULL) {
        $this->load->library('upload');

        $config['upload_path'] = FCPATH . 'uploads/school/';

        /* create directory if not exist */
        if (!is_dir($config['upload_path'])) {
          mkdir($config['upload_path'], 0777, TRUE);
      }

      $config['allowed_types'] = 'gif|jpg|jpeg|png';
      $config['max_size'] = '2048';
      $config['file_name'] = $fileName;
      $this->upload->initialize($config);

      if (!$this->upload->do_upload($name)) {
          $this->session->set_flashdata('success', $this->upload->display_errors('', ''));
          redirect(uri_string());
      }

      $upload_data = $this->upload->data();

      return $upload_data['file_name'];
  } 

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
