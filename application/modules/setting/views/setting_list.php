<div class="">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
  </section>
  <section class="content">
    <div class="row">
    <div class="col-md-6">

      <?= form_open_multipart(current_url()); ?>
      <div class="card card-primary">
        <div class="card-body">
         
          <div class="row">
            <div class="">
              <div class="form-group label-floating mb-2">
                <label class="control-label h5">Nama</label>
                <input type="text" name="setting_school" value="<?= $setting_school['setting_value'] ?>" class="form-control" required>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="">
              <div class="form-group label-floating mb-2">
                <label class="control-label h5">Alamat</label>
                <input name="setting_address" type="text" value="<?= $setting_address['setting_value'] ?>" class="form-control" required>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="">
              <div class="form-group label-floating mb-2">
                <label class="control-label h5">Nama Kecamatan</label>
                <input type="text" name="setting_district" value="<?= $setting_district['setting_value'] ?>" class="form-control" required>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="">
              <div class="form-group label-floating mb-2">
                <label class="control-label h5">Nama Kota/Kab</label>
                <input type="text" name="setting_city" value="<?= $setting_city['setting_value'] ?>" class="form-control" required>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="">
              <div class="form-group label-floating mb-2">
                <label class="control-label h5">Nomor Telepon</label>
                <input type="text" name="setting_phone" value="<?= $setting_phone['setting_value'] ?>" class="form-control" required>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="">
              <div class="form-group label-floating mb-2">
                <label class="control-label h5">Server API Midtrans</label>
                <input type="text" name="server_api_midtrans" value="<?= $server_api_midtrans['setting_value'] ?>" class="form-control" required>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="">
              <div class="form-group label-floating mb-2">
                <label class="control-label h5">Client API Midtrans</label>
                <input type="text" name="client_api_midtrans" value="<?= $client_api_midtrans['setting_value'] ?>" class="form-control" required>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="">
              <input type="submit" value="Simpan" class="btn btn-success pull-left">
            </div>
          </div>

        </div>
      </div>
    
    </div>
    <br>

    <div class="col-md-6">
      <div class="row mb-3">
        <div class="col-md-6">

          <div class="card card-primary"> 
            <div class="card-body">
              <label class="h5">Logo</label>
              <a href="#" class="thumbnail">
                <?php if (isset($setting_logo) AND $setting_logo['setting_value'] != NULL) { ?>
                <img src="<?= upload_url('school/' . $setting_logo['setting_value']) ?>" style="height: 50px" >
                <?php } else { ?>
                <img src="<?= media_url('img/missing_logo.gif') ?>" id="target" alt="Choose image to upload">
                <?php } ?>
              </a>
              <input type='file' id="setting_logo" name="setting_logo">
              <p>Ukuran Logo 50x50 pixel</p>
            </div>
          </div>

        </div>
      </div>
   
      <button class="btn btn-info mb-3" 
              type="button" 
              data-bs-toggle="collapse" 
              data-bs-target="#whatsapp">
              <i class="fa fa-arrow-down"></i> Set WhatsApp
      </button>

      <div class="card card-primary"> 
        <div class="card-body">

          <div id="whatsapp" class="collapse">
            <div class="form-group label-floating mb-2">
              <label class="control-label h5">Twilio ID</label>
              <input type="text" name="setting_twilio_id" value="<?= $setting_twilio_id['setting_value'] ?>" class="form-control" required>
            </div>
            <div class="form-group label-floating mb-2">
              <label class="control-label h5">Twilio Token</label>
              <input type="text" name="setting_twilio_token" value="<?= $setting_twilio_token['setting_value'] ?>" class="form-control" required>
            </div>
            <div class="form-group label-floating mb-2">
              <label class="control-label h5">Nomor WhatsApp</label>
              <input type="text" name="setting_whatsapp_no" value="<?= $setting_whatsapp_no['setting_value'] ?>" class="form-control" required>
              <small>Nomor whatsapp yg terdaftar di twilio</small>
            </div>
            <div class="form-group">
              <label class="h5">Aktifkan WA Gateway</label>
              <div class="radio">
                <label>
                  <input type="radio" name="setting_whatsapp" value="Y" <?= ($setting_whatsapp['setting_value'] == 'Y') ? 'checked' : ''; ?>> Ya
                </label>&nbsp;&nbsp;
                <label>
                  <input type="radio" name="setting_whatsapp" value="N" <?= ($setting_whatsapp['setting_value'] == 'N') ? 'checked' : ''; ?>> Tidak
                </label>
              </div>
            </div>
          </div>
                
        </div>
      </div>

    </div>
    <?= form_close() ?>

    </div>
    
  </section>
</div>

<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#target').attr('src', e.target.result);
      };

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#setting_logo").change(function() {
    readURL(this);
  });

</script>


