<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Snap_ppdb extends CI_Controller {
  public function __construct() {
    parent::__construct(TRUE);
      if ($this->session->userdata('logged_ppdb') == NULL) {
          header("Location:" . site_url('ppdb/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
      }
      $this->load->model(array('setting/Setting_model', 
                               'snap/Snap_model',
                               'formulir/Participant_model',
							                 'bayar/Bayar_model',
                               'formulir/Formpay_model',
                               'ppdb/Inpay_model'
                              ));

      /**call library midtrans */
      $params = array('server_key' => $this->Setting_model->get(array('id' => SERVER_API_MIDTRANS))['setting_value'], 
                      'production' => $this->config->item('midtrans_production'));
      $this->load->library('midtrans');
      $this->midtrans->config($params);
        
      $this->load->helper(array('form', 'url'));
  }

  // Snap PPDB
  public function index(){
    $data['title']  = 'Snap Checkout';
    $data['main']   = 'snap/checkout_snap';
    $this->load->view('ppdb/layout', $data);
  }

  public function token_inpay($pay_id=NULL, $total_bayar_inpay=NULL){		
    $ppdb_participant_id  = $this->session->userdata('ppdb_participant_id');
		$participant          = $this->Participant_model->get(array('ppdb_participant_id'=>$ppdb_participant_id));
		$bayar                = $this->Bayar_model->get(array('pay_id'=>$pay_id));
    $payment_total        = $bayar['payment_total'];
    $total                = intval($total_bayar_inpay);
		$order_id             = trim(trim($bayar['no_pendaftaran']).'-'.strtotime(date('YmdHis')));

		$transaction_details = array(
		  'order_id' => $order_id,
		  'gross_amount' => $total
		);

		$customer_details = array(
		  'first_name'    => $participant['nama_peserta'],
		  'email'         => $participant['email'],
		  'phone'         => $participant['nomor_hp']
		);

        $credit_card['secure'] = true;

        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit' => 'day', 
            'duration'  => 1
        );
		
        $payload = array(
            'transaction_details'=> $transaction_details,
            'customer_details'   => $customer_details,
            'credit_card'        => $credit_card,
            'expiry'             => $custom_expiry
		  );
		
		error_log(json_encode($payload));
		$snapToken = $this->midtrans->getSnapToken($payload);		
		error_log($snapToken);
		echo $snapToken;
  }

  public function finish_inpay($pay_id=NULL){
		$result = json_decode($this->input->post('result_data_inpay'));
		
    /**insert to table ppdb_inpay */
    $ppdb_bayar = $this->Bayar_model->get(array('pay_id'=>$pay_id));
    $order_id   = trim(trim($ppdb_bayar['no_pendaftaran']).'-'.strtotime(date('YmdHis')));
    $data['ppdb_bayar_id']        = $pay_id;
    $data['ppdb_inpay_number']    = $order_id;
    $data['ppdb_inpay_bill']      = $result->gross_amount;
    $data['ppdb_inpay_desc']      = 'biaya masuk';
    $data['ppdb_participant_id']  = $ppdb_bayar['nisn'];
    $data['link_pay']             = $result->pdf_url;
    $data['pay_status']           = $result->transaction_status;
    $data['payment_method']       = $result->payment_type;
		$this->Inpay_model->add($data);

    /**update ppdb bayar */
    if(($ppdb_bayar['payment_total']+$result->gross_amount) >= $ppdb_bayar['bill_total']){
      $status_bayar = 'Lunas';
    }else{
      $status_bayar = 'Belum Lunas';
    }

    $this->db->set('payment_total', $ppdb_bayar['payment_total']+$result->gross_amount);
    $this->db->set('pay_status', $status_bayar);
    $this->db->where('pay_id', $pay_id);
    $this->db->update('ppdb_bayar');

		/**update status ppdb_participant */
		$data['ppdb_participant_id'] = $ppdb_bayar['nisn'];
		$data['status'] = $status_bayar;
		$this->Participant_model->add($data);

		redirect(base_url() . 'ppdb/bayar');

  }

    public function token_formpay($ppdb_formpay_id=NULL){		

      $ppdb_participant_id  = $this->session->userdata('ppdb_participant_id');
      $participant          = $this->Participant_model->get(array('ppdb_participant_id'=>$ppdb_participant_id));
      $formpay              = $this->Formpay_model->get(array('ppdb_formpay_id'=>$ppdb_formpay_id));
      $total                = intval($formpay['bill_total']);
      
      $transaction_details = array(
        'order_id' => 'F'.date('Y').sprintf("%'.05d\n", $formpay['ppdb_formpay_id']),
        'gross_amount' => $total
      );
  
      $customer_details = array(
        'first_name'    => $participant['nama_peserta'],
        'email'         => $participant['email'],
        'phone'         => $participant['nomor_hp']
      );
  
          $credit_card['secure'] = true;
  
          $time = time();
          $custom_expiry = array(
              'start_time' => date("Y-m-d H:i:s O",$time),
              'unit' => 'day', 
              'duration'  => 1
          );
      
          $payload = array(
              'transaction_details'=> $transaction_details,
              'customer_details'   => $customer_details,
              'credit_card'        => $credit_card,
              'expiry'             => $custom_expiry
        );
      
      error_log(json_encode($payload));
      $snapToken = $this->midtrans->getSnapToken($payload);		
      error_log($snapToken);
      echo $snapToken;
  
    }

    public function finish_formpay($ppdb_formpay_id=NULL){
      $result = json_decode($this->input->post('result_data_formpay'));
      
      $this->db->set('link_pay', $result->pdf_url);
      $this->db->set('pay_status', $result->transaction_status);
      $this->db->set('payment_date', $result->transaction_time);
      $this->db->set('payment_method', $result->payment_type);
      $this->db->set('payment_total', $result->gross_amount);
      $this->db->where('ppdb_formpay_id', $ppdb_formpay_id);
      $this->db->update('ppdb_formpay');
  
      redirect(base_url() . 'ppdb/formulir');
      
    }

}