<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Snap_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    // Get ppdb_berkas from database
    function get($params = array()){
        if(isset($params['bayar_id'])){
            $this->db->where('bayar_id', $params['bayar_id']);
        }
        if(isset($params['no_pendaftaran'])){
            $this->db->where('no_pendaftaran', $params['no_pendaftaran']);
        }
        if(isset($params['tanggal'])){
            $this->db->where('tanggal', $params['tanggal']);
        }
        if(isset($params['metode_pembayaran'])){
            $this->db->where('metode_pembayaran', $params['metode_pembayaran']);
        }
        if(isset($params['total_tagihan'])){
            $this->db->where('total_tagihan', $params['total_tagihan']);
        }
        if(isset($params['status'])){
            $this->db->where('status', $params['status']);
        }
        
        if(isset($params['limit'])){
            if(!isset($params['offset'])){
                $params['offset'] = NULL;
            }
            $this->db->limit($params['limit'], $params['offset']);
        }

        if(isset($params['order_by'])){
            $this->db->order_by($params['order_by'], 'desc');
        }else{
            $this->db->order_by('bayar_id', 'desc');
        }
        $this->db->select('ppdb_bayar.*');
        $res = $this->db->get('ppdb_bayar');

        if(isset($params['bayar_id'])){
            return $res->row_array();
        }else{
            return $res->result_array();
        }
    }
    
    
// Add and update to database
function add($data = array()) {
    if(isset($data['bayar_id'])) {
        $this->db->set('bayar_id', $data['bayar_id']);
    }
    if(isset($data['no_pendafataran'])) {
        $this->db->set('no_pendafataran', $data['no_pendafataran']);
    }
    if(isset($data['tanggal'])) {
        $this->db->set('tanggal', $data['tanggal']);
    }
    if(isset($data['metode_pembayaran'])) {
        $this->db->set('metode_pembayaran', $data['metode_pembayaran']);
    }
    if(isset($data['total_tagihan'])) {
        $this->db->set('total_tagihan', $data['total_tagihan']);
    }
    if(isset($data['status'])) {
        $this->db->set('status', $data['status']);
    }
    if (isset($data['bayar_id'])) {
        $this->db->where('bayar_id', $data['bayar_id']);
        $this->db->update('ppdb_bayar');
        $id = $data['bayar_id'];
    } else {
        $this->db->insert('ppdb_bayar');
        $id = $this->db->insert_id();
    }

    $status = $this->db->affected_rows();
    return ($status == 0) ? FALSE : $id;
}

// Delete ppdb_berkas to database
function delete($id) {
    $this->db->where('bayar_id', $id);
    $this->db->delete('ppdb_bayar');
}

}
