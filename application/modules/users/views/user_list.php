<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">
						<a href="<?= site_url('manage/users/add') ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah</a>
						<a href="<?= site_url('manage/users/role')?>" class="btn btn-sm btn-danger"><i class="fa fa-cogs"></i> Role</a>
					</div>
					<!-- /.card-header -->
					<div class="card-body table-responsive">
						<table id="dtable" class="table table-hover">
							<thead class="bg-soft-dark">
								<tr>
									<th>No</th>
									<th>Email</th>
									<th>Nama</th>
									<th>Hak Akses</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if (!empty($user)) {
									$i = 1;
									foreach ($user as $row):
										?>
										<tr>
											<td><?= $i; ?></td>
											<td><?= $row['user_email']; ?></td>
											<td><?= $row['user_full_name']; ?></td>
											<td><?= $row['role_name']; ?></td>
											<td>
												<a href="<?= site_url('manage/users/view/' . $row['user_id']) ?>" class="btn btn-xs btn-info" data-toggle="tooltip" title="Lihat"><i class="fa fa-eye"></i></a>

												<?php if ($this->session->userdata('uid') != $row['user_id']) { ?>
												<a href="<?= site_url('manage/users/rpw/' . $row['user_id']) ?>" class="btn btn-xs btn-warning"><i class="fa fa-lock" data-toggle="tooltip" title="Reset Password"></i></a>
												<?php } else {
													?>
													<a href="<?= site_url('manage/profile/cpw/'); ?>" class="btn btn-xs btn-warning"><i class="fa fa-rotate-left" data-toggle="tooltip" title="Ubah Password"></i></a>
													<?php } ?>

													<?php if ($row['user_id'] != $this->session->userdata('uid')) { ?>
													<a href="#delModal<?= $row['user_id']; ?>" data-bs-toggle="modal" class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" title="Hapus"></i></a>
													<?php } ?>
												</td>	
											</tr>
											<div class="modal modal-default fade" id="delModal<?= $row['user_id']; ?>">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<h3 class="modal-title"><span class="fa fa-warning"></span> Konfirmasi penghapusan</h3>
															<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
															</div>
															<div class="modal-body">
																<p>Apakah anda yakin akan menghapus data ini?</p>
															</div>
															<div class="modal-footer">
																<?= form_open('manage/users/delete/' . $row['user_id']); ?>
																<input type="hidden" name="delName" value="<?= $row['user_full_name']; ?>">
																<button type="button" class="btn btn-default pull-left" data-bs-dismiss="modal"><span class="fa fa-close"></span> Batal</button>
																<button type="submit" class="btn btn-danger"><span class="fa fa-check"></span> Hapus</button>
																<?= form_close(); ?>
															</div>
														</div>
														<!-- /.modal-content -->
													</div>
													<!-- /.modal-dialog -->
												</div>
												<?php
												$i++;
											endforeach;
										} else {
											?>
											<tr id="row">
												<td colspan="6" align="center">Data Kosong</td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
								<!-- /.card-body -->
							</div>
							<!-- /.card -->
						</div>
					</div>
				</section>
				<!-- /.content -->
			</div>