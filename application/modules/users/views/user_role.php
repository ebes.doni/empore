<div class="">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- ========== Breadcrumbs Start ========== -->
		<?php $this->load->view('manage/breadcrumbs'); ?>
		<!-- ========== Breadcrumbs End ========== -->
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="row">
			<div class="col-md-4">

				<!-- Profile Image -->
				<div class="card card-primary">
					<div class="card-body">
						
						<div class="form-group mt-2">
							<label>Hak Akses <small data-toggle="tooltip" title="" data-original-title="Wajib diisi">*</small></label>
							<select name="role_id" id="role_id" class="form-control form-select" onchange="loadData()">
								<option value="">-Pilih Hak Akses-</option>		
                                <?php foreach($role as $r): ?>
                                    <option value="<?= $r['role_id'] ?>"><?= $r['role_name'] ?></option>
                                <?php endforeach ?>						 
                            </select>
						</div>
						<br>												
						<a href="<?= site_url('manage/users/')?>" class="btn btn-info btn-block"><b>Kembali</b></a>
						
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->

			</div>
			<div class="col-md-8">
				<!-- About Me card -->
				<div class="card card-primary">
					<div class="card-header with-border">
						<h3 class="card-title">Hak Akses</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
					    <div id="tabel"></div>	
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->

			</div>
		</div>
	</section>
</div>

<script type="text/javascript">
    function loadData(){
        var role_id = $("#role_id").val();
        $.ajax({
            type:'GET',
            url :'<?= site_url('manage/users/modul') ?>',
            data:'role_id='+role_id,
            success:function(html){
                $("#tabel").html(html);
            }
        })
    }
    
    function addRule(modul_id){
        var role_id = $("#role_id").val();
        $.ajax({
            type:'GET',
            url :'<?= site_url('manage/users/addrule') ?>',
            data:'role_id='+role_id+'&modul_id='+modul_id,
            success:function(html){
                alert('suksess mengubah akses');
            }
        })
    }
</script>