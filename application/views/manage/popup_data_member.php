<div class="modal fade" id="dataMember" role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cari Data Siswa</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                    <div class="card-body table-responsive">
                        <table id="dtable" class="table table-hover">
                            <thead class="thead-light">						
                                <tr>
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($members)) {
                                    $i = 1;
                                    foreach ($members as $row):
                                        ?>
                                        <tr>													
                                            <td><?= $i; ?></td>
                                            <td><?= $row['member_id']; ?></td>
                                            <td><?= $row['member_name']; ?></td>
                                            <td><label class="label <?= ($row['is_active']==1) ? 'label-success' : 'label-danger' ?>"><?php echo ($row['is_active']==1) ? 'Aktif' : 'Tidak Aktif' ?></label></td>
                                            <td align="center">
                                                <button type="button" 
                                                        data-bs-dismiss="modal" 
                                                        class="btn btn-primary btn-xs" 
                                                        onclick="ambil_data('<?= $row['member_id'] ?>')">Pilih
                                                </button>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    endforeach;
                                } else {
                                    ?>
                                    <tr id="row">
                                        <td colspan="8" align="center">Data Kosong</td>
                                    </tr>
                                    <?php } ?>
                            </tbody>
                        </table>	
                    </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-bs-dismiss="modal">Tutup</button>
                </div>

            </div>
        </div>
    </div>
</div>
<!--end popup data santri  -->

<!-- set default pageLength: 5 -->
<script>
    $(document).ready( function () {
        var table = $('#dtable').DataTable( {
            pageLength : 5,
            lengthMenu: [[5, 10, 20, 100, -1], [5, 10, 20, 100, 'All']]
        } )
    } );
</script>