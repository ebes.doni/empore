<span class="dropdown-header mt-4">Menu</span>
<small class="bi-three-dots nav-subtitle-replacer"></small>

    <div class="nav-item">
        <a class="nav-link <?= (!$this->uri->segment(3)) ? 'active' : '' ?>" href="<?= site_url('member') ?>" 
            role="button" data-bs-target="#navbarVerticalMenudashboard" 
            aria-expanded="false" aria-controls="navbarVerticalMenudashboard">
            <i class="fa fa-th nav-icon"></i>
            <span class="nav-link-title">Dashboard</span>
        </a>
    </div>
   
    <div class="nav-item">
          <a class="nav-link <?= ($this->uri->segment(3)=='member_profil') ? 'active' : '' ?>" 
            href="<?= site_url('member/akun/member_profil') ?>" 
            role="button" data-bs-target="#navbarVerticalMenudashboard" 
            aria-expanded="false" aria-controls="navbarVerticalMenudashboard">
            <i class="fa fa-user nav-icon"></i>
            <span class="nav-link-title">Profile</span>
          </a>
    </div>

    <div class="nav-item">
          <a class="nav-link <?= ($this->uri->segment(3)=='member_book_request') ? 'active' : '' ?>" 
            href="<?= site_url('member/akun/member_book_request') ?>" 
            role="button" data-bs-target="#navbarVerticalMenuBookRequest" 
            aria-expanded="false" aria-controls="navbarVerticalMenuBookRequest">
            <i class="fa fa-paper-plane nav-icon"></i>
            <span class="nav-link-title">Book Request</span>
          </a>
    </div>

    <div class="nav-item">
          <a class="nav-link <?= ($this->uri->segment(3)=='member_borrow') ? 'active' : '' ?>" 
            href="<?= site_url('member/akun/member_borrow') ?>" 
            role="button" data-bs-target="#navbarVerticalMenuborrow" 
            aria-expanded="false" aria-controls="navbarVerticalMenuborrow">
            <i class="fa fa-book nav-icon"></i>
            <span class="nav-link-title">List Peminjaman</span>
          </a>
    </div>
          
<!-- ========== Left Sidebar End ========== -->                     
<!-- </div> -->