<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('SUPERUSER', 1);
define('USER', 2);
define('AKADEMIK', 3);
define('KESISWAAN', 4);
define('GURU', 6);
define('KEUANGAN', 7);
define('KASIR', 8);
define('SEKRETARIS', 9);
define('PENGASUHAN', 10);

define('SCHOOL_NAME',1);
define('SCHOOL_ADRESS',2);
define('SCHOOL_PHONE',3);
define('SCHOOL_DISTRICT',4);
define('SCHOOL_CITY',5);
define('SCHOOL_LOGO',6);
define('SCHOOL_HEAD_SCHOOL',15);
define('SCHOOL_HEAD_FINANCE',16);
define('SERVER_API_MIDTRANS',17);
define('CLIENT_API_MIDTRANS',18);

/* End of file constants.php */
/* Location: ./constants.php */
