###################
Sistem Peminjaman Buku PT Empore
###################

Sebuah aplikasi web peminjaman buku sederhana dengan menggunakan Codeigniter 3.


###################
Stack Teknologi
###################
•	Bahasa Pemrograman: PHP
•	Framework: Codeigniter 3
•	Database: MySQL
•	Front-End: HTML, CSS Bootstrap, Jquery, datatables.net 
•	Web Server: Apache
•	Version Control: Git (Gitlab)
•	Libraries: RESTful API, Sweetalert2, DomPDF, Barcodegen

###################
Link :
###################
Template : https://htmlstream.com/preview/front-dashboard-v2.1.1/documentation/index.html 

RESTful API : https://github.com/chriskacerguis/codeigniter-restserver    

###################
Server Requirements
###################
•	PHP versi 5.6
•	MySQL versi 5.5
•	Web server Apache2  





